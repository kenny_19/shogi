package Vue;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class MapEnVue {
    public int pix=80;
    private int height;
    private int width;
    public MapEnVue(){
        height=720;
        width=720;
    }
    public void render(Graphics g){
        //on remplacer la carte par default
        for(int x=0;x<width/pix;x++) {
            for(int y=0;y<height/pix;y++) {
                g.setColor(new Color(250,250,250));
                g.fillRect(x*pix, y*pix, 75, 75);
            }
        }
        //desssiner frontiere
        g.setColor(new Color(0,0,0));
        g.drawRect(0,0,width,height);
    }
}
