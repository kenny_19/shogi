package Vue;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;

public class PieceEnVue extends JPanel {
    public int pix=80;
    private int height;
    private int width;
    HashMap<Integer,Image>listImg;
    public PieceEnVue(){
        height=720;
        width=720;
        // charge les img de piece
        listImg =new HashMap<Integer,Image>();

        listImg.put(11,Toolkit.getDefaultToolkit().getImage("src/img/11.PNG"));
        listImg.put(12,Toolkit.getDefaultToolkit().getImage("src/img/12.PNG"));
        listImg.put(13,Toolkit.getDefaultToolkit().getImage("src/img/13.PNG"));
        listImg.put(14,Toolkit.getDefaultToolkit().getImage("src/img/14.PNG"));
        listImg.put(15,Toolkit.getDefaultToolkit().getImage("src/img/15.PNG"));
        listImg.put(16,Toolkit.getDefaultToolkit().getImage("src/img/16.PNG"));
        listImg.put(17,Toolkit.getDefaultToolkit().getImage("src/img/17.PNG"));
        listImg.put(18,Toolkit.getDefaultToolkit().getImage("src/img/18.PNG"));

        listImg.put(21,Toolkit.getDefaultToolkit().getImage("src/img/21.PNG"));
        listImg.put(22,Toolkit.getDefaultToolkit().getImage("src/img/22.PNG"));
        listImg.put(23,Toolkit.getDefaultToolkit().getImage("src/img/23.PNG"));
        listImg.put(24,Toolkit.getDefaultToolkit().getImage("src/img/24.PNG"));
        listImg.put(25,Toolkit.getDefaultToolkit().getImage("src/img/25.PNG"));
        listImg.put(26,Toolkit.getDefaultToolkit().getImage("src/img/26.PNG"));
        listImg.put(27,Toolkit.getDefaultToolkit().getImage("src/img/27.PNG"));
        listImg.put(28,Toolkit.getDefaultToolkit().getImage("src/img/28.PNG"));

        listImg.put(33,Toolkit.getDefaultToolkit().getImage("src/img/33.PNG"));
        listImg.put(34,Toolkit.getDefaultToolkit().getImage("src/img/34.PNG"));
        listImg.put(35,Toolkit.getDefaultToolkit().getImage("src/img/35.PNG"));
        listImg.put(36,Toolkit.getDefaultToolkit().getImage("src/img/36.PNG"));
        listImg.put(37,Toolkit.getDefaultToolkit().getImage("src/img/37.PNG"));
        listImg.put(38,Toolkit.getDefaultToolkit().getImage("src/img/38.PNG"));

        listImg.put(43,Toolkit.getDefaultToolkit().getImage("src/img/43.PNG"));
        listImg.put(44,Toolkit.getDefaultToolkit().getImage("src/img/44.PNG"));
        listImg.put(45,Toolkit.getDefaultToolkit().getImage("src/img/45.PNG"));
        listImg.put(46,Toolkit.getDefaultToolkit().getImage("src/img/46.PNG"));
        listImg.put(47,Toolkit.getDefaultToolkit().getImage("src/img/47.PNG"));
        listImg.put(48,Toolkit.getDefaultToolkit().getImage("src/img/48.PNG"));
    }
    public void render(Graphics g,int[][] mycarte){

        //on remplacer la carte par default
        for(int i=1;i <= 9; i++){
            for(int j=1;j<=9;j++){
                if(mycarte[i][j]!=0){
                    g.drawImage(listImg.get(mycarte[i][j]), (i-1)*pix, (j-1)*pix, 70, 70, Color.WHITE, this);
                }
            }
        }

    }
}
