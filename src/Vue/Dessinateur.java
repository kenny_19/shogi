package Vue;

import Modele.Map.CarteCommun;

import java.awt.*;
import java.awt.image.BufferStrategy;

/**
 * Cette Methode pour dessiner les chose dans la carte
 * */
public class Dessinateur extends Canvas implements Runnable{
    public static final int WIDTH=950,HEIGHT=720;
    public static final String TITLE="Shogi";
    public MapEnVue level;
    public PieceEnVue piece;
    public CarteCommun myCarteM;
    /**
     * typeStrategie si il est 2 on choissis une case deplacement au hasard
     * si il est 3 on choissis une case deplacement parmi les case avec la plus grand note de priorite de action
     * */
    public int typeStrategie=2;

    public Dessinateur(CarteCommun myCarteM) {
        Dimension dimension=new Dimension(Dessinateur.WIDTH, Dessinateur.HEIGHT);
        setPreferredSize(dimension);
        setMinimumSize(dimension);
        setMaximumSize(dimension);

        //ini ce jeu
        this.myCarteM =myCarteM;

        level = new MapEnVue();//ini la carte
        piece= new PieceEnVue();//ini les piece
    }

    public Dessinateur(CarteCommun myCarteM,int typeStrategie) {
        Dimension dimension=new Dimension(Dessinateur.WIDTH, Dessinateur.HEIGHT);
        setPreferredSize(dimension);
        setMinimumSize(dimension);
        setMaximumSize(dimension);

        //ini ce jeu
        this.myCarteM =myCarteM;

        level = new MapEnVue();//ini la carte
        piece= new PieceEnVue();//ini les piece

        this.typeStrategie=typeStrategie;
    }

    @Override
    public void run() {
        requestFocus();
        render();
        render();
        //calculer les possibilite de deplacement
        if(typeStrategie==2){
            myCarteM.deplacerEquipe1();
            myCarteM.deplacerEquipe2();
        }else if(typeStrategie==3){
            myCarteM.deplacerEquipe1Priorite();
            myCarteM.deplacerEquipe2Priorite();
        }

    }

    private void render() {
        BufferStrategy bs=getBufferStrategy();
        if(bs==null) {
            createBufferStrategy(3);
            return;
        }

        Graphics g=bs.getDrawGraphics();
        g.setColor(Color.black);
        g.fillRect(0, 0, 720, Dessinateur.HEIGHT);
        g.setColor(Color.darkGray);
        g.fillRect(720, 0, Dessinateur.WIDTH, Dessinateur.HEIGHT);


        //print les chose
        level.render(g);//appelle render dans MapEnVue
        piece.render(g,myCarteM.getCarte());
        //piecec.render(g,myCarteM.getStartage())
        g.dispose();
        bs.show();
    }
}
