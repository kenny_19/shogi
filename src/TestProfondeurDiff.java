import Modele.Joueur.PieceAuto;
import Modele.Map.CarteCommun;
import Modele.Piece.PieceFactory;
import Vue.Dessinateur;

import javax.swing.*;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class TestProfondeurDiff {
    public static void main(String[] args) throws Exception{

        CarteCommun myCarteM=new CarteCommun();
        Dessinateur dessine=new Dessinateur(myCarteM,3);
        JFrame frame=new JFrame();
        frame.setTitle(Dessinateur.TITLE);
        frame.add(dessine);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);

        frame.setVisible(true);


        CyclicBarrier myB = new CyclicBarrier(40,dessine);

        Thread t1;
        Thread t2;
        Thread t3;
        Thread t4;
        Thread t5;
        Thread t6;
        Thread t7;
        Thread t8;
        Thread t9;
        Thread t10;
        Thread t11;
        Thread t12;
        Thread t13;
        Thread t14;
        Thread t15;
        Thread t16;
        Thread t17;
        Thread t18;
        Thread t19;
        Thread t20;
        Thread t21;
        Thread t22;
        Thread t23;
        Thread t24;
        Thread t25;
        Thread t26;
        Thread t27;
        Thread t28;
        Thread t29;
        Thread t30;
        Thread t31;
        Thread t32;
        Thread t33;
        Thread t34;
        Thread t35;
        Thread t36;
        Thread t37;
        Thread t38;
        Thread t39;
        Thread t40;

        PieceFactory pf=new PieceFactory();
        //ini les pawn

        t1 = new PieceAuto(pf.getPieceEnLieu("Pawn",1),myCarteM,myB,"t1",4);
        t2 = new PieceAuto(pf.getPieceEnLieu("Pawn",2),myCarteM,myB,"t2",4);
        t3 = new PieceAuto(pf.getPieceEnLieu("Pawn",3),myCarteM,myB,"t3",4);
        t4 = new PieceAuto(pf.getPieceEnLieu("Pawn",4),myCarteM,myB,"t4",4);
        t5 = new PieceAuto(pf.getPieceEnLieu("Pawn",5),myCarteM,myB,"t5",4);
        t6 = new PieceAuto(pf.getPieceEnLieu("Pawn",6),myCarteM,myB,"t6",4);
        t7 = new PieceAuto(pf.getPieceEnLieu("Pawn",7),myCarteM,myB,"t7",4);
        t8 = new PieceAuto(pf.getPieceEnLieu("Pawn",8),myCarteM,myB,"t8",4);

        t9 = new PieceAuto(pf.getPieceEnLieu("Pawn",9),myCarteM,myB,"t9",4,3);
        t10 = new PieceAuto(pf.getPieceEnLieu("Pawn",10),myCarteM,myB,"t10",4,3);
        t11 = new PieceAuto(pf.getPieceEnLieu("Pawn",11),myCarteM,myB,"t11",4,3);
        t12 = new PieceAuto(pf.getPieceEnLieu("Pawn",12),myCarteM,myB,"t12",4,3);
        t13 = new PieceAuto(pf.getPieceEnLieu("Pawn",13),myCarteM,myB,"t13",4,3);
        t14 = new PieceAuto(pf.getPieceEnLieu("Pawn",14),myCarteM,myB,"t14",4,3);
        t15 = new PieceAuto(pf.getPieceEnLieu("Pawn",15),myCarteM,myB,"t15",4,3);
        t16 = new PieceAuto(pf.getPieceEnLieu("Pawn",16),myCarteM,myB,"t16",4,3);
        t17 = new PieceAuto(pf.getPieceEnLieu("Pawn",17),myCarteM,myB,"t17",4,3);
        t18 = new PieceAuto(pf.getPieceEnLieu("Pawn",18),myCarteM,myB,"t18",4,3);

        //ini les bishop
        t19 = new PieceAuto(pf.getPieceEnLieu("Bishop",1),myCarteM,myB,"t19",4);
        t20 = new PieceAuto(pf.getPieceEnLieu("Bishop",2),myCarteM,myB,"t20",4,3);

        //ini les gold
        t21 = new PieceAuto(pf.getPieceEnLieu("Gold",1),myCarteM,myB,"t21",4);
        t22 = new PieceAuto(pf.getPieceEnLieu("Gold",2),myCarteM,myB,"t22",4);
        t23 = new PieceAuto(pf.getPieceEnLieu("Gold",3),myCarteM,myB,"t23",4,3);
        t24 = new PieceAuto(pf.getPieceEnLieu("Gold",4),myCarteM,myB,"t24",4,3);

        //ini les king
        t25 = new PieceAuto(pf.getPieceEnLieu("King",1),myCarteM,myB,"t25",4);
        t26 = new PieceAuto(pf.getPieceEnLieu("King",2),myCarteM,myB,"t26",4,3);

        //ini les knights
        t27 = new PieceAuto(pf.getPieceEnLieu("KNight",1),myCarteM,myB,"t27",4);
        t28 = new PieceAuto(pf.getPieceEnLieu("KNight",2),myCarteM,myB,"t28",4);
        t29 = new PieceAuto(pf.getPieceEnLieu("KNight",3),myCarteM,myB,"t29",4,3);
        t30 = new PieceAuto(pf.getPieceEnLieu("KNight",4),myCarteM,myB,"t30",4,3);

        //ini les lancer
        t31 = new PieceAuto(pf.getPieceEnLieu("Lance",1),myCarteM,myB,"t31",4);
        t32 = new PieceAuto(pf.getPieceEnLieu("Lance",2),myCarteM,myB,"t32",4);
        t33 = new PieceAuto(pf.getPieceEnLieu("Lance",3),myCarteM,myB,"t33",4,3);
        t34 = new PieceAuto(pf.getPieceEnLieu("Lance",4),myCarteM,myB,"t34",4,3);

        //ini les rook
        t35 = new PieceAuto(pf.getPieceEnLieu("Rook",1),myCarteM,myB,"t35",4);
        t36 = new PieceAuto(pf.getPieceEnLieu("Rook",2),myCarteM,myB,"t36",4,3);

        //ini les silver
        t37 = new PieceAuto(pf.getPieceEnLieu("Silver",1),myCarteM,myB,"t37",4);
        t38 = new PieceAuto(pf.getPieceEnLieu("Silver",2),myCarteM,myB,"t38",4);
        t39 = new PieceAuto(pf.getPieceEnLieu("Silver",3),myCarteM,myB,"t39",4,3);
        t40 = new PieceAuto(pf.getPieceEnLieu("Silver",4),myCarteM,myB,"t40",4,3);

        Executor executor = Executors.newFixedThreadPool(40);

        executor.execute(t1);
        executor.execute(t2);
        executor.execute(t3);
        executor.execute(t4);
        executor.execute(t5);
        executor.execute(t6);
        executor.execute(t7);
        executor.execute(t8);
        executor.execute(t9);
        executor.execute(t10);

        executor.execute(t11);
        executor.execute(t12);
        executor.execute(t13);
        executor.execute(t14);
        executor.execute(t15);
        executor.execute(t16);
        executor.execute(t17);
        executor.execute(t18);
        executor.execute(t19);
        executor.execute(t20);

        executor.execute(t21);
        executor.execute(t22);
        executor.execute(t23);
        executor.execute(t24);
        executor.execute(t25);
        executor.execute(t26);
        executor.execute(t27);
        executor.execute(t28);
        executor.execute(t29);
        executor.execute(t30);

        executor.execute(t31);
        executor.execute(t32);
        executor.execute(t33);
        executor.execute(t34);
        executor.execute(t35);
        executor.execute(t36);
        executor.execute(t37);
        executor.execute(t38);
        executor.execute(t39);
        executor.execute(t40);
    }
}
