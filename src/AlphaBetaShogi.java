import java.io.*;
  
class AlphaBetaShogi {
  
    // Valeurs initiales d'Alpha et Beta
    static int MAX = 1000;      // +infini
    static int MIN = -1000;     // -infini
    
    // Retourne les valeurs optimal pour le joueur courant
    static int minimax(int depth, int nodeIndex, Boolean maximizingPlayer, int values[], int alpha, int beta)
    {
        // Si on atteint la profondeur max de l'arbre
        if (depth == 3) {
            return values[nodeIndex];
        }
            
        if (maximizingPlayer)
        {
            int best = MIN;
    
            for (int i = 0; i < 2; i++)
            {
                int val = minimax(depth + 1, nodeIndex * 2 + i, false, values, alpha, beta);
                best = Math.max(best, val);
                alpha = Math.max(alpha, best);
    
                if (beta <= alpha)
                    break;
            }
            return best;
        }
        else {
            int best = MAX;  
            for (int i = 0; i < 2; i++)
            {     
                int val = minimax(depth + 1, nodeIndex * 2 + i, true, values, alpha, beta);
                best = Math.min(best, val);
                beta = Math.min(beta, best);
    
                if (beta <= alpha)
                    break;
            }
            return best;
        }
    }
  
    public static void main (String[] args)
    {
        /*  
                       ?                                   5  
                   /       \                           /       \
                 ?           ?                       5           2    
                / \         / \          =>         / \         / \ 
              ?     ?     ?     ?                 5     6     2     ?
             / \   / \   / \   / \               / \   / \   / \   / \
            3   5 6   9 1   2 0   -1            3   5 6   9 1   2 0   -1          
        */
        int values[] = {3, 5, 6, 9, 1, 2, 0, -1};   // minimax optimum : 5
        System.out.println("=============================");
        System.out.println("Minimax optimum : " + minimax(0, 0, true, values, MIN, MAX));
        System.out.println("=============================");
     
    }
}
