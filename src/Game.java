import Modele.Map.CarteM;
import Vue.MapEnVue;
import Vue.PieceEnVue;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;

/**
 * Ce class est premier version de sho-gi.
 * On jouer ce jau avec la main.
 * */
public class Game extends Canvas implements Runnable, KeyListener {

    public static final int WIDTH=720,HEIGHT=720;
    private boolean isRunning=false;
    private Thread thread;
    public static final String TITLE="Shogi";
    public MapEnVue level;
    public PieceEnVue piece;
    public CarteM myCarteM;

    public Game() {
        Dimension dimension=new Dimension(Game.WIDTH,Game.HEIGHT);
        setPreferredSize(dimension);
        setMinimumSize(dimension);
        setMaximumSize(dimension);

        //ini ce jeu
        myCarteM =new CarteM();


        addKeyListener(this);
        level = new MapEnVue();//ini la carte
        piece= new PieceEnVue();//ini les piece
    }

    private void render() {
        BufferStrategy bs=getBufferStrategy();
        if(bs==null) {
            createBufferStrategy(3);
            return;
        }

        Graphics g=bs.getDrawGraphics();
        g.setColor(Color.black);
        g.fillRect(0, 0, Game.WIDTH, Game.HEIGHT);


        //print les chose
        level.render(g);//appelle render dans MapEnVue
        piece.render(g,myCarteM.getCarte());
        //piecec.render(g,myCarteM.getStartage())
        g.dispose();
        bs.show();
    }


    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {

    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {

    }

    @Override
    public void run() {
        requestFocus();
        int fps=0;
        double timer=System.currentTimeMillis();
        long lastTime=System.nanoTime();
        double targetTick=60.0;
        double delta=0;
        double ns=1000000000/targetTick;
        //pour attend dessine
        int i = 0;
        while(isRunning) {
            long now=System.nanoTime();
            delta+=(now - lastTime)/ns;
            lastTime=now;
            while(delta>=1) {
                i++;
                if(i%10 == 0 && i != 0){
                    tick();
                    i=0;
                }
                render();
                fps++;
                delta--;
            }

            if(System.currentTimeMillis() - timer>=1000) {
                //System.out.println(fps);
                fps=0;
                timer+=1000;
            }
        }
        stop();
    }

    public synchronized void stop()  {
        if(!isRunning)return;
        isRunning=false;

        try {
            thread.join();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

    private void tick() {
        //System.out.println("Working");
        myCarteM.trick1();
    }

    public static void main(String[] args) {

        Game game=new Game();
        JFrame frame=new JFrame();
        frame.setTitle(Game.TITLE);
        frame.add(game);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);

        frame.setVisible(true);

        game.start();
    }

    public synchronized void start() {
        if(isRunning)return;
        isRunning=true;
        thread=new Thread(this);
        thread.start();
    }
}