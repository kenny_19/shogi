package Modele.Joueur;

import Modele.InfoDeplace.FactoryInfoD;
import Modele.InfoDeplace.InfoD;
import Modele.Map.CarteCommun;
import Modele.Piece.King;
import Modele.Piece.PieceFactory;
import Modele.Piece.PieceNormal;
import Modele.Strategie.CarteAvecInfoD;
import Modele.Strategie.NoeudCarte;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.Collections;
/**
 * Cette class est pour creer les obj de piece qui peut deplace automatique.
 * cyclicBarrier est contoleur pour les thread
 * c est donne partagant
 * piece est piece normal qui contient les info pour les piece sho-gi
 * */
public class PieceAuto extends Thread{
    private CyclicBarrier cyclicBarrier;
    private CarteCommun c;
    private PieceNormal piece;
    /**
     * modeJeu est pour choisir le mode de deplacement
     * 1 est premier version comme gameauto1.
     * 2 est deuxieme version comme gameauto2.
     * 3 est troisime version qui choix le cas de deplacement parmi les case qui ont max note de priorite
     * 4 est quatrsieme version qui calculer vec algo MinMax
     * */
    private int modeJeu;
    private String name="anonyme";
    //Info de deplacement possible pour prochaine etape utilise avec minmax
    //private InfoD deplaceProchaine=null;
    //hauteur pour arbre MinMax on choisir 2 par default
    private int depthArbre;

    public PieceAuto(PieceNormal p,CarteCommun c){
        this.piece=p;
        this.c=c;
        placerPiece();
        modeJeu=1;
        depthArbre=2;
    }

    public PieceAuto(PieceNormal p,CarteCommun c,CyclicBarrier myBarrier,String myName){
        this.piece=p;
        this.c=c;
        this.cyclicBarrier=myBarrier;
        placerPiece();
        modeJeu=2;
        name=myName;
        depthArbre=2;
    }

    public PieceAuto(PieceNormal p,CarteCommun c,CyclicBarrier myBarrier,String myName,int modeJeu){
        this.piece=p;
        this.c=c;
        this.cyclicBarrier=myBarrier;
        placerPiece();
        this.modeJeu=modeJeu;
        name=myName;
        depthArbre=2;
    }

    public PieceAuto(PieceNormal p,CarteCommun c,CyclicBarrier myBarrier,String myName,int modeJeu,int heuteurArbre){
        this.piece=p;
        this.c=c;
        this.cyclicBarrier=myBarrier;
        placerPiece();
        this.modeJeu=modeJeu;
        name=myName;
        if(heuteurArbre<1){
            depthArbre=2;
        }else{
            depthArbre=heuteurArbre;
        }

    }

    @Override
    public void run() {
        super.run();
        Scanner scan = new Scanner(System.in);
        //pour premier version pas de communication
        if(modeJeu==1){
            while (true){
                System.out.println("===============================================" + "\n" +
                        " Thread "+ Thread.currentThread().getName() + " fonctionne: " + "\n"
                        +" pour continuer entree 0 por terminer entree 9 : ");
                // test si un entier
                if (scan.hasNextInt()) {
                    // test si 0 on deplacer
                    if(scan.nextInt()==0){
                        this.auto1();
                        System.out.println("x de piece : "+ piece.getX() + " y de piece : "+ piece.getY() + " etat de piece : " + piece.getEtat()+" ");
                    }else if(scan.nextInt()==9){
                        break;
                    }
                }
            }
        }else if(modeJeu>1){
            //avec commuication mais pas de priorite de action
            try {
                while (!c.fini()){
                    cyclicBarrier.await();
                    choisisModeCommunicationEquipe1();
                    cyclicBarrier.await();
                    //c.affichierCalnal1();
                    choisisModeDeplcementEquipe1();
                    cyclicBarrier.await();
                    Thread.sleep(2000);
                    c.videCanal1();
                    cyclicBarrier.await();
                    choisisModeCommunicationEquipe2();
                    cyclicBarrier.await();
                    //c.affichierCalnal2();
                    choisisModeDeplcementEquipe2();
                    cyclicBarrier.await();
                    Thread.sleep(2000);
                    c.videCanal2();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (BrokenBarrierException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * pour les pieces dans equipe 1
     * choisir le fancon de deplacer piece
     * ca dependre modeJeu
     * quand modeJeu est plus grand 1
     * */
    public void choisisModeDeplcementEquipe1(){
        if(modeJeu==2){
            auto2_equipe1();
        }else if(modeJeu==3){
            auto3_equipe1();
        }else if(modeJeu==4 || modeJeu==5){
            auto4_equipe1();
        }
    }

    /**
     * pour les pieces dans equipe 2
     * choisir le fancon de deplacer piece
     * ca dependre modeJeu
     * quand modeJeu est plus grand 1
     * */
    public void choisisModeDeplcementEquipe2(){
        if(modeJeu==2){
            auto2_equipe2();
        }else if(modeJeu==3){
            auto3_equipe2();
        }else if(modeJeu==4 || modeJeu==5){
            auto4_equipe2();
        }
    }

    /**
     * deplacer ce piece avec mange et bouge
     * mais pas de communication
     * */
    public void auto1(){
        c.setCarte(piece.auto1(c).getCarte());
        //printCarte pour test
        c.printCarte();
    }

    /**
     * deplacer ce piece avec mange et bouge
     * avec communication basic
     * mais pas de priorite de action
     * */
    public void auto2_equipe1(){
        if((piece.getEtat()>=10 && piece.getEtat()<20)||(piece.getEtat()>=30 && piece.getEtat()<40)){
            String nomDeDeplacement=c.getNomDeplaceEquip1();
            if(name.equals(nomDeDeplacement)){
                c.setCarte(piece.auto1(c).getCarte());
                //printCarte pour test
                c.affichierCalnal1();
                c.printCarte();
            }
        }
    }

    /**
     * deplacer ce piece avec mange et bouge
     * avec communication basic
     * mais pas de priorite de action
     * */
    public void auto2_equipe2(){
        if((piece.getEtat()>=20 && piece.getEtat()<30)||(piece.getEtat()>=40 && piece.getEtat()<50)){
            String nomDeDeplacement=c.getNomDeplaceEquip2();
            if(name.equals(nomDeDeplacement)){
                c.setCarte(piece.auto1(c).getCarte());
                //printCarte pour test
                //c.affichierCalnal2();
                c.printCarte();
            }
        }
    }

    /**
     * deplacer ce piece avec mange et bouge
     * avec communication basic
     * avec de priorite de action basic mange et bouge
     * */
    public void auto3_equipe1(){
        if((piece.getEtat()>=10 && piece.getEtat()<20)||(piece.getEtat()>=30 && piece.getEtat()<40)){
            String nomDeDeplacement=c.getNomDeplaceEquip1();
            if(name.equals(nomDeDeplacement)){
                c.setCarte(piece.auto2(c).getCarte());
                //printCarte pour test
                c.affichierCalnal1();
                c.printCarte();
            }
        }
    }

    /**
     * deplacer ce piece avec mange et bouge
     * avec communication basic
     *  avec de priorite de action basic mange et bouge
     * */
    public void auto3_equipe2(){
        if((piece.getEtat()>=20 && piece.getEtat()<30)||(piece.getEtat()>=40 && piece.getEtat()<50)){
            String nomDeDeplacement=c.getNomDeplaceEquip2();
            if(name.equals(nomDeDeplacement)){
                c.setCarte(piece.auto2(c).getCarte());
                //printCarte pour test
                //c.affichierCalnal2();
                c.printCarte();
            }
        }
    }

    /**
     * deplacer ce piece avec mange et bouge
     * avec communication basic
     * avec de priorite de action basic mange et bouge
     * */
    public void auto4_equipe1(){
        if((piece.getEtat()>=10 && piece.getEtat()<20)||(piece.getEtat()>=30 && piece.getEtat()<40)){
            InfoD infoDeplacementCommun=c.getInfoDeplaceEquip1();
            if(equalBasicInfoDEtPiece(infoDeplacementCommun,piece)){
                c.setCarte(piece.auto2(c).getCarte());
                //printCarte pour test
                c.affichierInfoDeplqceE1();
                c.printCarte();
            }
        }
    }

    /**
     * deplacer ce piece avec mange et bouge
     * avec communication basic
     *  avec de priorite de action basic mange et bouge
     * */
    public void auto4_equipe2(){
        if((piece.getEtat()>=20 && piece.getEtat()<30)||(piece.getEtat()>=40 && piece.getEtat()<50)){
            InfoD infoDeplacementCommun=c.getInfoDeplaceEquip2();
            if(equalBasicInfoDEtPiece(infoDeplacementCommun,piece)){
                c.setCarte(piece.auto2(c).getCarte());
                //printCarte pour test
                c.affichierInfoDeplqceE2();
                c.printCarte();
            }
        }
    }

    /**
     * tester les baisc champs de infoD est equal.
     * */
    public boolean equalBasicInfoDEtPiece(InfoD a,PieceNormal b){
        if(a.getX()==b.getX()){
            if(a.getY()==b.getY()){
                if(a.getEtatP()==b.getEtat()){
                        return true;
                }
            }
        }
        return false;
    }

    /**
     * choisis mode de communication de equipe 1
     * ce depend le modeJeu
     * quand modeJeu est plus grand 1
     * */
    public void choisisModeCommunicationEquipe1(){
        if(modeJeu == 2){
            communication2_equipe1();
        }else if(modeJeu == 3){
            communication3_equipe1();
        }else if(modeJeu == 4){
            communication4_equipe1();
        }else if(modeJeu == 5){
            communication5_equipe1();
        }
    }

    /**
     * choisis mode de communication de equipe 2
     * ce depend le modeJeu
     * quand modeJeu est plus grand 1
     * */
    public void choisisModeCommunicationEquipe2(){
        if(modeJeu == 2){
            communication2_equipe2();
        }else if(modeJeu == 3){
            communication3_equipe2();
        }else if(modeJeu == 4){
            communication4_equipe2();
        }else if(modeJeu == 5){
            communication5_equipe2();
        }
    }

    /**
     * deposer un case de deplacement dans canal1 de communication
     * mais pas de priorite de action
     * */
    public void communication2_equipe1(){

        if((piece.getEtat()>=10 && piece.getEtat()<20)||(piece.getEtat()>=30 && piece.getEtat()<40)){
            //calculer les cas de deplcement
            ArrayList<InfoD>list=piece.deplacePossible(c.getCarte());
            InfoD myInfo=piece.choixAuHazard(list);
            c.ajouteUneCase(piece.getEtat(),myInfo,name);
        }

    }
    /**
     * deposer un case de deplacement dans canal2 de communication
     * mais pas de priorite de action
     * */
    public void communication2_equipe2(){

        if((piece.getEtat()>=20 && piece.getEtat()<30)||(piece.getEtat()>=40 && piece.getEtat()<50)){
            //calculer les cas de deplcement
            ArrayList<InfoD>list=piece.deplacePossible(c.getCarte());
            InfoD myInfo=piece.choixAuHazard(list);
            c.ajouteUneCase(piece.getEtat(),myInfo,name);
        }

    }

    /**
     * deposer un case de deplacement dans canal1 de communication
     * avec priorite de action basic comme bouge et mange
     * */
    public void communication3_equipe1(){

        if((piece.getEtat()>=10 && piece.getEtat()<20)||(piece.getEtat()>=30 && piece.getEtat()<40)){
            //calculer les cas de deplcement
            ArrayList<InfoD>list=piece.deplacePossibleAvecNote(c.getCarte());
            InfoD myInfo=piece.choixAuHazardAvecPriorite(list);
            c.ajouteUneCase(piece.getEtat(),myInfo,name);
        }

    }

    /**
     * deposer un case de deplacement dans canal2 de communication
     * avec priorite de action basic comme bouge et mange
     * */
    public void communication3_equipe2(){

        if((piece.getEtat()>=20 && piece.getEtat()<30)||(piece.getEtat()>=40 && piece.getEtat()<50)){
            //calculer les cas de deplcement
            ArrayList<InfoD>list=piece.deplacePossibleAvecNote(c.getCarte());
            InfoD myInfo=piece.choixAuHazardAvecPriorite(list);
            c.ajouteUneCase(piece.getEtat(),myInfo,name);
        }

    }

    /**
     * deposer un case de deplacement dans canal1 de communication
     * avec priorite de action avec les note vient de minmax algo
     * renouvouler variable menbre deplaceProchaine
     * */
    public void communication4_equipe1(){

        if((piece.getEtat()>=10 && piece.getEtat()<20)||(piece.getEtat()>=30 && piece.getEtat()<40)){
            //calculer les cas de deplcement
            int depth=depthArbre;
            int equipe=1;
            NoeudCarte racin=new NoeudCarte(new CarteAvecInfoD(c.getCarte()));
            InfoD res=racin.chercheEtapeProchaine(c.getCarte(),depth,equipe,1);
            int note=NoeudCarte.minimaxVal(racin,depth,equipe,equipe);
            res.setPoint(note);
            //deplaceProchaine=res;
            InfoD InfoDepot= FactoryInfoD.geneInfoD(res.getX(),res.getY(),res.getDeltaX(),res.getDeltaY(),res.getEtatP());
            c.ajouteUneCase(piece.getEtat(),InfoDepot,name);
        }

    }

    /**
     * deposer un case de deplacement dans canal2 de communication
     * avec priorite de action avec les note vient de minmax algo
     * renouvouler variable menbre deplaceProchaine
     * */
    public void communication4_equipe2(){

        if((piece.getEtat()>=20 && piece.getEtat()<30)||(piece.getEtat()>=40 && piece.getEtat()<50)){
            //calculer les cas de deplcement
            int depth=depthArbre;
            int equipe=2;
            NoeudCarte racin=new NoeudCarte(new CarteAvecInfoD(c.getCarte()));
            InfoD res=racin.chercheEtapeProchaine(c.getCarte(),depth,equipe,1);
            int note=NoeudCarte.minimaxVal(racin,depth,equipe,equipe);
            res.setPoint(note);
            //deplaceProchaine=res;
            InfoD InfoDepot= FactoryInfoD.geneInfoD(res.getX(),res.getY(),res.getDeltaX(),res.getDeltaY(),res.getEtatP());
            c.ajouteUneCase(piece.getEtat(),InfoDepot,name);
        }

    }
    /**
     * deposer un case de deplacement dans canal1 de communication
     * avec priorite de action avec les note vient de alpha beta minmax algo
     * renouvouler variable menbre deplaceProchaine
     * */
    public void communication5_equipe1(){

        if((piece.getEtat()>=10 && piece.getEtat()<20)||(piece.getEtat()>=30 && piece.getEtat()<40)){
            //calculer les cas de deplcement
            int depth=depthArbre;
            int equipe=1;
            NoeudCarte racin=new NoeudCarte(new CarteAvecInfoD(c.getCarte()));
            InfoD res=racin.chercheEtapeProchaine(c.getCarte(),depth,equipe,2);
            int note=NoeudCarte.minimaxVal(racin,depth,equipe,equipe);
            res.setPoint(note);
            //deplaceProchaine=res;
            InfoD InfoDepot= FactoryInfoD.geneInfoD(res.getX(),res.getY(),res.getDeltaX(),res.getDeltaY(),res.getEtatP());
            c.ajouteUneCase(piece.getEtat(),InfoDepot,name);
        }

    }

    /**
     * deposer un case de deplacement dans canal2 de communication
     * avec priorite de action avec les note vient de alpha beta minmax algo
     * renouvouler variable menbre deplaceProchaine
     * */
    public void communication5_equipe2(){

        if((piece.getEtat()>=20 && piece.getEtat()<30)||(piece.getEtat()>=40 && piece.getEtat()<50)){
            //calculer les cas de deplcement
            int depth=depthArbre;
            int equipe=2;
            NoeudCarte racin=new NoeudCarte(new CarteAvecInfoD(c.getCarte()));
            InfoD res=racin.chercheEtapeProchaine(c.getCarte(),depth,equipe,2);
            int note=NoeudCarte.minimaxVal(racin,depth,equipe,equipe);
            res.setPoint(note);
            //deplaceProchaine=res;
            InfoD InfoDepot= FactoryInfoD.geneInfoD(res.getX(),res.getY(),res.getDeltaX(),res.getDeltaY(),res.getEtatP());
            c.ajouteUneCase(piece.getEtat(),InfoDepot,name);
        }

    }


    /**
     * placer ce piece dans la carte
     * quand on inisialiser ce piece
     * */
    public void placerPiece(){
        c.placerPiece(piece.getX(),piece.getY(),piece.getEtat());
    }



}
