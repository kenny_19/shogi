package Modele.Joueur;
import Modele.Piece.*;

import java.util.ArrayList;
/**
 * Cette classe prensente une equipe pour jouer en main.
 *  */
public class JoueurNormal {
    private ArrayList collectionPiece = new ArrayList<PieceNormal>();
    //Les pièces qui peuvent être jouées
    private ArrayList pieceHorsCarte = new ArrayList<PieceNormal>();
    //donne la place de joueur 1 et 2
    //1 pour haut
    //2 pour base
    private int place;

    public JoueurNormal(int place){
        if(place==1){
            //ini pawn
            for(int i=1;i<=9;i++){
                collectionPiece.add(new Pawn(i));
            }
            //ini Rook
            collectionPiece.add(new Rook(1));
            //ini Bishop
            collectionPiece.add(new Bishop(1));
            //ini lancer
            collectionPiece.add(new Lance(1));
            collectionPiece.add(new Lance(2));
            //ini knight
            collectionPiece.add(new KNight(1));
            collectionPiece.add(new KNight(2));
            //ini silver
            collectionPiece.add(new Silver(1));
            collectionPiece.add(new Silver(2));
            //ini gold
            collectionPiece.add(new Gold(1));
            collectionPiece.add(new Gold(2));
            //ini king
            collectionPiece.add(new King(1));
        }else if(place==2){
            //ini pawn
            for(int i=10;i<=18;i++){
                collectionPiece.add(new Pawn(i));
            }
            //ini Rook
            collectionPiece.add(new Rook(2));
            //ini Bishop
            collectionPiece.add(new Bishop(2));
            //ini lancer
            collectionPiece.add(new Lance(3));
            collectionPiece.add(new Lance(4));
            //ini knight
            collectionPiece.add(new KNight(3));
            collectionPiece.add(new KNight(4));
            //ini silver
            collectionPiece.add(new Silver(3));
            collectionPiece.add(new Silver(4));
            //ini gold
            collectionPiece.add(new Gold(3));
            collectionPiece.add(new Gold(4));
            //ini king
            collectionPiece.add(new King(2));
        }
    }

    public ArrayList getCollectionPiece() {
        return collectionPiece;
    }

    public ArrayList getPieceHorsCarte() {
        return pieceHorsCarte;
    }

    public int getPlace() {
        return place;
    }

    public boolean cherchePieceParLieu(int x,int y){
        PieceNormal tmp;
        for(int i=0;i<collectionPiece.size();i++){
            tmp = (PieceNormal) collectionPiece.get(i);
            if(tmp.getX()==x && tmp.getY()==y){
                return true;
            }
        }
        return false;
    }

    public PieceNormal cherchePieceExacteParLieu(int x,int y){
        PieceNormal tmp=null;
        for(int i=0;i<collectionPiece.size();i++){
            tmp = (PieceNormal) collectionPiece.get(i);
            if(tmp.getX()==x && tmp.getY()==y){
                return tmp;
            }
        }
        return tmp;
    }

    //on deplacer une piece si on le trouve le lieu de ce piece
    public void deplacerPieceEnJoueur(int x,int y,int deltaX,int deltaY,int[][] laCarte){
        PieceNormal tmp=null;
        for(int i=0;i<collectionPiece.size();i++){
            tmp = (PieceNormal) collectionPiece.get(i);
            if(tmp.getX()==x && tmp.getY()==y){
                System.out.println("-----------trouve piece----------");
                collectionPiece.remove(i);
                i--;
                tmp.deplacer(deltaX, deltaY,laCarte);
                collectionPiece.add(tmp);
            }
        }
    }

    /**
     * si une piece est mange par d'autre
     * on enleve ce piece
     * */
    public void enleveUnePieceDansCarte(int x,int y){
        PieceNormal tmp=null;
        for(int i=0;i<collectionPiece.size();i++){
            tmp = (PieceNormal) collectionPiece.get(i);
            if(tmp.getX()==x && tmp.getY()==y){
                System.out.println("-----------trouve piece----------");
                collectionPiece.remove(i);
                i--;
            }
        }
    }

    /**
     * ajouter une piece dans la carte
     * */
    public void ajouterUnePiece(PieceNormal p){
        collectionPiece.add(p);
    }

    /**
     * gagne une piece apres mange une piece
     * */
    public void gagneUnePiece(PieceNormal p){
        String joueur;
        if(place==1)
            joueur="joueur1";
        else
            joueur="joueur2";
        pieceHorsCarte.add(p.copyHorsCarte(joueur));
    }
}
