package Modele.Map;

import Modele.Joueur.JoueurNormal;
import Modele.Piece.PieceNormal;

import java.util.ArrayList;
import java.util.Scanner;
/**
 * Ce classe presente la carte en commune.
 * Ce class est utilise pour jouer en main.
 * */
public class CarteM {
    //La Map où se trouve les pièces
    //utilise pas colonne 0 et ligne 0
    private int carte[][]=new int [10][10];
    private int tour;
    //deux joueur dans le map
    private JoueurNormal joueur1;
    private JoueurNormal joueur2;

    public CarteM(){
        for(int i=0;i < 10; i++){
            for(int j=0;j<10;j++){
                carte[i][j]=0;
            }
        }
        //premier collection avec les piece
        joueur1=new JoueurNormal(1);
        //deuxieme collection avec les piece
        joueur2=new JoueurNormal(2);
        f5();
        tour = 11;
    }

    /**pour saisir c'est le tour de quel equipe
     * 11-13 pour equipe 1
     * 21-23 pour equipe 2
     * */
    public void changeTour(){
        if(tour<21){
            if(tour<13)
                tour++;
            else
                tour=21;
        }
        else {
            if(tour<23)
                tour++;
            else
                tour=11;
        }
    }

    public int getTour(){
        return tour;
    }

    //renouvouler le map avec les info utilisateur
    public void f5(){
        ArrayList collectionPiece1 = joueur1.getCollectionPiece();
        ArrayList collectionPiece2 = joueur2.getCollectionPiece();
        PieceNormal tmp;
        for(int i=0;i < 10; i++){
            for(int j=0;j<10;j++){
                carte[i][j]=0;
            }
        }
        int lieuX=0;
        int lieuY=0;
        for (int i=0; i<collectionPiece1.size();i++){
            tmp = (PieceNormal) collectionPiece1.get(i);
            lieuX = tmp.getX();
            lieuY = tmp.getY();
            carte[lieuX][lieuY]= tmp.getEtat();
        }
        for (int i=0; i<collectionPiece2.size();i++){
            tmp = (PieceNormal) collectionPiece2.get(i);
            lieuX = tmp.getX();
            lieuY = tmp.getY();
            carte[lieuX][lieuY]= tmp.getEtat();
        }
    }

    public int[][] getCarte() {
        return carte;
    }

    public void printCarte(){
        for(int i=1;i<=9;i++){
            for(int j=1;j<=9;j++){
                if(carte[j][i]==0)
                    System.out.print("00"+" ");
                else
                    System.out.print(carte[j][i]+" ");
            }
            System.out.println();
        }
    }

    //renvoie 1 si trouve dans joueur 1
    //renvoie 2 si trouve dans joueur 2
    //renvoie -1 si trouve pas
    public int cherchePieceEnLieu(int x,int y){
        if(joueur1.cherchePieceParLieu(x,y))
            return 1;
        else if(joueur2.cherchePieceParLieu(x,y))
            return 2;
        else
            return -1;
    }

    //cherche un piece exacte
    public PieceNormal cherchePieceExacte(int x,int y){
        if(joueur1.cherchePieceParLieu(x,y))
            return joueur1.cherchePieceExacteParLieu(x,y);
        else if(joueur2.cherchePieceParLieu(x,y))
            return joueur2.cherchePieceExacteParLieu(x,y);
        else
            return null;
    }

    /**
     * deplacer un piece
     * si ce piece exsite
     * ,on discute si il peut
     * mange une piece dans d'autre equipe
     * et faire la promotion apres deplacement
     * */
    public void deplacerUnPiece(int x,int y,int deltaX,int deltaY){
        int cherchePiece=cherchePieceEnLieu(x,y);
        PieceNormal tmp;
        if(cherchePiece==1){
            tmp=cherchePieceExacte(x,y);
            if(tmp.testDeplacer(deltaX,deltaY,carte)){
                mangeUnePiece(x,y,deltaX,deltaY);
                promoUnePiece(x,y,deltaX,deltaY);
                joueur1.deplacerPieceEnJoueur(x,y,deltaX,deltaY,carte);
            }
        }else if(cherchePiece==2){
            tmp=cherchePieceExacte(x,y);
            if(tmp.testDeplacer(deltaX,deltaY,carte)){
                mangeUnePiece(x,y,deltaX,deltaY);
                promoUnePiece(x,y,deltaX,deltaY);
                joueur2.deplacerPieceEnJoueur(x,y,deltaX,deltaY,carte);
            }
        }
        f5();
    }

    /**
     * tester apres deplacement ce piece peux mange la piece dans destination
     * si oui mange une piece qui est dans d'autre equipe
     * sinon rien faire
     * */
    public void mangeUnePiece(int x,int y,int deltaX,int deltaY){
        int cherchePiece=cherchePieceEnLieu(x,y);
        PieceNormal tmp=null;
        if(cherchePiece==1){
            tmp=cherchePieceExacte(x,y);
            //si joueur1 mange une piece de joueur2
            if(tmp.testMange(deltaX,deltaY,carte)){
                PieceNormal pieceDautre=cherchePieceExacte(x+deltaX,y+deltaY);
                joueur2.enleveUnePieceDansCarte(pieceDautre.getX(),pieceDautre.getY());
                joueur1.gagneUnePiece(pieceDautre);
            }
        }else if(cherchePiece==2){
            tmp=cherchePieceExacte(x,y);
            //si joueur2 mange une piece de joueur1
            if(tmp.testMange(deltaX,deltaY,carte)){
                PieceNormal pieceDautre=cherchePieceExacte(x+deltaX,y+deltaY);
                joueur1.enleveUnePieceDansCarte(pieceDautre.getX(),pieceDautre.getY());
                joueur2.gagneUnePiece(pieceDautre);
            }
        }
    }

    /**
     * tester apres deplacement ce piece peux faire la promotion dans destination
     * si oui on faire la promotion
     * sinon on faire rien
     * */
    public void promoUnePiece(int x,int y,int deltaX,int deltaY){
        int cherchePiece=cherchePieceEnLieu(x,y);
        PieceNormal tmp=null;
        if(cherchePiece==1){
            tmp=cherchePieceExacte(x,y);
            if(tmp.testPromotion(deltaY)){
                System.out.print("peux promo-------");
                joueur1.enleveUnePieceDansCarte(tmp.getX(),tmp.getY());
                tmp.promotion();
                joueur1.ajouterUnePiece(tmp);
            }
        }else if(cherchePiece==2){
            tmp=cherchePieceExacte(x,y);
            if(tmp.testPromotion(deltaY)){
                System.out.print("peux promo------");
                joueur2.enleveUnePieceDansCarte(tmp.getX(),tmp.getY());
                tmp.promotion();
                joueur2.ajouterUnePiece(tmp);
            }
        }
    }

    //jour ce jeu seulement manuel
    public void trick1(){
        int x = 0;
        int y = 0;
        int deltaX = 0;
        int deltaY = 0;
        Scanner scan = new Scanner(System.in);
        this.printCarte();
        System.out.print("choisir X d'un piece 1-9(entrer 10 pour quitter) : ");
        if (scan.hasNextInt()) {
            // test si un entier
            x = scan.nextInt();
        } else {
            // entrer un info erreur
            System.out.println("pas entier！");
        }

        System.out.print("choisir Y d'un piece 1-9(entrer 10 pour quitter) : ");
        if (scan.hasNextInt()) {
            // test si un entier
            y = scan.nextInt();
        } else {
            // entrer un info erreur
            System.out.println("pas entier");
        }

        System.out.print("choisir deltaX d'un piece 1-9(entrer 10 pour quitter) : ");
        if (scan.hasNextInt()) {
            // test si un entier
            deltaX = scan.nextInt();
        } else {
            // entrer un info erreur
            System.out.println("pas entier");
        }

        System.out.print("choisir Y d'un piece 1-9(entrer 10 pour quitter) : ");
        if (scan.hasNextInt()) {
            // test si un entier
            deltaY = scan.nextInt();
        } else {
            // entrer un info erreur
            System.out.println("pas entier");
        }

        this.deplacerUnPiece(x,y,deltaX,deltaY);
    }

}
