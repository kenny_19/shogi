package Modele.Map;

import Modele.InfoDeplace.InfoD;
import Modele.Piece.PieceNormal;

import java.util.ArrayList;

/**
 * Ce carte est pour piece auto.
 * Il est le donnes partage.
 * carte pour la carte commun
 * canalEquipe1 canal pour cammunication de equipe1 (10-18,30-38,50-58)
 * canalEquipe2 canal pour communication de equipe2 (20-28,40-48m60-68)
 * */
public class CarteCommun {
    volatile int carte[][]=new int [10][10];
    volatile ArrayList<InfoD>canalEquipe1;
    volatile ArrayList<InfoD>canalEquipe2;
    volatile String nomDeplaceEquip1="";
    volatile String nomDeplaceEquip2="";
    volatile InfoD infoDeplaceEquip1;
    volatile InfoD infoDeplaceEquip2;

    public InfoD getInfoDeplaceEquip1() {
        return infoDeplaceEquip1;
    }

    public void setInfoDeplaceEquip1(InfoD infoDeplaceEquip1) {
        this.infoDeplaceEquip1 = infoDeplaceEquip1;
    }

    public InfoD getInfoDeplaceEquip2() {
        return infoDeplaceEquip2;
    }

    public void setInfoDeplaceEquip2(InfoD infoDeplaceEquip2) {
        this.infoDeplaceEquip2 = infoDeplaceEquip2;
    }

    public CarteCommun(){
        for(int i=0;i < 10; i++){
            for(int j=0;j<10;j++){
                carte[i][j]=0;
            }
        }
        canalEquipe1=new ArrayList<InfoD>();
        canalEquipe2=new ArrayList<InfoD>();
    }

    /**
     * Laissir le piece auto se placer dans la carte
     * */
    public synchronized void placerPiece(int x,int y,int etat){
        //verifier les piece est dans la carte
        if(x<=9 && x>=1 && y<=9 && y>=1){
            carte[x][y]=etat;
        }
    }

    /**
     * Dessiner la carte avec ligne de command
     * *  */
    public synchronized void printCarte(){
        System.out.println();
        for(int i=1;i<=9;i++){
            for(int j=1;j<=9;j++){
                if(carte[j][i]==0)
                    System.out.print("00"+" ");
                else
                    System.out.print(carte[j][i]+" ");
            }
            System.out.println();
        }
    }

    /**
     * deplacer un piece
     * contient les situation de mange et promo
     * */
    public synchronized void deplacerUnPiece(int x,int y,int deltaX,int deltaY){
        if(x<=9 && x>=1 && y<=9 && y>=1){
            if(x+deltaX<=9 && x+deltaX>=1 && y+deltaY<=9 && y+deltaY>=1){
                carte[x+deltaX][y+deltaY]=carte[x][y];
                carte[x][y]=0;
            }
        }
    }

    public int[][] getCarte() {
        return carte;
    }

    public synchronized void setCarte(int[][] carte) {
        this.carte = carte;
    }

    /**
     * Laissir une piece depot son case de deplacement dans le canal en Commune
     * @param etat
     * */
    public synchronized void ajouteUneCase(int etat,InfoD casDeplacer,String nomDePiece){
        if(casDeplacer!=null){
            casDeplacer.setName(nomDePiece);
            if((etat>=10 && etat<20)||(etat>=30 && etat<40)){
                canalEquipe1.add(casDeplacer);
            }else if((etat>=20 && etat<30)||(etat>=40 && etat<50)){
                canalEquipe2.add(casDeplacer);
            }
        }
    }

    /**
     * Vider Canal
     * */
    public synchronized void videCanal1(){
        canalEquipe1=new ArrayList<InfoD>();
    }

    /**
     * Vider Canal
     * */
    public synchronized void videCanal2(){
        canalEquipe2=new ArrayList<InfoD>();
    }

    /**
     * choisir les case de deplacement au hazard
     * */
    public void deplacerEquipe1(){
        if(canalEquipe1.size()!=0){
            int index = (int) (Math.random()* canalEquipe1.size());
            nomDeplaceEquip1 = canalEquipe1.get(index).getName();
        }else
            nomDeplaceEquip1 = null;
    }

    /**
     * choisir les case de deplacement parmi le case a maximum note de priorite
     * */
    public void deplacerEquipe2(){
        if(canalEquipe2.size()!=0){
            int index = (int) (Math.random()* canalEquipe2.size());
            nomDeplaceEquip2 = canalEquipe2.get(index).getName();
        }else
            nomDeplaceEquip2 = null;
    }

    /**
     * choisir les case de deplacement au hazard
     * */
    public void deplacerEquipe2Priorite(){
        if(canalEquipe2.size()!=0){
            InfoD caseTemp=canalEquipe2.get(0);
            int noteMax=caseTemp.getPoint();
            for(InfoD infoD:canalEquipe2){
                caseTemp=infoD;
                if(caseTemp.getPoint()>noteMax)
                    noteMax=caseTemp.getPoint();
            }
            ArrayList<InfoD> listAvecPlusGrandNote=new ArrayList<InfoD>();
            for (InfoD infoD : canalEquipe2) {
                caseTemp = infoD;
                if (caseTemp.getPoint() == noteMax)
                    listAvecPlusGrandNote.add(caseTemp);
            }
            int index = (int) (Math.random()* listAvecPlusGrandNote.size());
            nomDeplaceEquip2 = listAvecPlusGrandNote.get(index).getName();
            infoDeplaceEquip2 = listAvecPlusGrandNote.get(index);
        }
    }

    /**
     * choisir les case de deplacement parmi le case a maximum note de priorite
     * */
    public void deplacerEquipe1Priorite(){
        if(canalEquipe1.size()!=0){
            InfoD caseTemp=canalEquipe1.get(0);
            int noteMax=caseTemp.getPoint();
            for(InfoD infoD:canalEquipe1){
                caseTemp=infoD;
                if(caseTemp.getPoint()>noteMax)
                    noteMax=caseTemp.getPoint();
            }
            ArrayList<InfoD> listAvecPlusGrandNote=new ArrayList<InfoD>();
            for (InfoD infoD : canalEquipe1) {
                caseTemp = infoD;
                if (caseTemp.getPoint() == noteMax)
                    listAvecPlusGrandNote.add(caseTemp);
            }
            int index = (int) (Math.random()* listAvecPlusGrandNote.size());
            nomDeplaceEquip1 = listAvecPlusGrandNote.get(index).getName();
            infoDeplaceEquip1 = listAvecPlusGrandNote.get(index);
        }
    }



    public void affichierCalnal1(){
        String res = " canal1 avec size : "+canalEquipe1.size()+" \n";
        System.out.println(res);
        for(int i=0;i<canalEquipe1.size();i++){
            canalEquipe1.get(i).afficher(canalEquipe1.get(i));
        }
    }

    public void affichierCalnal2(){
        String res = " canal2 avec size : "+canalEquipe2.size()+" \n";
        System.out.println(res);
        for(int i=0;i<canalEquipe2.size();i++){
            canalEquipe2.get(i).afficher(canalEquipe2.get(i));
        }
    }

    public void affichierInfoDeplqceE1(){
        infoDeplaceEquip1.afficher(infoDeplaceEquip1);
    }

    public void affichierInfoDeplqceE2(){
        infoDeplaceEquip2.afficher(infoDeplaceEquip2);
    }

    public String getNomDeplaceEquip1() {
        return nomDeplaceEquip1;
    }

    public void setNomDeplaceEquip1(String nomDeplaceEquip1) {
        this.nomDeplaceEquip1 = nomDeplaceEquip1;
    }

    public String getNomDeplaceEquip2() {
        return nomDeplaceEquip2;
    }

    public void setNomDeplaceEquip2(String nomDeplaceEquip2) {
        this.nomDeplaceEquip2 = nomDeplaceEquip2;
    }



    public boolean fini(){
        boolean roi1=false;
        boolean roi2=false;
        for(int i=1;i<=9;i++){
            for(int j=1;j<=9;j++){
                if(carte[j][i]==11)
                    roi1=true;
                if(carte[j][i]==21)
                    roi2=true;
            }
        }
        return !(roi1&&roi2);
    }
}
