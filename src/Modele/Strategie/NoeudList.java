package Modele.Strategie;

import java.util.ArrayList;

/**
 * Ce class est un cas simple pour realiser un arbre avec arraylist
 * */
public abstract class NoeudList implements Noeud{
    protected int id;
    protected NoeudList parent;
    protected ArrayList<NoeudList> fils;

    public void setParent(NoeudList p){
        this.parent=p;
        this.parent.insertFils(this);
    }

    public NoeudList getParent(){
        return parent;
    }

    public void supprimeParent(){
        this.parent.supprimeFils(this);
        this.parent=null;
    }

    public void insertFils(NoeudList f){
        f.parent=this;
        fils.add(f);
    }

    public void supprimeFils(NoeudList f){
        NoeudList temp;
        for(int i=0;i<fils.size();i++){
            temp=fils.get(i);
            if(temp.getId()==f.getId()){
                fils.remove(i);
            }
        }
    }


    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id=id;
    }

    @Override
    public int getSize() {
        return fils.size();
    }

    @Override
    public int getHeight() {
        if(fils.size()==0)
            return 0;
        else{
            ArrayList<Integer>highListe=new ArrayList<Integer>();
            int highMax=0;
            for( NoeudList i : fils){
                if(i.getHeight()>highMax)
                    highMax=i.getHeight();
            }
            return highMax+1;
        }
    }

    @Override
    public int getDepth() {
        if(parent==null)
            return 0;
        else{
            return 1+parent.getDepth();
        }
    }
    /**
     * parcour un arbre prefix
     * */
    public void parcourArbre(){
        System.out.println(this.id);
        if(fils.size()!=0){
            for(NoeudList i:fils){
                i.parcourArbre();
            }
        }
    }

    /**
     * parcour un arbre prefix avec son valeur
     * */
    public abstract void parcourArbreAvecVal();

    /**
     * montrer le fils avec plus grand valeur
     * */
    public abstract void afficherMaxFils();

    /**
     * montrer le fils avec plus petite valeur
     * */
    public abstract void afficherMinFils();
}
