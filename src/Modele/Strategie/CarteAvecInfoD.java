package Modele.Strategie;

import Modele.InfoDeplace.*;

public class CarteAvecInfoD {
    private int[][] carteProchaine;
    private InfoD infoDepalce;
    private int typeD;

    public CarteAvecInfoD(int[][] carte, InfoD d){
        carteProchaine=carte;
        infoDepalce=d;
        typeD=typeInfoD(d);
    }

    /**
     * creer CarteAvecInfoD pardefault
     * */
    public CarteAvecInfoD(int[][] carte){
        carteProchaine=carte;
        infoDepalce=new InfoDPawn(0,0,0,0,18);
        typeD=typeInfoD(infoDepalce);
    }


    public int[][] getCarteProchaine() {
        return carteProchaine;
    }

    public void setCarteProchaine(int[][] carteProchaine) {
        this.carteProchaine = carteProchaine;
    }

    public InfoD getInfoDepalce() {
        return infoDepalce;
    }

    public void setInfoDepalce(InfoD infoDepalce) {
        this.infoDepalce = infoDepalce;
    }

    public int getTypeD() {
        return typeD;
    }

    public void setTypeD(int typeD) {
        this.typeD = typeD;
    }

    public void afficher(){
        int[][] carte = carteProchaine;
        System.out.println("-------------CarteAvecInfoD---------");
        for(int i=1;i<=9;i++){
            for(int j=1;j<=9;j++){
                if(carte[j][i]==0)
                    System.out.print("00"+" ");
                else
                    System.out.print(carte[j][i]+" ");
            }
            System.out.println();
        }
        System.out.println("type infoD : "+ typeD);
        infoDepalce.afficher(infoDepalce);
        System.out.println("---------------------------------");
    }

    /**
     * cherche type de infoD.
     * @param o object dans class infoD
     * @return 6 pour Bishop...
     * 9 pour resultat faux;
     * */
    public static int typeInfoD(InfoD o){
        if(o.getClass().equals(InfoDBishop.class)){
            return 6;
        }else if(o.getClass().equals(InfoDGold.class)){
            return 2;
        }else if(o.getClass().equals(InfoDKing.class)){
            return 1;
        }else if(o.getClass().equals(InfoDKNight.class)){
            return 4;
        }else if(o.getClass().equals(InfoDLance.class)){
            return 7;
        }else if(o.getClass().equals(InfoDPawn.class)){
            return 8;
        }else if(o.getClass().equals(InfoDRook.class)){
            return 5;
        }else if(o.getClass().equals(InfoDSilver.class)){
            return 3;
        }else{
            return 9;
        }
    }
}
