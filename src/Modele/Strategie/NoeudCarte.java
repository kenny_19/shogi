package Modele.Strategie;

import Modele.InfoDeplace.InfoD;

import java.util.ArrayList;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class NoeudCarte extends NoeuListEnt{
    /**
     * On placer carte et info de deplacement avec default dans racine.
     * On placer la carte possible (apres deplace piece avec avec info de deplacemnet)
     * et l'info de depacement dans d'autre noeud
     * */
    protected CarteAvecInfoD infoCarteEtDeplace;
    /**
     * id de noeud augement auto
     */
    static int idCommun=0;

    public NoeudCarte(int id, int valeur) {
        super(id, valeur);
    }

    public NoeudCarte(int valeur) {
        super(valeur);
    }

    public NoeudCarte(int id, int valeur, NoeuListEnt parent) {
        super(id, valeur, parent);
    }

    public NoeudCarte(int valeur, NoeuListEnt parent) {
        super(valeur, parent);
    }

    public NoeudCarte(CarteAvecInfoD carte){
        infoCarteEtDeplace=carte;
        id=idCommun;
        idCommun++;
        note=0;
    }


    public CarteAvecInfoD getInfoCarteEtDeplace() {
        return infoCarteEtDeplace;
    }

    public void setInfoCarteEtDeplace(CarteAvecInfoD infoDeplacement) {
        this.infoCarteEtDeplace = infoDeplacement;
    }
    /**
     * donne la racine qui contient la carte origine,generer l'arbre contient les cartes possibles.
     * Et donne les note
     * */
    public NoeudCarte geneArbreDeNoeudDeCarteAvecFront(NoeudCarte parent,int depth,int equipe){
        if(depth==0){
            //si pas de fils et on arrive plus fond
            //on calculer les notes
            return parent;
        }else{
            //si il existe fils
            CarteAvecInfoD temp=parent.getInfoCarteEtDeplace();
            ArrayList<CarteAvecInfoD>listCarteProche=GenererLaCarteProchaine.prochaineCarte(temp.getCarteProchaine(),equipe);
            ArrayList<NoeudCarte>listFils=new ArrayList<NoeudCarte>();
            NoeudCarte noeudCarteTemp;
            for(CarteAvecInfoD i:listCarteProche){
                if(equipe==1) {
                    //equipe 1
                    noeudCarteTemp = geneArbreDeNoeudDeCarteAvecFront(new NoeudCarte(i), depth - 1, 2);
                }else{
                    //equipe 2
                    noeudCarteTemp = geneArbreDeNoeudDeCarteAvecFront(new NoeudCarte(i), depth - 1, 1);
                }
                parent.insertFils(noeudCarteTemp);
                //fils.add(noeudCarteTemp);
            }
            return parent;
        }
    }

    /**
     * calculer les notes pour sles fils au plus bas
     * */
    public void donnerNotePourLesFeuilBas(CarteAvecInfoD carteDepart,int equipe){
        if(this.fils.size()==0){
            CompteurNote c=new CompteurNote();
            this.setNote(c.calculerLesNoteCarteAvecInfo(carteDepart,this.getInfoCarteEtDeplace(),equipe));
        }else{
            NoeudCarte temp;
            for(NoeudList i:fils){
                temp=(NoeudCarte)i;
                temp.donnerNotePourLesFeuilBas(carteDepart,equipe);
            }
        }
    }

    @Override
    public void parcourArbreAvecVal(){
        System.out.println(this.getId() + ": " + this.getNote());
        infoCarteEtDeplace.afficher();
        if(fils.size()!=0){
            for(NoeudList i:fils){
                i.parcourArbreAvecVal();
            }
        }
    }

    public int nombreNoeud(){
        if(fils.size()==0)
            return 1;
        else{
            int nbNoeudFils=0;
            NoeudCarte temp;
            for(NoeudList i:fils){
                temp=(NoeudCarte)i;
                nbNoeudFils=nbNoeudFils+temp.nombreNoeud();
            }
            return nbNoeudFils+1;
        }
    }


    /**
     * affichier arrilist contient NoeuListEnt
     * */
    public static void afficherListNoeudCarte(ArrayList<NoeudCarte> a){
        if(a.size()!=0){
            NoeudCarte temp;
            for(int i=0;i<a.size();i++){
                temp=(NoeudCarte)a.get(i);
                System.out.println("ordre de membre : "+ i +" Id : "+temp.getId()+" valeur "+temp.getNote());
                temp.infoCarteEtDeplace.afficher();
            }
        }
    }


    /**
     * On cherche le chemin de racine ver la fils qui a le value cherche par le algo minmax.
     * qui contient avec destination et la rasine.
     * On donne la priorite pour le chemain qui contient les etape prochaine avec mange piece d'autre equipe.
     * @param n arbre
     * @param depth largeur depart de rasine
     * @param team la piece est dans equipe 1
     * */
    public static ArrayList<NoeudCarte> minimaxChemin(NoeudCarte n,int depth,int team,int tour){
        int bestVallue=minimaxVal(n,depth,team,tour);
        NoeudCarte destination=chercheNoeudAvecVal(n,bestVallue,team);
        return chercheChemin(destination);
    }

    /**
     * retourner un chemin si on connais la noeud racine la noeud destination
     * */
    public static ArrayList<NoeudCarte> chercheChemin(NoeudCarte destination){
        ArrayList<NoeudCarte>chemin=new ArrayList<>();
        if(destination!=null) {
            //cherche depth de destination
            int depthDeDest=destination.getDepth();
            NoeudCarte temp=destination;
            for(int i=0;i<=depthDeDest;i++) {
                chemin.add(temp);
                if (temp.getParent() != null){
                    temp = (NoeudCarte) temp.getParent();
                }
            }
        }
        return chemin;
    }

    public NoeudCarte chercheMaxFils(){
        if(fils.size()==0)
            return null;
        else{
            NoeudCarte temp;
            NoeudCarte maxFils=(NoeudCarte)fils.get(0);
            int maxVal=maxFils.note;
            for(int i=0;i<fils.size();i++){
                temp=(NoeudCarte)fils.get(i);
                if(temp.getNote()>maxVal){
                    maxFils=temp;
                    maxVal=maxFils.note;
                }
            }
            return maxFils;
        }
    }

    public NoeudCarte chercheMinFils(){
        if(fils.size()==0)
            return null;
        else{
            NoeudCarte temp;
            NoeudCarte minFils=(NoeudCarte)fils.get(0);
            int minVal=minFils.note;
            for(int i=0;i<fils.size();i++){
                temp=(NoeudCarte)fils.get(i);
                if(temp.getNote()<minVal){
                    minFils=temp;
                    minVal=minFils.note;
                }
            }
            return minFils;
        }
    }

    /**
     * On cherche le prochain deplacement avec ce algo.
     * Et par default on retourner la valeur max fils dans le cas termine.
     * Par contre on utilise ce algo seulement dans le tour de eauipe pour les piece.
     * @param n arbre
     * @param depth largeur depart de rasine
     * @param team chercher min ou max
     * @param tour le tour de equipe maintenance, motif pas
     * */
    public static int minimaxVal(NoeudCarte n,int depth,int team,int tour){
        if(depth==0){
            return n.getNote();
        }else{
            if(tour==1){
                if(team==1){
                    //chercher max
                    if(n.fils.size()!=0){
                        NoeudCarte f = (NoeudCarte)n.fils.get(0);
                        int myValue;
                        int bestValue = minimaxVal(f,depth-1,2);
                        for (NoeudList fil : n.fils) {
                            f = (NoeudCarte) fil;
                            myValue = minimaxVal(f, depth - 1, 2);
                            if (myValue > bestValue){
                                bestValue = myValue;
                                //n.setNote(bestValue);
                            }
                        }
                        return bestValue;
                    }else
                        return n.getNote();
                }else{
                    //chercher min
                    if(n.fils.size()!=0){
                        NoeudCarte f = (NoeudCarte)n.fils.get(0);
                        int myValue;
                        int bestValue = minimaxVal(f,depth - 1,1);
                        for (NoeudList fil : n.fils) {
                            f = (NoeudCarte) fil;
                            myValue = minimaxVal(f, depth - 1, 1);
                            if (myValue < bestValue){
                                bestValue = myValue;
                                //n.setNote(bestValue);
                            }
                        }
                        return bestValue;
                    }else
                        return n.getNote();
                }
            }else{
                //cas tour est 2
                if(team==2){
                    //chercher max
                    if(n.fils.size()!=0){
                        NoeudCarte f = (NoeudCarte)n.fils.get(0);
                        int myValue;
                        int bestValue = minimaxVal(f,depth-1,1);
                        for (NoeudList fil : n.fils) {
                            f = (NoeudCarte) fil;
                            myValue = minimaxVal(f, depth - 1, 1);
                            if (myValue > bestValue){
                                bestValue = myValue;
                                //n.setNote(bestValue);
                            }
                        }
                        return bestValue;
                    }else
                        return n.getNote();
                }else{
                    //chercher min
                    if(n.fils.size()!=0){
                        NoeudCarte f = (NoeudCarte)n.fils.get(0);
                        int myValue;
                        int bestValue = minimaxVal(f,depth - 1,2);
                        for (NoeudList fil : n.fils) {
                            f = (NoeudCarte) fil;
                            myValue = minimaxVal(f, depth - 1, 2);
                            if (myValue < bestValue){
                                bestValue = myValue;
                                //n.setNote(bestValue);
                            }
                        }
                        return bestValue;
                    }else
                        return n.getNote();
                }
            }
        }
    }
    /**
     * c'est une version améliorer pour algo minmax
     * */
    public static int alphaBeta(NoeuListEnt n,int depth,int alpha,int beta,int team,int tour){
        if(depth == 0 || n.fils.size()==0){
            return n.getNote();
        }else{
            if(tour == 1){
                //case tour est 1
               if(team==1){
                   //cherche max
                   NoeuListEnt f;
                   int myValue = -1000;
                   for (NoeudList fil : n.fils) {
                       f = (NoeuListEnt) fil;
                       myValue = max(myValue, alphaBeta(f, depth - 1, alpha, beta, 2));
                       alpha = max(alpha,myValue);
                       if(alpha >= beta)
                           break;
                   }
                   return myValue;
               }else{
                   //cherche min
                   NoeuListEnt f;
                   int myValue = 1000;
                   for (NoeudList fil : n.fils) {
                       f = (NoeuListEnt) fil;
                       myValue = min(myValue, alphaBeta(f, depth - 1, alpha, beta, 1));
                       beta = min(beta, myValue);
                       if (alpha >= beta)
                           break;
                   }
                   return myValue;
               }
            }else{
               //case tour est 2
                if(team==2){
                    //cherche max
                    NoeuListEnt f;
                    int myValue = -1000;
                    for (NoeudList fil : n.fils) {
                        f = (NoeuListEnt) fil;
                        myValue = max(myValue, alphaBeta(f, depth - 1, alpha, beta, 1));
                        alpha = max(alpha,myValue);
                        if(alpha >= beta)
                            break;
                    }
                    return myValue;
                }else{
                    //cherche min
                    NoeuListEnt f;
                    int myValue = 1000;
                    for (NoeudList fil : n.fils) {
                        f = (NoeuListEnt) fil;
                        myValue = min(myValue, alphaBeta(f, depth - 1, alpha, beta, 2));
                        beta = min(beta, myValue);
                        if (alpha >= beta)
                            break;
                    }
                    return myValue;
                }
            }
        }
    }

    /**
     * cherche un noeud avec son valeur.
     * On ameliorer ce fonction.
     * On donne la priorite pour le chemain qui contient les etape prochaine avec mange piece d'autre equipe.
     * */
    public static NoeudCarte chercheNoeudAvecVal(NoeudCarte n,int val,int equipe){
        ArrayList<NoeudCarte>resList = chercheToutLesNoeudAvecVal(n,val);
        if(resList.size()!=0){
            NoeudCarte temp=resList.get(0);;
            ArrayList<NoeudCarte>tempList;
            CompteurNote compteur =new CompteurNote();
            int sizeDeTempList;
            int noteTemp;
            //parcour chaque noeud possible avec son chemin
            //calculer les note pour la prochaine etape
            for(int i=0;i<resList.size();i++){
                temp = resList.get(i);
                tempList=chercheChemin(temp);
                sizeDeTempList = tempList.size();
                if(sizeDeTempList>2){
                    noteTemp=compteur.calculerLesNote(n.getInfoCarteEtDeplace().getCarteProchaine(),tempList.get(sizeDeTempList-2).getInfoCarteEtDeplace().getCarteProchaine(),equipe);
                    if( noteTemp>0)
                        return temp;
                }
            }
            return temp;
        }
        else
            return  null;
    }

    /**
     * cherche tout les noeud avec un valeur dans ce arbre
     * */
    public static ArrayList<NoeudCarte> chercheToutLesNoeudAvecVal(NoeudCarte n,int val){
        ArrayList<NoeudCarte>res = new ArrayList<>();
        if(n.getNote()==val)
            //pour les feuil avec meme valeur
            res.add(n);
        else if(n.getNote()!=val && n.fils.size()==0){
        //pour les feuil avec d'autre valeur
        }else{
            ArrayList<NoeudCarte> resTemporaire=new ArrayList<>();
            NoeudCarte temp;
            for(int i=0;i<n.fils.size();i++){
                temp=(NoeudCarte)n.fils.get(i);
                resTemporaire=chercheToutLesNoeudAvecVal(temp,val);
                if(resTemporaire.size()!=0)
                    res.addAll(resTemporaire);
            }
        }
        return res;
    }

    /**
     * recherche InfoD avec etape prochaine
     * @param type 1 pour minmax Normal, 2 pour alphabeta minmx
     * */
    public InfoD chercheEtapeProchaine(int myC[][],int depth,int equipe,int type){
        //partie construit
        NoeudCarte racin=new NoeudCarte(new CarteAvecInfoD(myC));
        racin=racin.geneArbreDeNoeudDeCarteAvecFront(racin,depth,equipe);
        racin.donnerNotePourLesFeuilBas(racin.getInfoCarteEtDeplace(),equipe);
        int minMaxV;
        if(type==1)
            minMaxV=NoeudCarte.minimaxVal(racin,depth,equipe,equipe);
        else
            minMaxV=NoeudCarte.alphaBeta(racin,depth,-1000,1000,equipe,equipe);
        NoeudCarte res;
        //discutter les etape
        if(minMaxV==0){
            //il n'y a pas de point gagner dans les tours suivant
            if(depth!=0){
                int index = (int) (Math.random()* racin.fils.size());
                res=(NoeudCarte)racin.fils.get(index);

                return res.getInfoCarteEtDeplace().getInfoDepalce();
            }else{

                return racin.getInfoCarteEtDeplace().getInfoDepalce();
            }

        }else{
            ArrayList<NoeudCarte>listRes=NoeudCarte.minimaxChemin(racin,depth,equipe,equipe);
            if(listRes.size()==0){
                //si il existe pas de chemin
                if(depth!=0){

                    int index = (int) (Math.random()* fils.size());
                    res=(NoeudCarte)racin.fils.get(index);

                    return res.getInfoCarteEtDeplace().getInfoDepalce();
                }else{

                    return racin.getInfoCarteEtDeplace().getInfoDepalce();
                }
            }else{
                //sinon on rendre prochaine etape
                res = listRes.get(listRes.size()-2);

                return res.getInfoCarteEtDeplace().getInfoDepalce();
            }
        }
    }

    public static void main(String[] args){
        int myC[][] = new int[10][10];
        for(int i=0;i<=9;i++){
            for(int j=0;j<=9;j++){
                myC[i][j]=0;
            }
        }
        myC[3][3]=16;
        myC[3][7]=26;
        NoeudCarte racin=new NoeudCarte(new CarteAvecInfoD(myC));
        int depth=5;
        int equipe=2;
        racin=racin.geneArbreDeNoeudDeCarteAvecFront(racin,depth,equipe);
        racin.donnerNotePourLesFeuilBas(racin.getInfoCarteEtDeplace(),equipe);
        //racin.parcourArbreAvecVal();

        NoeudCarte.afficherListNoeudCarte(NoeudCarte.minimaxChemin(racin,depth,equipe,equipe));
        System.out.println("minmaxBestVal : "+ NoeudCarte.minimaxVal(racin,depth,equipe,equipe));

        InfoD res=racin.chercheEtapeProchaine(myC,depth,equipe,1);
        res.afficher(res);
    }
}
