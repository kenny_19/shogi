package Modele.Strategie;

import java.util.HashMap;

/**
 * cette class pour compter les piece dans la carte et calculer la note pour strategie minmax
 * */
public class CompteurNote{
    private HashMap<Integer,Integer>carte1Equipe1;
    private HashMap<Integer,Integer>carte1Equipe2;
    private HashMap<Integer,Integer>carte2Equipe1;
    private HashMap<Integer,Integer>carte2Equipe2;

    public void iniCompteur(){
        carte1Equipe1=new HashMap<Integer, Integer>();
        carte1Equipe2=new HashMap<Integer, Integer>();
        carte2Equipe1=new HashMap<Integer, Integer>();
        carte2Equipe2=new HashMap<Integer, Integer>();

        for(int i=1;i<=8;i++){
                carte1Equipe1.put(10+i,0);
                carte1Equipe1.put(30+i,0);

                carte1Equipe2.put(20+i,0);
                carte1Equipe2.put(40+i,0);

                carte2Equipe1.put(10+i,0);
                carte2Equipe1.put(30+i,0);

                carte2Equipe2.put(20+i,0);
                carte2Equipe2.put(40+i,0);
        }
    }

    public CompteurNote(){
        iniCompteur();
    }

    //renouvler compteur avec premier carte
    public void renouvlerAvecCarte1(int[][] carte1){
        for(int i=1;i<=9;i++){
            for(int j=1;j<=9;j++){
                if(carte1[j][i]!=0){
                    if(carte1[j][i]/10==1 || carte1[j][i]/10==3){
                        carte1Equipe1.replace(carte1[j][i],carte1Equipe1.get(carte1[j][i]),carte1Equipe1.get(carte1[j][i])+1);
                    }
                    if(carte1[j][i]/10==2 || carte1[j][i]/10==4){
                        carte1Equipe2.replace(carte1[j][i],carte1Equipe2.get(carte1[j][i]),carte1Equipe2.get(carte1[j][i])+1);
                    }
                }
            }
        }
    }

    //renouvler compteur avec deuxieme carte
    public void renouvlerAvecCarte2(int[][] carte1){
        for(int i=1;i<=9;i++){
            for(int j=1;j<=9;j++){
                if(carte1[j][i]!=0){
                    if(carte1[j][i]/10==1 || carte1[j][i]/10==3){
                        carte2Equipe1.replace(carte1[j][i],carte2Equipe1.get(carte1[j][i]),carte2Equipe1.get(carte1[j][i])+1);
                    }
                    if(carte1[j][i]/10==2 || carte1[j][i]/10==4){
                        carte2Equipe2.replace(carte1[j][i],carte2Equipe2.get(carte1[j][i]),carte2Equipe2.get(carte1[j][i])+1);
                    }
                }
            }
        }
    }

    //calculer les notes d'apres les equipe
    public int calculerLesNote(int[][] carte1,int[][] carte2,int equipe){
        renouvlerAvecCarte1(carte1);
        renouvlerAvecCarte2(carte2);
        int note=0;
        if(equipe==1){
            for(int i=1;i<=8;i++){
                //si l'equipe perd les piece,on perd les notes
                note=note-(carte1Equipe1.get(10+i)-carte2Equipe1.get(10+i));
                note=note-(carte1Equipe1.get(30+i)-carte2Equipe1.get(30+i));

                //si l'equipe d'ennemi perd les piece,on prend les notes
                note=note+(carte1Equipe2.get(20+i)-carte2Equipe2.get(20+i));
                note=note+(carte1Equipe2.get(40+i)-carte2Equipe2.get(40+i));
            }
            //si on perd le roi,on perd bcp de note
            if(carte1Equipe1.get(11)-carte2Equipe1.get(11)==1){
                note-=5;
            }else if(carte1Equipe2.get(21) -carte2Equipe2.get(21) ==1){
                //si l'equipe d'ennemi perd le roi,on prend bcp de note
                note+=5;
            }
        }
        if(equipe==2){
            for(int i=1;i<=8;i++){
                //si l'equipe perd les piece,on perd les notes
                note=note+(carte1Equipe1.get(10+i)-carte2Equipe1.get(10+i));
                note=note+(carte1Equipe1.get(30+i)-carte2Equipe1.get(30+i));

                //si l'equipe d'ennemi perd les piece,on prend les notes
                note=note-(carte1Equipe2.get(20+i)-carte2Equipe2.get(20+i));
                note=note-(carte1Equipe2.get(40+i)-carte2Equipe2.get(40+i));
            }
            //si on perd le roi,on perd bcp de note
            if(carte1Equipe2.get(21)-carte2Equipe2.get(21)==1){
                note-=5;
            }else if(carte1Equipe1.get(11)-carte2Equipe1.get(11)==1){
                //si l'equipe d'ennemi perd le roi,on prend bcp de note
                note+=5;
            }
        }
        iniCompteur();
        return note;
    }


    //calculer les notes d'apres les equipe
    public int calculerLesNoteCarteAvecInfo(CarteAvecInfoD ci1,CarteAvecInfoD ci2,int equipe){
        return calculerLesNote(ci1.getCarteProchaine(),ci2.getCarteProchaine(),equipe);
    }
}
