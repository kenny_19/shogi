package Modele.Strategie;

import java.util.ArrayList;

import static java.lang.Math.max;
import static java.lang.Math.min;

/**
 * ce class est test et realiser un arbre basic
 * avec une algo minmax basic
 * */
public class NoeuListEnt extends NoeudList{
    /**
     * c'est le note gain pour le joueur
     * */
    protected int note;

    /**
     * id de noeud augement auto
     */
    static int idCommun=0;

    /**
     * creer un noeud vide
     * */
    public NoeuListEnt(int id,int valeur){
        this.id=id;
        this.note=valeur;
        this.parent=null;
        fils=new ArrayList<NoeudList>();
    }

    /**
     * creer un noeud vide
     * */
    public NoeuListEnt(int valeur){
        this.id=idCommun;
        idCommun++;
        this.note=valeur;
        this.parent=null;
        fils=new ArrayList<NoeudList>();
    }

    /**
     * constructeur par default
     * */
    public NoeuListEnt(){
        this.id=idCommun;
        idCommun++;
        this.note=0;
        this.parent=null;
        fils=new ArrayList<NoeudList>();
    }

    public void setNote(int note) {
        this.note = note;
    }


    /**
     * creer un noeud avec parent
     * */
    public NoeuListEnt(int id,int valeur,NoeuListEnt parent){
        this.id=id;
        this.note=valeur;
        this.setParent(parent);
    }

    /**
     * creer un noeud avec parent
     * */
    public NoeuListEnt(int valeur,NoeuListEnt parent){
        this.id=idCommun;
        idCommun++;
        this.note=valeur;
        this.setParent(parent);
    }


    @Override
    public void parcourArbreAvecVal(){
        System.out.println(this.id + ": " + note);
        if(fils.size()!=0){
            for(NoeudList i:fils){
                i.parcourArbreAvecVal();
            }
        }
    }

    public NoeuListEnt chercheMaxFils(){
        if(fils.size()==0)
            return null;
        else{
            NoeuListEnt temp;
            NoeuListEnt maxFils=(NoeuListEnt)fils.get(0);
            int maxVal=maxFils.note;
            for(int i=0;i<fils.size();i++){
                temp=(NoeuListEnt)fils.get(i);
                if(temp.getNote()>maxVal){
                    maxFils=temp;
                    maxVal=maxFils.note;
                }
            }
            return maxFils;
        }
    }

    public NoeuListEnt chercheMinFils(){
        if(fils.size()==0)
            return null;
        else{
            NoeuListEnt temp;
            NoeuListEnt minFils=(NoeuListEnt)fils.get(0);
            int minVal=minFils.note;
            for(int i=0;i<fils.size();i++){
                temp=(NoeuListEnt)fils.get(i);
                if(temp.getNote()<minVal){
                    minFils=temp;
                    minVal=minFils.note;
                }
            }
            return minFils;
        }
    }


    @Override
    public void afficherMaxFils() {
        NoeuListEnt maxFils=this.chercheMaxFils();
        System.out.println("MaxFils id : "+maxFils.getId()+" valeur : "+maxFils.getNote());
    }

    @Override
    public void afficherMinFils() {
        NoeuListEnt minFils=this.chercheMinFils();
        System.out.println("MinFils id : "+minFils.getId()+" valeur : "+minFils.getNote());
    }

    public int getNote() {
        return note;
    }

    /**
     * On cherche le prochain deplacement avec ce algo.
     * Et par default on retourner la valeur max fils dans le cas termine.
     * Par contre on utilise ce algo seulement dans le tour de eauipe pour les piece.
     * @param n arbre
     * @param depth largeur depart de rasine
     * @param team la piece est dans equipe 1
     * */
    public static int minimaxVal(NoeuListEnt n,int depth,int team){
        if(depth==0){
            return n.getNote();
        }else{
            if(team==1){
                //chercher max
                if(n.fils.size()!=0){
                    NoeuListEnt f = (NoeuListEnt)n.fils.get(0);
                    int myValue;
                    int bestValue = minimaxVal(f,depth-1,2);
                    for (NoeudList fil : n.fils) {
                        f = (NoeuListEnt) fil;
                        myValue = minimaxVal(f, depth - 1, 2);
                        if (myValue > bestValue){
                            bestValue = myValue;
                            //n.setNote(bestValue);
                        }
                    }
                    return bestValue;
                }else
                    return n.getNote();
            }else{
                //chercher min
                if(n.fils.size()!=0){
                    NoeuListEnt f = (NoeuListEnt)n.fils.get(0);
                    int myValue;
                    int bestValue = minimaxVal(f,depth - 1,1);
                    for (NoeudList fil : n.fils) {
                        f = (NoeuListEnt) fil;
                        myValue = minimaxVal(f, depth - 1, 1);
                        if (myValue < bestValue){
                            bestValue = myValue;
                            //n.setNote(bestValue);
                        }
                    }
                    return bestValue;
                }else
                    return n.getNote();
            }
        }
    }

    /**
     * c'est une version améliorer pour algo minmax
     * */
    public static int alphaBeta(NoeuListEnt n,int depth,int alpha,int beta,int team){
        if(depth == 0 || n.fils.size()==0){
            return n.getNote();
        }else{
            if(team == 1){
                //cherche max
                NoeuListEnt f;
                int myValue = -1000;
                for (NoeudList fil : n.fils) {
                    f = (NoeuListEnt) fil;
                    myValue = max(myValue, alphaBeta(f, depth - 1, alpha, beta, 2));
                    alpha = max(alpha,myValue);
                    if(alpha >= beta)
                        break;
                }
                return myValue;
            }else{
                //cherche min
                NoeuListEnt f;
                int myValue = 1000;
                for (NoeudList fil : n.fils) {
                    f = (NoeuListEnt) fil;
                    myValue = min(myValue, alphaBeta(f, depth - 1, alpha, beta, 1));
                    beta = min(beta,myValue);
                    if(alpha >= beta)
                        break;
                }
                return myValue;
            }
        }
    }

    /**
     * On cherche le chemin de racine ver la fils qui a le value cherche par le algo minmax.
     * qui contient avec destination et la rasine.
     * @param n arbre
     * @param depth largeur depart de rasine
     * @param team la piece est dans equipe 1
     * */
    public static ArrayList<NoeuListEnt> minimaxChemin(NoeuListEnt n,int depth,int team){
        int bestVallue=minimaxVal(n,depth,team);
        ArrayList<NoeuListEnt>chemin=new ArrayList<>();
        NoeuListEnt destination=chercheNoeudAvecVal(n,bestVallue);
        if(destination!=null) {
            NoeuListEnt temp=destination;
            for(int i=0;i<=depth;i++) {
                chemin.add(temp);
                if (temp.parent != null)
                    temp = (NoeuListEnt) temp.parent;
            }
        }
        return chemin;
    }

    /**
     * affichier arrilist contient NoeuListEnt
     * */
    public static void afficherList(ArrayList<NoeuListEnt> a){
        if(a.size()!=0){
            NoeuListEnt temp=a.get(0);
            for(int i=0;i<a.size();i++){
                temp=a.get(i);
                System.out.println("ordre de membre : "+ i +" Id : "+temp.getId()+" valeur "+temp.getNote());
            }
        }
    }

    /**
     * cherche un noeud avec son valeur
     * */
    public static NoeuListEnt chercheNoeudAvecVal(NoeuListEnt n,int val){
        if(n.getNote()==val)
            return n;
        else if(n.getNote()!=val && n.fils.size()==0)
            return null;
        else{
            NoeuListEnt res=null;
            NoeuListEnt temp;
            for(int i=0;i<n.fils.size();i++){
                temp=(NoeuListEnt)n.fils.get(i);
                res=chercheNoeudAvecVal(temp,val);
                if(res!=null)
                    return res;
            }
            return res;
        }
    }

    /**
     * vider idCommun
     * */
    public static void viderIdCommun(){
        idCommun=0;
    }

    public static void main(String[] args){
        NoeuListEnt a=new NoeuListEnt(0);

        NoeuListEnt b=new NoeuListEnt(0);
        NoeuListEnt c=new NoeuListEnt(0);

        NoeuListEnt d=new NoeuListEnt(0);
        NoeuListEnt e=new NoeuListEnt(0);
        NoeuListEnt f=new NoeuListEnt(0);
        NoeuListEnt g=new NoeuListEnt(0);

        NoeuListEnt h=new NoeuListEnt(0);
        NoeuListEnt i=new NoeuListEnt(0);
        NoeuListEnt j=new NoeuListEnt(0);
        NoeuListEnt k=new NoeuListEnt(0);
        NoeuListEnt l=new NoeuListEnt(0);
        NoeuListEnt m=new NoeuListEnt(0);


        NoeuListEnt n=new NoeuListEnt(10);
        NoeuListEnt o=new NoeuListEnt(-20);
        NoeuListEnt p=new NoeuListEnt(5);
        NoeuListEnt q=new NoeuListEnt(-10);
        NoeuListEnt r=new NoeuListEnt(7);
        NoeuListEnt s=new NoeuListEnt(5);
        NoeuListEnt t=new NoeuListEnt(-20);
        NoeuListEnt u=new NoeuListEnt(-7);
        NoeuListEnt v=new NoeuListEnt(-5);


        a.insertFils(b);
        a.insertFils(c);

        b.insertFils(d);
        b.insertFils(e);
        c.insertFils(f);
        c.insertFils(g);

        d.insertFils(h);
        d.insertFils(i);
        e.insertFils(j);
        f.insertFils(k);
        f.insertFils(l);
        g.insertFils(m);

        h.insertFils(n);
        h.insertFils(o);
        i.insertFils(p);
        j.insertFils(q);
        k.insertFils(r);
        k.insertFils(s);
        l.insertFils(t);
        m.insertFils(u);
        m.insertFils(v);
/*
*           0
*      /        \
*     0          0
*    / \         / \
*    0   0       0   0
*   / \   |     / \   \
*  0   0  0    0  0     0
* / \   |  |   /\  \    / \
*10 -20 5 -10 7  5 -20 -7 -5
* -----------------------------
* apres algo minmax
* -----------------------------
*          -7
*      /        \
*     -10         -7
*    / \         / \
*    5   -10     5   -7
*   / \   |     / \   \
* -20  5  -10   5 -20   -7
* / \   |  |   /\  \    / \
*10 -20 5 -10 7  5 -20 -7 -5
* */
        a.parcourArbreAvecVal();

        int myMinMax=NoeuListEnt.minimaxVal(a,4,1);
        ArrayList<NoeuListEnt> myChemin=minimaxChemin( a, 4, 1);
        System.out.println("myMinMax is " + myMinMax);
        afficherList(myChemin);
        System.out.println("==============================" );
        int abMinmax=NoeuListEnt.alphaBeta(a,4,-1000,1000,1);
        System.out.println("abMinmax is " + abMinmax);

    }
}
