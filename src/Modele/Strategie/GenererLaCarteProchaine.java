package Modele.Strategie;

import Modele.InfoDeplace.*;


import java.util.ArrayList;

public class GenererLaCarteProchaine {
    /**
    * Eentrer la carte et l'equipe et rendrer la liste de carte possible
     *  apres la deplacement d'une piece de ce equipe avec les information de deplacement.
    * */
    public static ArrayList<CarteAvecInfoD> prochaineCarte(int[][] myCarte, int equipe){
        ArrayList<InfoD>notreEquipe=new ArrayList<InfoD>();
        ArrayList<CarteAvecInfoD> res=new ArrayList<CarteAvecInfoD>();
        int caractere;
        FactoryInfoD factory=new FactoryInfoD();
        //1.parcour la carte et cherche tout les piece de ce equipe et generer la list de infoD
        for(int i=1;i<=9;i++){
            for(int j=1;j<=9;j++){
                if(myCarte[j][i]!=0 && (myCarte[j][i]/10==3|| myCarte[j][i]/10==1) && equipe==1){
                    //avec les case equipe 1
                    //generer infoD pour chaque piece
                    notreEquipe.add(factory.geneInfoD(j,i,myCarte[j][i]));
                }

                if(myCarte[j][i]!=0 && (myCarte[j][i]/10==4|| myCarte[j][i]/10==2) && equipe==2){
                    //avec les case equipe 2
                    //generer infoD pour chaque piece
                    //chauqe infoD correspond a une piece
                    notreEquipe.add(factory.geneInfoD(j,i,myCarte[j][i]));
                }
            }
        }
        /*
        * debuger afficher nombre de piece dans la list
        * */
        //afficherListInfoD(notreEquipe);
        InfoD temp;
        ArrayList<CarteAvecInfoD>tempList;
        //2.parcour liste infoD, pour chaque infoD,genere la liste de CarteAvecInfoD.
        for(int i=0;i<notreEquipe.size();i++){
            temp=notreEquipe.get(i);
            tempList=temp.geneAutoDeplacement3(myCarte);
            res.addAll(tempList);
        }
        return res;
    }

    /**
     * afficher list infoD
     * */
    public static void afficherListInfoD(ArrayList<InfoD>l){
        System.out.println("listeInfoD-------");
        System.out.println(" size de list InfoD : "+l.size());
        for(InfoD i: l)
            i.afficher(i);
        System.out.println();
    }


    public static void main(String[] args){
        int myC[][] = new int[10][10];
        int myC2[][] = new int[10][10];
        for(int i=0;i<=9;i++){
            for(int j=0;j<=9;j++){
                myC[i][j]=0;
                myC2[i][j]=0;
            }
        }
        myC[7][7]=16;
        myC[2][2]=25;
        ArrayList<CarteAvecInfoD>nouvCarte=GenererLaCarteProchaine.prochaineCarte(myC,2);
        CarteAvecInfoD temp;
        System.out.println(" size of list : "+nouvCarte.size());
        for(int i=0;i<nouvCarte.size();i++){
            temp=nouvCarte.get(i);
            temp.afficher();
        }

        CompteurNote compt=new CompteurNote();
        CarteAvecInfoD c1=new CarteAvecInfoD(myC);
        myC2[7][7]=myC[2][2];
        myC2[2][2]=0;
        CarteAvecInfoD c2=new CarteAvecInfoD(myC2);
        c1.afficher();
        c2.afficher();
        int note=compt.calculerLesNoteCarteAvecInfo(c1,c2,1);
        System.out.println("note equipe 1 : "+note);
    }
}
