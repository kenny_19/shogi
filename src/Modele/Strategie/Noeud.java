package Modele.Strategie;
/**
 * ce class est arbre normal pour montrer le resultat de calculer avec algo min max
 * */
public interface Noeud {
    /**
     * obtenir le numero id de ce noeud
     * */
    public int getId();
    /**
     * saisir numero id de ce noeud
     * */
    public void setId(int id);
    /**
     * retourner nombre de fils de ce noeud
     */
    public int getSize();
    /**
     * retourner hauteur de ce noeud
     */
    public int getHeight();
    /**
     * retourner profondeur de ce noeud
     */
    public int getDepth();
}
