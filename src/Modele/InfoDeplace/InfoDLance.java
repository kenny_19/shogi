package Modele.InfoDeplace;

import Modele.Strategie.CarteAvecInfoD;

import java.util.ArrayList;

public class InfoDLance extends InfoD{
    public InfoDLance(int x1, int y1, int deltaX1, int deltaY1, int etat) {
        super(x1, y1, deltaX1, deltaY1, etat);
    }

    public InfoDLance(int x, int y, int etat) {
        super(x, y, etat);
    }

    @Override
    public ArrayList<InfoD> geneAutoDeplacement1(int[][] m) {
        //si il est deja mange
        if(x==0 && y==0 ) return null;
        if(etatP==37 || etatP==47){
            return geneDeplacementCommeGold1(m);
        }
        ArrayList<InfoD> res=new ArrayList<InfoD>();
        int bloque=0;
        if(etatP==17){
            for(int i=1;i<=8;i++){
                if(y+i<=9 && y+i>=1 && bloque==0 ){
                    if(m[x][y+i]==0)
                        res.add(new InfoDLance(x, y, 0, i, etatP));
                    else if(!verifierMemeEquipe(m[x][y+i])) {
                        res.add(new InfoDLance(x, y, 0, i, etatP));
                        bloque=1;
                    }else
                        bloque=1;
                }
            }
        }else if(etatP==27){
            for(int i=1;i<=8;i++){
                if(y-i<=9 && y-i>=1 && bloque==0 ){
                    if(m[x][y-i]==0)
                        res.add(new InfoDLance(x,y,0,-i,etatP));
                    else if(!verifierMemeEquipe(m[x][y-i])){
                        res.add(new InfoDLance(x,y,0,-i,etatP));
                        bloque=1;
                    }
                    else
                        bloque=1;
                }
            }
        }
        return res;
    }

    @Override
    public ArrayList<InfoD> geneAutoDeplacement2(int[][] m) {
        //si il est deja mange
        if(x==0 && y==0 ) return null;
        if(etatP==37 || etatP==47){
            return geneDeplacementCommeGold2(m);
        }
        ArrayList<InfoD> res=new ArrayList<InfoD>();
        int bloque=0;
        if(etatP==17){
            for(int i=1;i<=7;i++){
                if(y+i<=9 && y+i>=1 && bloque==0 ){
                    if(m[x][y+i]==0){
                        //si bouge
                        InfoDLance myCase=new InfoDLance(x, y, 0, i, etatP);
                        myCase.setPoint(0);
                        res.add(myCase);
                    }
                    else if(!verifierMemeEquipe(m[x][y+i])) {
                        //si mange
                        InfoDLance myCase=new InfoDLance(x, y, 0, i, etatP);
                        myCase.setPoint(1);
                        res.add(myCase);
                        bloque=1;
                    }else
                        bloque=1;
                }
            }
        }else if(etatP==27){
            for(int i=1;i<=7;i++){
                if(y-i<=9 && y-i>=1 && bloque==0 ){
                    if(m[x][y-i]==0){
                        InfoDLance myCase=new InfoDLance(x,y,0,-i,etatP);
                        myCase.setPoint(0);
                        res.add(myCase);
                    }
                    else if(!verifierMemeEquipe(m[x][y-i])){
                        InfoDLance myCase=new InfoDLance(x,y,0,-i,etatP);
                        myCase.setPoint(1);
                        res.add(myCase);
                        bloque=1;
                    }
                    else
                        bloque=1;
                }
            }
        }
        return res;
    }

    @Override
    public ArrayList<CarteAvecInfoD> geneAutoDeplacement3(int[][] m) {
        ArrayList<InfoD>listInfoD=geneAutoDeplacement1(m);
        ArrayList<CarteAvecInfoD>listRes=new ArrayList<CarteAvecInfoD>();
        InfoDLance temp;
        for(int i=0;i<listInfoD.size();i++){
            temp=(InfoDLance) listInfoD.get(i);
            listRes.add(new CarteAvecInfoD(temp.genererProchaineCarte(m,temp),temp));
        }
        return listRes;
    }


    public ArrayList<InfoD> geneDeplacementCommeGold1(int[][] m) {
        ArrayList<InfoD> res=new ArrayList<InfoD>();
        switch (this.etatP/10){
            case 1:
                if(x-1<=9 && x-1>=1){
                    if(!verifierMemeEquipe(m[x-1][y]))
                        res.add(new InfoDLance(x,y,-1,0,etatP));
                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x-1][y+1]))
                        res.add(new InfoDLance(x,y,-1,1,etatP));
                }

                if(x+1<=9 && x+1>=1){
                    if(!verifierMemeEquipe(m[x+1][y]))
                        res.add(new InfoDLance(x,y,1,0,etatP));
                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x+1][y+1]))
                        res.add(new InfoDLance(x,y,1,1,etatP));
                }

                if(x<=9 && x>=1){
                    if(y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x][y-1]))
                        res.add(new InfoDLance(x,y,0,-1,etatP));
                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x][y+1]))
                        res.add(new InfoDLance(x,y,0,1,etatP));
                }

                break;
            case 2:
                if(x-1<=9 && x-1>=1){
                    if(!verifierMemeEquipe(m[x-1][y]))
                        res.add(new InfoDLance(x,y,-1,0,etatP));
                    if(y-1<=9 && y-1>=1&& !verifierMemeEquipe(m[x-1][y-1]))
                        res.add(new InfoDLance(x,y,-1,-1,etatP));
                }

                if(x+1<=9 && x+1>=1){
                    if(!verifierMemeEquipe(m[x+1][y]))
                        res.add(new InfoDLance(x,y,1,0,etatP));
                    if(y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x+1][y-1]))
                        res.add(new InfoDLance(x,y,1,-1,etatP));
                }

                if(x<=9 && x>=1){
                    if(y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x][y-1]))
                        res.add(new InfoDLance(x,y,0,-1,etatP));
                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x][y+1]))
                        res.add(new InfoDLance(x,y,0,1,etatP));
                }

                break;
            default:
                break;
        }
        return res;
    }

    /**
     * generer les case deplacement possible avec gold avec note de priorite
     * */
    private ArrayList<InfoD> geneDeplacementCommeGold2(int[][] m) {
        ArrayList<InfoD> res=new ArrayList<InfoD>();
        switch (this.etatP/10){
            case 1:
                if(x-1<=9 && x-1>=1){
                    if(!verifierMemeEquipe(m[x-1][y])){
                        InfoDGold myCase=new InfoDGold(x,y,-1,0,etatP);
                        if(m[x-1][y]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }
                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x-1][y+1])){
                        InfoDGold myCase=new InfoDGold(x,y,-1,1,etatP);
                        if(m[x-1][y+1]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }

                }

                if(x+1<=9 && x+1>=1){
                    if(!verifierMemeEquipe(m[x+1][y])){
                        InfoDGold myCase=new InfoDGold(x,y,1,0,etatP);
                        if(m[x+1][y]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }
                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x+1][y+1])){
                        InfoDGold myCase=new InfoDGold(x,y,1,1,etatP);
                        if(m[x+1][y+1]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }

                }

                if(x<=9 && x>=1){
                    if(y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x][y-1])){
                        InfoDGold myCase=new InfoDGold(x,y,0,-1,etatP);
                        if(m[x][y-1]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }

                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x][y+1])){
                        InfoDGold myCase=new InfoDGold(x,y,0,1,etatP);
                        if(m[x][y+1]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }

                }

                break;
            case 2:
                if(x-1<=9 && x-1>=1){
                    if(!verifierMemeEquipe(m[x-1][y])){
                        InfoDGold myCase=new InfoDGold(x,y,-1,0,etatP);
                        if(m[x-1][y]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }

                    if(y-1<=9 && y-1>=1&& !verifierMemeEquipe(m[x-1][y-1])){
                        InfoDGold myCase=new InfoDGold(x,y,-1,-1,etatP);
                        if(m[x-1][y-1]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }

                }

                if(x+1<=9 && x+1>=1){
                    if(!verifierMemeEquipe(m[x+1][y])){
                        InfoDGold myCase=new InfoDGold(x,y,1,0,etatP);
                        if(m[x+1][y]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }

                    if(y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x+1][y-1])){
                        InfoDGold myCase=new InfoDGold(x,y,1,-1,etatP);
                        if(m[x+1][y-1]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }

                }

                if(x<=9 && x>=1){
                    if(y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x][y-1])){
                        InfoDGold myCase=new InfoDGold(x,y,0,-1,etatP);
                        if(m[x][y-1]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }

                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x][y+1])){
                        InfoDGold myCase=new InfoDGold(x,y,0,1,etatP);
                        if(m[x][y+1]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }
                }

                break;
            default:
                break;
        }
        return res;
    }

    public ArrayList<InfoD> genePromotion(int[][] m) {
        ArrayList<InfoD> res=new ArrayList<InfoD>();
        // si dans la zone de promotion
        if(x>=4 && x<=7) {
            switch (this.etatP) {
                case 17:
                    etatP = 37;
                    res.add(new InfoDLance(x, y, 0, 0, etatP));
                case 27:
                    etatP = 47;
                    res.add(new InfoDLance(x, y, 0, 0, etatP));
                default:
                    ;
            }
        }
        return res;
    }
}
