package Modele.InfoDeplace;

import Modele.Strategie.CarteAvecInfoD;

import java.util.ArrayList;

public class InfoDBishop extends InfoD{

    public InfoDBishop(int x1, int y1, int deltaX1, int deltaY1, int etat) {
        super(x1, y1, deltaX1, deltaY1, etat);
    }

    public InfoDBishop(int x, int y, int etat) {
        super(x, y, etat);
    }

    @Override
    public ArrayList<InfoD> geneAutoDeplacement1(int[][] m) {
        //si il est deja mange
        if(x==0 && y==0 ) return null;
        ArrayList<InfoD> res=new ArrayList<InfoD>();
        switch (this.etatP){
            case 16:
            case 26:
            case 36:
            case 46:
                int pieceExiste1=0;
                int pieceExiste2=0;
                int pieceExiste3=0;
                int pieceExiste4=0;
                //generer possible deplacement avec les cas normal
                for(int i=1;i<=8;i++){
                    if(x+i<=9 && x+i>=1){
                        //test dans la carte
                        if(y+i<=9 && y+i>=1){
                            //verifer il n'y a pas de piece entre lieu de depart et destination
                            if(pieceExiste1==0){
                                if(m[x+i][y+i]==0){
                                    res.add(new InfoDBishop(x,y,i,i,etatP));
                                }else if(!verifierMemeEquipe(m[x+i][y+i])){
                                    res.add(new InfoDBishop(x,y,i,i,etatP));
                                    pieceExiste1=1;
                                }else{
                                    pieceExiste1=1;
                                }
                            }
                        }

                        if(y-i<=9 && y-i>=1){
                            //verifer il n'y a pas de piece entre lieu de depart et destination
                            if(pieceExiste2==0){
                                if(m[x+i][y-i]==0){
                                    res.add(new InfoDBishop(x,y,i,-i,etatP));
                                }else if(!verifierMemeEquipe(m[x+i][y-i])){
                                    res.add(new InfoDBishop(x,y,i,-i,etatP));
                                    pieceExiste2=1;
                                }else{
                                    pieceExiste2=1;
                                }
                            }
                        }
                    }

                    if(x-i<=9 && x-i>=1){
                        //test dans la carte
                        if(y+i<=9 && y+i>=1){
                            //verifer il n'y a pas de piece entre lieu de depart et destination
                            if(pieceExiste3==0){
                                if(m[x-i][y+i]==0){
                                    res.add(new InfoDBishop(x,y,-i,i,etatP));
                                }else if(!verifierMemeEquipe(m[x-i][y+i])){
                                    res.add(new InfoDBishop(x,y,-i,i,etatP));
                                    pieceExiste3=1;
                                }else{
                                    pieceExiste3=1;
                                }
                            }
                        }

                        if(y-i<=9 && y-i>=1){
                            //verifer il n'y a pas de piece entre lieu de depart et destination
                            if(pieceExiste4==0){
                                if(m[x-i][y-i]==0){
                                    res.add(new InfoDBishop(x,y,-i,-i,etatP));
                                }else if(!verifierMemeEquipe(m[x-i][y-i])){
                                    res.add(new InfoDBishop(x,y,-i,-i,etatP));
                                    pieceExiste4=1;
                                }else{
                                    pieceExiste4=1;
                                }
                            }
                        }
                    }
                }
                //generer possible deplacement avec les cas promo
                if(etatP==38 || etatP==48){
                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x][y+1]))
                        res.add(new InfoDBishop(x,y,0,1,etatP));
                    if(y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x][y-1]))
                        res.add(new InfoDBishop(x,y,0,-1,etatP));
                    if(x+1<=9 && x+1>=1 && !verifierMemeEquipe(m[x+1][y]))
                        res.add(new InfoDBishop(x,y,1,0,etatP));
                    if(x-1<=9 && x-1>=1 && !verifierMemeEquipe(m[x-1][y]))
                        res.add(new InfoDBishop(x,y,-1,0,etatP));
                }

            default:;
        }
        return res;
    }

    @Override
    public ArrayList<InfoD> geneAutoDeplacement2(int[][] m) {
        //si il est deja mange
        if(x==0 && y==0 ) return null;
        ArrayList<InfoD> res=new ArrayList<InfoD>();
        switch (this.etatP){
            case 16:
            case 26:
            case 36:
            case 46:
                int pieceExiste1=0;
                int pieceExiste2=0;
                int pieceExiste3=0;
                int pieceExiste4=0;
                //generer possible deplacement avec les cas normal
                for(int i=1;i<=8;i++){
                    if(x+i<=9 && x+i>=1){
                        //test dans la carte
                        if(y+i<=9 && y+i>=1){
                            //verifer il n'y a pas de piece entre lieu de depart et destination
                            if(pieceExiste1==0){
                                if(m[x+i][y+i]==0){
                                    //pour les case bouge
                                    InfoDBishop myCase=new InfoDBishop(x,y,i,i,etatP);
                                    myCase.setPoint(0);
                                    res.add(myCase);
                                }else if(verifierDautreEquipe(m[x+i][y+i])){
                                    //pour les case mange
                                    InfoDBishop myCase=new InfoDBishop(x,y,i,i,etatP);
                                    myCase.setPoint(1);
                                    res.add(myCase);
                                    pieceExiste1=1;
                                }else{
                                    pieceExiste1=1;
                                }
                            }
                        }

                        if(y-i<=9 && y-i>=1){
                            //verifer il n'y a pas de piece entre lieu de depart et destination
                            if(pieceExiste2==0){
                                if(m[x+i][y-i]==0){
                                    //pour les case bouge
                                    InfoDBishop myCase=new InfoDBishop(x,y,i,-i,etatP);
                                    myCase.setPoint(0);
                                    res.add(myCase);
                                }else if(verifierDautreEquipe(m[x+i][y-i])){
                                    //pour les case mange
                                    InfoDBishop myCase=new InfoDBishop(x,y,i,-i,etatP);
                                    myCase.setPoint(1);
                                    res.add(myCase);
                                    pieceExiste2=1;
                                }else{
                                    pieceExiste2=1;
                                }
                            }
                        }
                    }

                    if(x-i<=9 && x-i>=1){
                        //test dans la carte
                        if(y+i<=9 && y+i>=1){
                            //verifer il n'y a pas de piece entre lieu de depart et destination
                            if(pieceExiste3==0){
                                if(m[x-i][y+i]==0){
                                    //pour les case bouge
                                    InfoDBishop myCase=new InfoDBishop(x,y,-i,i,etatP);
                                    myCase.setPoint(0);
                                    res.add(myCase);
                                }else if(verifierDautreEquipe(m[x-i][y+i])){
                                    //pour les case mange
                                    InfoDBishop myCase=new InfoDBishop(x,y,-i,i,etatP);
                                    myCase.setPoint(1);
                                    res.add(myCase);
                                    pieceExiste3=1;
                                }else{
                                    pieceExiste3=1;
                                }
                            }
                        }

                        if(y-i<=9 && y-i>=1){
                            //verifer il n'y a pas de piece entre lieu de depart et destination
                            if(pieceExiste4==0){
                                if(m[x-i][y-i]==0){
                                    //pour les case bouge
                                    InfoDBishop myCase=new InfoDBishop(x,y,-i,-i,etatP);
                                    myCase.setPoint(0);
                                    res.add(myCase);
                                }else if(verifierDautreEquipe(m[x-i][y-i])){
                                    //pour les case mange
                                    InfoDBishop myCase=new InfoDBishop(x,y,-i,-i,etatP);
                                    myCase.setPoint(1);
                                    res.add(myCase);
                                    pieceExiste4=1;
                                }else{
                                    pieceExiste4=1;
                                }
                            }
                        }
                    }
                }
                //generer possible deplacement avec les cas apres promo
                if(etatP==38 || etatP==48){
                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x][y+1])){
                        InfoDBishop myCase=new InfoDBishop(x,y,0,1,etatP);
                        if(m[x][y+1]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }
                    if(y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x][y-1])){
                        InfoDBishop myCase=new InfoDBishop(x,y,0,-1,etatP);
                        if(m[x][y-1]==0)
                            myCase.setPoint(0);
                        else
                            myCase.setPoint(1);
                        res.add(myCase);
                    }
                    if(x+1<=9 && x+1>=1 && !verifierMemeEquipe(m[x+1][y])){
                        InfoDBishop myCase= new InfoDBishop(x,y,1,0,etatP);
                        if(m[x+1][y]==0)
                            myCase.setPoint(0);
                        else
                            myCase.setPoint(1);
                        res.add(myCase);
                    }

                    if(x-1<=9 && x-1>=1 && !verifierMemeEquipe(m[x-1][y])){
                        InfoDBishop myCase =new InfoDBishop(x,y,-1,0,etatP);
                        if(m[x-1][y]==0)
                            myCase.setPoint(0);
                        else
                            myCase.setPoint(1);
                        res.add(myCase);
                    }
                }

            default:;
        }
        return res;
    }

    @Override
    public ArrayList<CarteAvecInfoD> geneAutoDeplacement3(int[][] m) {
        ArrayList<InfoD>listInfoD=geneAutoDeplacement1(m);
        ArrayList<CarteAvecInfoD>listRes=new ArrayList<CarteAvecInfoD>();
        InfoDBishop temp;
        for(int i=0;i<listInfoD.size();i++){
            temp=(InfoDBishop) listInfoD.get(i);
            listRes.add(new CarteAvecInfoD(temp.genererProchaineCarte(m,temp),temp));
        }
        return listRes;
    }


    public ArrayList<InfoD> genePromotion(int[][] m) {
        ArrayList<InfoD> res=new ArrayList<InfoD>();
        // si dans la zone de promotion
        if(x>=4 && x<=7) {
            switch (this.etatP) {
                case 18:
                    etatP = 38;
                    res.add(new InfoDBishop(x, y, 0, 0, etatP));
                case 28:
                    etatP = 48;
                    res.add(new InfoDBishop(x, y, 0, 0, etatP));
                default:
                    ;
            }
        }
        return res;
    }
}
