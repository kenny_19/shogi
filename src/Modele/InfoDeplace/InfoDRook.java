package Modele.InfoDeplace;

import Modele.Strategie.CarteAvecInfoD;

import java.util.ArrayList;

public class InfoDRook extends InfoD{

    public InfoDRook(int x1, int y1, int deltaX1, int deltaY1, int etat) {
        super(x1, y1, deltaX1, deltaY1, etat);
    }

    public InfoDRook(int x, int y, int etat) {
        super(x, y, etat);
    }

    @Override
    public ArrayList<InfoD> geneAutoDeplacement1(int[][] m) {
        //si il est deja mange
        if(x==0 && y==0 ) return null;
        ArrayList<InfoD> res=new ArrayList<InfoD>();
        switch (etatP){
            case 15:
            case 25:
            case 35:
            case 45:
                int pieceExiste1=0;
                int pieceExiste2=0;
                int pieceExiste3=0;
                int pieceExiste4=0;
                for(int i=1;i<=7;i++){

                    if(x+i<=9 && x+i>=1){
                        if(pieceExiste1==0) {
                            if (m[x + i][y] == 0) {
                                res.add(new InfoDRook(x, y, i, 0, etatP));
                            } else if (!verifierMemeEquipe(m[x + i][y])) {
                                res.add(new InfoDRook(x, y, i, 0, etatP));
                                pieceExiste1 = 1;
                            } else {
                                pieceExiste1 = 1;
                            }
                        }
                    }

                    if(x-i<=9 && x-i>=1){
                        if(pieceExiste2==0) {
                            if (m[x - i][y] == 0) {
                                res.add(new InfoDRook(x, y, -i, 0, etatP));
                            } else if (!verifierMemeEquipe(m[x - i][y])) {
                                res.add(new InfoDRook(x, y, -i, 0, etatP));
                                pieceExiste2 = 1;
                            } else {
                                pieceExiste2 = 1;
                            }
                        }
                    }

                    if(y+i<=9 && y+i>=1){
                        if(pieceExiste3==0) {
                            if (m[x][y+ i] == 0) {
                                res.add(new InfoDRook(x, y, 0, i, etatP));
                            } else if (!verifierMemeEquipe(m[x ][y+ i])) {
                                res.add(new InfoDRook(x, y, 0, i, etatP));
                                pieceExiste3 = 1;
                            } else {
                                pieceExiste3 = 1;
                            }
                        }
                    }

                    if(y-i<=9 && y-i>=1){
                        if(pieceExiste4==0) {
                            if (m[x][y- i] == 0) {
                                res.add(new InfoDRook(x, y, 0, -i, etatP));
                            } else if (!verifierMemeEquipe(m[x ][y- i])) {
                                res.add(new InfoDRook(x, y, 0, -i, etatP));
                                pieceExiste4 = 1;
                            } else {
                                pieceExiste4 = 1;
                            }
                        }
                    }
                }
                //pour les cas promo
                if(etatP == 35 ||etatP == 45){
                    if(x+1<=9 && x+1>=1 && y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x+1][y+1]))
                        res.add(new InfoDRook(x,y,1,1,etatP));
                    if(x+1<=9 && x+1>=1 && y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x+1][y-1]))
                        res.add(new InfoDRook(x,y,1,-1,etatP));
                    if(x-1<=9 && x-1>=1 && y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x-1][y-1]))
                        res.add(new InfoDRook(x,y,-1,-1,etatP));
                    if(x-1<=9 && x-1>=1 && y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x-1][y+1]))
                        res.add(new InfoDRook(x,y,-1,1,etatP));
                }
            default:;
        }
        return res;
    }

    @Override
    public ArrayList<InfoD> geneAutoDeplacement2(int[][] m) {
        //si il est deja mange
        if(x==0 && y==0 ) return null;
        ArrayList<InfoD> res=new ArrayList<InfoD>();
        switch (etatP){
            case 15:
            case 25:
            case 35:
            case 45:
                int pieceExiste1=0;
                int pieceExiste2=0;
                int pieceExiste3=0;
                int pieceExiste4=0;
                for(int i=1;i<=8;i++){

                    if(x+i<=9 && x+i>=1){
                        if(pieceExiste1==0) {
                            if (m[x + i][y] == 0) {
                                //si bouge
                                InfoDRook myCase=new InfoDRook(x, y, i, 0, etatP);
                                myCase.setPoint(0);
                                res.add(myCase);
                            } else if (verifierDautreEquipe(m[x + i][y])) {
                                //si mange
                                InfoDRook myCase=new InfoDRook(x, y, i, 0, etatP);
                                myCase.setPoint(1);
                                res.add(myCase);
                                pieceExiste1 = 1;
                            } else {
                                pieceExiste1 = 1;
                            }
                        }
                    }

                    if(x-i<=9 && x-i>=1){
                        if(pieceExiste2==0) {
                            if (m[x - i][y] == 0) {
                                //si bouge
                                InfoDRook myCase=new InfoDRook(x, y, -i, 0, etatP);
                                myCase.setPoint(0);
                                res.add(myCase);
                            } else if (verifierDautreEquipe(m[x - i][y])) {
                                //si mange
                                InfoDRook myCase=new InfoDRook(x, y, -i, 0, etatP);
                                myCase.setPoint(1);
                                res.add(myCase);
                                pieceExiste2 = 1;
                            } else {
                                pieceExiste2 = 1;
                            }
                        }
                    }

                    if(y+i<=9 && y+i>=1){
                        if(pieceExiste3==0) {
                            if (m[x][y+ i] == 0) {
                                //si bouge
                                InfoDRook myCase=new InfoDRook(x, y, 0, i, etatP);
                                myCase.setPoint(0);
                                res.add(myCase);
                            } else if (verifierDautreEquipe(m[x ][y+ i])) {
                                //si mange
                                InfoDRook myCase=new InfoDRook(x, y, 0, i, etatP);
                                myCase.setPoint(1);
                                res.add(myCase);
                                pieceExiste3 = 1;
                            } else {
                                pieceExiste3 = 1;
                            }
                        }
                    }

                    if(y-i<=9 && y-i>=1){
                        if(pieceExiste4==0) {
                            if (m[x][y- i] == 0) {
                                //si bouge
                                InfoDRook myCase=new InfoDRook(x, y, 0, -i, etatP);
                                myCase.setPoint(0);
                                res.add(myCase);
                            } else if (verifierDautreEquipe(m[x ][y- i])) {
                                //si mange
                                InfoDRook myCase=new InfoDRook(x, y, 0, -i, etatP);
                                myCase.setPoint(1);
                                res.add(myCase);
                                pieceExiste4 = 1;
                            } else {
                                pieceExiste4 = 1;
                            }
                        }
                    }
                }
                //pour les cas promo
                if(etatP == 35 ||etatP == 45){
                    if(x+1<=9 && x+1>=1 && y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x+1][y+1])){
                        InfoDRook myCase=new InfoDRook(x,y,1,1,etatP);
                        if(m[x+1][y+1]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }

                    if(x+1<=9 && x+1>=1 && y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x+1][y-1])){
                        InfoDRook myCase=new InfoDRook(x,y,1,-1,etatP);
                        if(m[x+1][y-1]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }

                    if(x-1<=9 && x-1>=1 && y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x-1][y-1])){
                        InfoDRook myCase=new InfoDRook(x,y,-1,-1,etatP);
                        if(m[x-1][y-1]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }

                    if(x-1<=9 && x-1>=1 && y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x-1][y+1])){
                        InfoDRook myCase=new InfoDRook(x,y,-1,1,etatP);
                        if(m[x-1][y+1]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }

                }
            default:;
        }
        return res;
    }

    @Override
    public ArrayList<CarteAvecInfoD> geneAutoDeplacement3(int[][] m) {
        ArrayList<InfoD>listInfoD=geneAutoDeplacement1(m);
        ArrayList<CarteAvecInfoD>listRes=new ArrayList<>();
        InfoDRook temp;
        for(int i=0;i<listInfoD.size();i++){
            temp=(InfoDRook) listInfoD.get(i);
            listRes.add(new CarteAvecInfoD(temp.genererProchaineCarte(m,temp),temp));
        }
        return listRes;
    }


    public ArrayList<InfoD> genePromotion(int[][] m) {
        ArrayList<InfoD> res=new ArrayList<InfoD>();
        // si dans la zone de promotion
        if(x>=4 && x<=7) {
            switch (this.etatP) {
                case 15:
                    etatP = 35;
                    res.add(new InfoDRook(x, y, 0, 0, etatP));
                case 25:
                    etatP = 45;
                    res.add(new InfoDRook(x, y, 0, 0, etatP));
                default:
                    ;
            }
        }
        return res;
    }
}
