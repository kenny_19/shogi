package Modele.InfoDeplace;

import Modele.Strategie.CarteAvecInfoD;

import java.util.ArrayList;

public class InfoDSilver extends InfoD{
    public InfoDSilver(int x1, int y1, int deltaX1, int deltaY1, int etat) {
        super(x1, y1, deltaX1, deltaY1, etat);
    }

    public InfoDSilver(int x, int y, int etat) {
        super(x, y, etat);
    }

    @Override
    public ArrayList<InfoD> geneAutoDeplacement1(int[][] m) {
        //si il est deja mange
        if(x==0 && y==0 ) return null;
        if(etatP==33 || etatP==43){
            return geneDeplacementCommeGold1(m);
        }
        ArrayList<InfoD> res= new ArrayList<InfoD>();
        switch (this.etatP/10){
            case 1:
                if(x-1<=9 && x-1>=1){
                    if(y-1<=9 && y-1>=1 &&!verifierMemeEquipe(m[x-1][y-1]))
                        res.add(new InfoDSilver(x,y,-1,-1,etatP));
                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x-1][y+1]))
                        res.add(new InfoDSilver(x,y,-1,1,etatP));
                }

                if(x+1<=9 && x+1>=1){
                    if(y-1<=9 && y-1>=1 &&!verifierMemeEquipe(m[x+1][y-1]))
                        res.add(new InfoDSilver(x,y,1,-1,etatP));
                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x+1][y+1]))
                        res.add(new InfoDSilver(x,y,1,1,etatP));
                }

                if(x<=9 && x>=1){
                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x][y+1]))
                        res.add(new InfoDSilver(x,y,0,1,etatP));
                }

                break;
            case 2:
                if(x-1<=9 && x-1>=1){
                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x-1][y+1]))
                        res.add(new InfoDSilver(x,y,-1,1,etatP));
                    if(y-1<=9 && y-1>=1&& !verifierMemeEquipe(m[x-1][y-1]))
                        res.add(new InfoDSilver(x,y,-1,-1,etatP));
                }

                if(x+1<=9 && x+1>=1){
                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x+1][y+1]))
                        res.add(new InfoDSilver(x,y,1,1,etatP));
                    if(y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x+1][y-1]))
                        res.add(new InfoDSilver(x,y,1,-1,etatP));
                }

                if(x<=9 && x>=1){
                    if(y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x][y-1]))
                        res.add(new InfoDSilver(x,y,0,-1,etatP));
                }

                break;
            default:
                break;
        }
        return res;
    }

    @Override
    public ArrayList<InfoD> geneAutoDeplacement2(int[][] m) {
        //si il est deja mange
        if(x==0 && y==0 ) return null;
        if(etatP==33 || etatP==43){
            return geneDeplacementCommeGold2(m);
        }
        ArrayList<InfoD> res= new ArrayList<InfoD>();
        switch (this.etatP/10){
            case 1:
                //pour eauipe 1
                if(x-1<=9 && x-1>=1){
                    if(y-1<=9 && y-1>=1 &&!verifierMemeEquipe(m[x-1][y-1])){
                        InfoDSilver myCase=new InfoDSilver(x,y,-1,-1,etatP);
                        if(m[x-1][y-1]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            myCase.setPoint(1);
                        res.add(myCase);
                    }
                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x-1][y+1])){
                        InfoDSilver myCase=new InfoDSilver(x,y,-1,1,etatP);
                        if(m[x-1][y+1]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            myCase.setPoint(1);
                        res.add(myCase);
                    }

                }

                if(x+1<=9 && x+1>=1){
                    if(y-1<=9 && y-1>=1 &&!verifierMemeEquipe(m[x+1][y-1])){
                        InfoDSilver myCase=new InfoDSilver(x,y,1,-1,etatP);
                        if(m[x+1][y-1]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            myCase.setPoint(1);
                        res.add(myCase);
                    }

                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x+1][y+1])){
                        InfoDSilver myCase=new InfoDSilver(x,y,1,1,etatP);
                        if(m[x+1][y+1]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            myCase.setPoint(1);
                        res.add(myCase);
                    }

                }

                if(x<=9 && x>=1){
                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x][y+1])){
                        InfoDSilver myCase=new InfoDSilver(x,y,0,1,etatP);
                        if(m[x][y+1]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            myCase.setPoint(1);
                        res.add(myCase);
                    }

                }

                break;
            case 2:
                //pour eauipe2
                if(x-1<=9 && x-1>=1){
                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x-1][y+1])){
                        res.add(new InfoDSilver(x,y,-1,1,etatP));
                    }

                    if(y-1<=9 && y-1>=1&& !verifierMemeEquipe(m[x-1][y-1])){
                        res.add(new InfoDSilver(x,y,-1,-1,etatP));
                    }

                }

                if(x+1<=9 && x+1>=1){
                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x+1][y+1])){
                        res.add(new InfoDSilver(x,y,1,1,etatP));
                    }

                    if(y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x+1][y-1])){
                        res.add(new InfoDSilver(x,y,1,-1,etatP));
                    }
                }

                if(x<=9 && x>=1){
                    if(y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x][y-1])){
                        res.add(new InfoDSilver(x,y,0,-1,etatP));
                    }
                }

                break;
            default:
                break;
        }
        return res;
    }

    @Override
    public ArrayList<CarteAvecInfoD> geneAutoDeplacement3(int[][] m) {
        ArrayList<InfoD>listInfoD=geneAutoDeplacement1(m);
        ArrayList<CarteAvecInfoD>listRes=new ArrayList<CarteAvecInfoD>();
        InfoDSilver temp;
        for(int i=0;i<listInfoD.size();i++){
            temp=(InfoDSilver) listInfoD.get(i);
            listRes.add(new CarteAvecInfoD(temp.genererProchaineCarte(m,temp),temp));
        }
        return listRes;
    }


    public ArrayList<InfoD> geneDeplacementCommeGold1(int[][] m) {
        ArrayList<InfoD> res=new ArrayList<InfoD>();
        switch (this.etatP/10){
            case 1:
                if(x-1<=9 && x-1>=1){
                    if(!verifierMemeEquipe(m[x-1][y]))
                        res.add(new InfoDSilver(x,y,-1,0,etatP));
                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x-1][y+1]))
                        res.add(new InfoDSilver(x,y,-1,1,etatP));
                }

                if(x+1<=9 && x+1>=1){
                    if(!verifierMemeEquipe(m[x+1][y]))
                        res.add(new InfoDSilver(x,y,1,0,etatP));
                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x+1][y+1]))
                        res.add(new InfoDSilver(x,y,1,1,etatP));
                }

                if(x<=9 && x>=1){
                    if(y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x][y-1]))
                        res.add(new InfoDSilver(x,y,0,-1,etatP));
                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x][y+1]))
                        res.add(new InfoDSilver(x,y,0,1,etatP));
                }

                break;
            case 2:
                if(x-1<=9 && x-1>=1){
                    if(!verifierMemeEquipe(m[x-1][y]))
                        res.add(new InfoDSilver(x,y,-1,0,etatP));
                    if(y-1<=9 && y-1>=1&& !verifierMemeEquipe(m[x-1][y-1]))
                        res.add(new InfoDSilver(x,y,-1,-1,etatP));
                }

                if(x+1<=9 && x+1>=1){
                    if(!verifierMemeEquipe(m[x+1][y]))
                        res.add(new InfoDSilver(x,y,1,0,etatP));
                    if(y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x+1][y-1]))
                        res.add(new InfoDSilver(x,y,1,-1,etatP));
                }

                if(x<=9 && x>=1){
                    if(y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x][y-1]))
                        res.add(new InfoDSilver(x,y,0,-1,etatP));
                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x][y+1]))
                        res.add(new InfoDSilver(x,y,0,1,etatP));
                }

                break;
            default:
                break;
        }
        return res;
    }

    /**
     * generer les case deplacement possible avec gold avec note de priorite
     * */
    private ArrayList<InfoD> geneDeplacementCommeGold2(int[][] m) {
        ArrayList<InfoD> res=new ArrayList<InfoD>();
        switch (this.etatP/10){
            case 1:
                if(x-1<=9 && x-1>=1){
                    if(!verifierMemeEquipe(m[x-1][y])){
                        InfoDGold myCase=new InfoDGold(x,y,-1,0,etatP);
                        if(m[x-1][y]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }
                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x-1][y+1])){
                        InfoDGold myCase=new InfoDGold(x,y,-1,1,etatP);
                        if(m[x-1][y+1]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }

                }

                if(x+1<=9 && x+1>=1){
                    if(!verifierMemeEquipe(m[x+1][y])){
                        InfoDGold myCase=new InfoDGold(x,y,1,0,etatP);
                        if(m[x+1][y]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }
                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x+1][y+1])){
                        InfoDGold myCase=new InfoDGold(x,y,1,1,etatP);
                        if(m[x+1][y+1]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }

                }

                if(x<=9 && x>=1){
                    if(y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x][y-1])){
                        InfoDGold myCase=new InfoDGold(x,y,0,-1,etatP);
                        if(m[x][y-1]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }

                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x][y+1])){
                        InfoDGold myCase=new InfoDGold(x,y,0,1,etatP);
                        if(m[x][y+1]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }

                }

                break;
            case 2:
                if(x-1<=9 && x-1>=1){
                    if(!verifierMemeEquipe(m[x-1][y])){
                        InfoDGold myCase=new InfoDGold(x,y,-1,0,etatP);
                        if(m[x-1][y]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }

                    if(y-1<=9 && y-1>=1&& !verifierMemeEquipe(m[x-1][y-1])){
                        InfoDGold myCase=new InfoDGold(x,y,-1,-1,etatP);
                        if(m[x-1][y-1]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }

                }

                if(x+1<=9 && x+1>=1){
                    if(!verifierMemeEquipe(m[x+1][y])){
                        InfoDGold myCase=new InfoDGold(x,y,1,0,etatP);
                        if(m[x+1][y]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }

                    if(y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x+1][y-1])){
                        InfoDGold myCase=new InfoDGold(x,y,1,-1,etatP);
                        if(m[x+1][y-1]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }

                }

                if(x<=9 && x>=1){
                    if(y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x][y-1])){
                        InfoDGold myCase=new InfoDGold(x,y,0,-1,etatP);
                        if(m[x][y-1]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }

                    if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x][y+1])){
                        InfoDGold myCase=new InfoDGold(x,y,0,1,etatP);
                        if(m[x][y+1]==0)
                            //si bouge
                            myCase.setPoint(0);
                        else
                            //si mange
                            myCase.setPoint(1);
                        res.add(myCase);
                    }
                }

                break;
            default:
                break;
        }
        return res;
    }

    public ArrayList<InfoD> genePromotion(int[][] m) {
        ArrayList<InfoD> res=new ArrayList<InfoD>();
        // si dans la zone de promotion
        if(x>=4 && x<=7) {
            switch (this.etatP) {
                case 13:
                    etatP = 33;
                    res.add(new InfoDSilver(x, y, 0, 0, etatP));
                case 23:
                    etatP = 43;
                    res.add(new InfoDSilver(x, y, 0, 0, etatP));
                default:
                    ;
            }
        }
        return res;
    }
}
