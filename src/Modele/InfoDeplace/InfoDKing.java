package Modele.InfoDeplace;

import Modele.Strategie.CarteAvecInfoD;
import Modele.Strategie.GenererLaCarteProchaine;

import java.util.ArrayList;

public class InfoDKing extends InfoD{
    public InfoDKing(int x, int y, int deltaX, int deltaY, int etat) {
        this.x=x;
        this.y=y;
        this.deltaX=deltaX;
        this.deltaY=deltaY;
        this.etatP=etat;
    }

    public InfoDKing(int x, int y, int etat) {
        super(x, y, etat);
    }

    @Override
    public ArrayList<InfoD> geneAutoDeplacement1(int[][] m) {
        //si il est deja mange
        if(x==0 && y==0 ) return null;
        ArrayList<InfoD> res=new ArrayList<InfoD>();
        InfoDKing temp;
        if(x-1<=9 && x-1>=1){
            if(!verifierMemeEquipe(m[x-1][y])){
                temp=new InfoDKing(x,y,-1,0,etatP);
                res.add(temp);
            }
            if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x-1][y+1])){
                temp=new InfoDKing(x,y,-1,1,etatP);
                res.add(temp);
            }
            if(y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x-1][y-1])){
                temp=new InfoDKing(x,y,-1,-1,etatP);
                res.add(temp);
            }
        }

        if(x+1<=9 && x+1>=1){
            if(!verifierMemeEquipe(m[x+1][y])){
                temp=new InfoDKing(x,y,1,0,etatP);
                res.add(temp);
            }
            if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x+1][y+1])){
                temp=new InfoDKing(x,y,1,1,etatP);
                res.add(temp);
            }
            if(y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x+1][y-1])){
                temp=new InfoDKing(x,y,1,-1,etatP);
                res.add(temp);
            }
        }

        if(x<=9 && x>=1){
            if(y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x][y-1])){
                temp=new InfoDKing(x,y,0,-1,etatP);
                res.add(temp);
            }
            if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x][y+1])){
                temp=new InfoDKing(x,y,0,1,etatP);
                res.add(temp);
            }
        }
        return res;
    }


    @Override
    public ArrayList<InfoD> geneAutoDeplacement2(int[][] m) {
        //si il est deja mange
        if(x==0 && y==0 ) return null;
        ArrayList<InfoD> res=new ArrayList<InfoD>();
        if(x-1<=9 && x-1>=1){
            if(!verifierMemeEquipe(m[x-1][y])){
                InfoDKing myCase=new InfoDKing(x,y,-1,0,etatP);
                if(m[x-1][y]==0)
                    //si bouge
                    myCase.setPoint(0);
                else
                    //si mange
                    myCase.setPoint(1);
                res.add(myCase);
            }

            if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x-1][y+1])){
                InfoDKing myCase=new InfoDKing(x,y,-1,1,etatP);
                if(m[x-1][y+1]==0)
                    //si bouge
                    myCase.setPoint(0);
                else
                    //si mange
                    myCase.setPoint(1);
                res.add(myCase);
            }

            if(y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x-1][y-1])){
                InfoDKing myCase=new InfoDKing(x,y,-1,-1,etatP);
                if(m[x-1][y-1]==0)
                    //si bouge
                    myCase.setPoint(0);
                else
                    //si mange
                    myCase.setPoint(1);
                res.add(myCase);
            }

        }

        if(x+1<=9 && x+1>=1){
            if(!verifierMemeEquipe(m[x+1][y])){
                InfoDKing myCase=new InfoDKing(x,y,1,0,etatP);
                if(m[x+1][y]==0)
                    //si bouge
                    myCase.setPoint(0);
                else
                    //si mange
                    myCase.setPoint(1);
                res.add(myCase);
            }

            if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x+1][y+1])){
                InfoDKing myCase=new InfoDKing(x,y,1,1,etatP);
                if(m[x+1][y+1]==0)
                    //si bouge
                    myCase.setPoint(0);
                else
                    //si mange
                    myCase.setPoint(1);
                res.add(myCase);
            }

            if(y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x+1][y-1])){
                InfoDKing myCase=new InfoDKing(x,y,1,-1,etatP);
                if(m[x+1][y-1]==0)
                    //si bouge
                    myCase.setPoint(0);
                else
                    //si mange
                    myCase.setPoint(1);
                res.add(myCase);
            }

        }

        if(x<=9 && x>=1){
            if(y-1<=9 && y-1>=1 && !verifierMemeEquipe(m[x][y-1])){
                InfoDKing myCase=new InfoDKing(x,y,0,-1,etatP);
                if(m[x][y-1]==0)
                    //si bouge
                    myCase.setPoint(0);
                else
                    //si mange
                    myCase.setPoint(1);
                res.add(myCase);
            }

            if(y+1<=9 && y+1>=1 && !verifierMemeEquipe(m[x][y+1])){
                InfoDKing myCase=new InfoDKing(x,y,0,1,etatP);
                if(m[x][y+1]==0)
                    //si bouge
                    myCase.setPoint(0);
                else
                    //si mange
                    myCase.setPoint(1);
                res.add(myCase);
            }

        }
        return res;
    }

    @Override
    public ArrayList<CarteAvecInfoD> geneAutoDeplacement3(int[][] m) {
        ArrayList<InfoD>listInfoD=geneAutoDeplacement1(m);
        ArrayList<CarteAvecInfoD>listRes=new ArrayList<CarteAvecInfoD>();
        InfoDKing temp;

        int[][] carteTemp;
        for(int i=0;i<listInfoD.size();i++){
            temp=(InfoDKing) listInfoD.get(i);
            carteTemp=temp.genererProchaineCarte(m,temp);

            listRes.add(new CarteAvecInfoD(temp.genererProchaineCarte(m,temp),temp));
        }
        return listRes;
    }


}
