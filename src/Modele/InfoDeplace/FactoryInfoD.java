package Modele.InfoDeplace;

public class FactoryInfoD {
    public InfoD geneInfoD(int x,int y,int etat){
        switch(etat%10){
            case 1:
                return new InfoDKing(x,y,etat);
            case 2:
                return new InfoDGold(x,y,etat);
            case 3:
                return new InfoDSilver(x,y,etat);
            case 4:
                return new InfoDKNight(x,y,etat);
            case 5:
                return new InfoDRook(x,y,etat);
            case 6:
                return new InfoDBishop(x,y,etat);
            case 7:
                return new InfoDLance(x,y,etat);
            case 8:
                return new InfoDPawn(x,y,etat);
            default:
                return null;
        }
    }

    public static InfoD geneInfoD(int x,int y,int deltaX,int deltaY,int etat){
        switch(etat%10){
            case 1:
                return new InfoDKing(x,y,deltaX,deltaY,etat);
            case 2:
                return new InfoDGold(x,y,deltaX,deltaY,etat);
            case 3:
                return new InfoDSilver(x,y,deltaX,deltaY,etat);
            case 4:
                return new InfoDKNight(x,y,deltaX,deltaY,etat);
            case 5:
                return new InfoDRook(x,y,deltaX,deltaY,etat);
            case 6:
                return new InfoDBishop(x,y,deltaX,deltaY,etat);
            case 7:
                return new InfoDLance(x,y,deltaX,deltaY,etat);
            case 8:
                return new InfoDPawn(x,y,deltaX,deltaY,etat);
            default:
                return null;
        }
    }
}
