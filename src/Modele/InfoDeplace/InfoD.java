package Modele.InfoDeplace;

import Modele.Strategie.CarteAvecInfoD;

import java.util.ArrayList;

/**
 * ce class est pour creeer un object qui contient
 * les information pour deplacer une piece.Donc il
 * contient le lieu origin et les distance deplacement
 * dans ligne et colonne
 * */
public abstract class InfoD {
    protected int x;
    protected int y;
    protected int deltaX;
    protected int deltaY;
    //private int noteDeAction;
    protected int etatP;
    protected int point=0;
    private String name="anonyme";
    /**
     * constructeur avec info complet
     * */
    public InfoD(int x1, int y1, int deltaX1, int deltaY1, int etat){
        x=x1;
        y=y1;
        deltaX=deltaX1;
        deltaY=deltaY1;
        etatP=etat;
    }
    /**
     * constructeur basic
     * */
    public InfoD(int x,int y,int etat){
        this.x=x;
        this.y=y;
        deltaX=0;
        deltaY=0;
        this.etatP=etat;
    }

    public InfoD() {
        x=0;
        y=0;
        deltaX=0;
        deltaY=0;
    }

    public int getEtatP() {
        return etatP;
    }

    public void setEtatP(int etatP) {
        this.etatP = etatP;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getDeltaX() {
        return deltaX;
    }

    public void setDeltaX(int deltaX) {
        this.deltaX = deltaX;
    }

    public int getDeltaY() {
        return deltaY;
    }

    public void setDeltaY(int deltaY) {
        this.deltaY = deltaY;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * generer tous les deplacement possible
     * * */
    public abstract ArrayList<InfoD> geneAutoDeplacement1(int[][] m);

    /**
     * generer tous les deplacement possible.
     * Et donne les note priorite pour chaque case de deplacement.
     * * */
    public abstract ArrayList<InfoD> geneAutoDeplacement2(int[][] m);

    /**
     * generer tous les deplacement possible avec la deplacement et la carte suivant.
     * Et donne les note priorite pour chaque case de deplacement ,si donneNote est true.
     * * */
    public abstract ArrayList<CarteAvecInfoD> geneAutoDeplacement3(int[][] m);

    public boolean verifierMemeEquipe(int etatDautre){
        if((etatP>=51 && etatP<=58)||(etatP>=33 && etatP<=38 )||(etatP>=11 && etatP<=18)){
            if((etatDautre>=51 && etatDautre<=58)||(etatDautre>=33 && etatDautre<=38 )||(etatDautre>=11 && etatDautre<=18)){
                return true;
            }
        }
        else if((etatP>=61 && etatP<=68)||(etatP>=43 && etatP<=48 )||(etatP>=21 && etatP<=28)){
            if((etatDautre>=61 && etatDautre<=68)||(etatDautre>=43 && etatDautre<=48 )||(etatDautre>=21 && etatDautre<=28)){
                return true;
            }
        }
        return false;
    }

    /**
     * generer prochaine carte avec infoD
     * */
    public int[][] genererProchaineCarte(int[][]carte,InfoD d){
        int[][]myCarte=new int[10][10];
        for(int i=0;i<10;i++){
            for(int j=0;j<10;j++){
                if(i==0||j==0){
                    myCarte[j][i]=0;
                }else{
                    myCarte[j][i]=carte[j][i];
                }
            }
        }

        myCarte[d.getDeltaX()+d.getX()][d.getY()+d.getDeltaY()]=etatP;
        myCarte[d.getX()][d.getY()]=0;

        return myCarte;
    }

    /**
     * si les deux piece sont dans meme equipe retourner false
     * sinon retourner true
     * */
    public boolean verifierDautreEquipe(int etatDautre){
        if((etatP>=51 && etatP<=58)||(etatP>=33 && etatP<=38 )||(etatP>=11 && etatP<=18)){
            if((etatDautre>=61 && etatDautre<=68)||(etatDautre>=43 && etatDautre<=48 )||(etatDautre>=21 && etatDautre<=28)){
                return true;
            }
        }
        else if((etatP>=61 && etatP<=68)||(etatP>=43 && etatP<=48 )||(etatP>=21 && etatP<=28)){
            if((etatDautre>=51 && etatDautre<=58)||(etatDautre>=33 && etatDautre<=38 )||(etatDautre>=11 && etatDautre<=18)){
                return true;
            }
        }
        return false;
    }

    public void afficher(InfoD d){
        System.out.println("InfoD x : "+ d.getX() + " y : " + d.getY() +" delatX : "+d.getDeltaX()+ " deltaY : "+d.getDeltaY()+" etat : "+d.getEtatP());
    }
}
