package Modele.Piece;

import Modele.InfoDeplace.InfoD;
import Modele.InfoDeplace.InfoDBishop;
import Modele.InfoDeplace.InfoDKing;
import Modele.InfoDeplace.InfoDRook;
import Modele.Map.CarteCommun;

import java.util.ArrayList;

public class Rook extends PieceNormal{
    //lieu initial
    // 1 : en haut à gauche
    // 2 : en bas à droite
    public Rook(int lieu){
        nom="rook";
        if(lieu==1){
            this.x=2;
            this.y=2;
            this.etat=15;
        }else{
            this.x=8;
            this.y=8;
            this.etat=25;
        }
    }

    /**
     * ini une piece hors de la carte d'une joueur
     * */
    public Rook(String joueur){
        nom="rook";
        if(joueur.equals("joueur1")){
            this.etat=55;
        }else{
            this.etat=65;
        }
    }

    public Rook(int x,int y,int etat){
        sasirPiece(x,y,etat);
    }

    @Override
    public boolean testDeplacer(int deltaX,int deltaY,int [][]carte) {
        int operation = 0;
        //saisir les cas
        //deplacer en direct
        if(deltaX==0 && deltaY!=0){
            operation = 1;
        }else if(deltaX!=0 && deltaY==0){
            operation = 2;
        }else if(etat==35||etat==45){
            if ((deltaX == 1 || deltaX == -1 )&& (deltaY == 1 || deltaY == -1)) {
                operation = 3;
            }
        }

        if(x+deltaX < 1 || x+deltaX > 9 || y+deltaY<1 || y+deltaY>9)
            operation = 0;

        switch (operation){
            case 1:
                //deplacer en ligen
            case 2:
                //deplacer en colonne
            case 3:
                //deplacer avec promotion
                return !verifierMemeEquipe(carte[x + deltaX][y + deltaY]);
            default:
                return false;
        }
    }

    @Override
    public boolean testPromotion(int deltaY) {
        return testPromoNormal(deltaY);
    }

    @Override
    public ArrayList<InfoD> deplacePossible(int[][] laCarte) {
        InfoDRook info=new InfoDRook(x,y,etat);
        return info.geneAutoDeplacement1(laCarte);
    }

    @Override
    public ArrayList<InfoD> deplacePossibleAvecNote(int[][] laCarte) {
        InfoDRook info=new InfoDRook(x,y,etat);
        return info.geneAutoDeplacement2(laCarte);
    }

    @Override
    public PieceNormal copyHorsCarte(String joueur) {
        return new Rook(joueur);
    }

    @Override
    public CarteCommun auto1(CarteCommun laCarte) {
        //si ce piece a deja mange
        if(testEtreMange(laCarte)) {
            etreMange();
            return laCarte;
        }


        int deltaX,deltaY;
        InfoD myInfo=choixAuHazard(deplacePossible(laCarte.getCarte()));
        //verifier que il y a cas de deplacer
        if(myInfo != null){
            InfoDRook casDeplace= (InfoDRook) myInfo;
            deltaX = casDeplace.getDeltaX();
            deltaY = casDeplace.getDeltaY();
            if(testDeplacer(deltaX,deltaY,laCarte.getCarte())) {
                laCarte.deplacerUnPiece(x, y, deltaX, deltaY);
                x += deltaX;
                y += deltaY;
            }
        }

        return laCarte;
    }

    @Override
    public CarteCommun auto2(CarteCommun laCarte) {
        //si ce piece a deja mange
        if(testEtreMange(laCarte)) {
            etreMange();
            return laCarte;
        }

        int deltaX,deltaY;
        InfoD myInfo=this.deplacementEquipe(laCarte);
        //verifier que il y a cas de deplacer
        if(myInfo != null && myInfo.getEtatP()==this.getEtat()
                && myInfo.getX()==this.getX() && myInfo.getY()==this.getY()){
            InfoDRook casDeplace= (InfoDRook) myInfo;
            deltaX = casDeplace.getDeltaX();
            deltaY = casDeplace.getDeltaY();
            if(testDeplacer(deltaX,deltaY,laCarte.getCarte())) {
                laCarte.deplacerUnPiece(x, y, deltaX, deltaY);
                x += deltaX;
                y += deltaY;
            }
        }

        return laCarte;
    }


}