package Modele.Piece;

import Modele.InfoDeplace.InfoD;
import Modele.InfoDeplace.InfoDRook;
import Modele.Map.CarteCommun;

import java.util.ArrayList;

public abstract class PieceNormal {
    //La classe de toutes les pièces du jeu
    protected String nom;
    //6 états A，B，A avec promo，B avec promo，la carte de A，la zone en attente de B
    //1x pour dans la carte avec joueur 1
    //2x pour dans la carte avec joueur 2
    //3x pour deja promotion dans la carte avec joueur 1
    //4x pour deja promotion dans la carte avec joueur 2
    //5x pour dans la zone attend avec joueur 1
    //6x pour dans la zone attend avec joueur 2
    protected int etat;
    //position
    protected int x,y;
    //etat de promotion
    protected boolean promo;
    //Déplacement
    public void deplacer(int deltaX,int deltaY,int[][] laCarte) {
        if(testDeplacer( deltaX, deltaY,laCarte))
        {
            x+=deltaX;
            y+=deltaY;
        }
    }
    //pour voir là où il est possible de déplacer la pièce
    public abstract boolean testDeplacer(int deltaX,int deltaY,int[][] laCarte);

    /**pour voir la, ou il est possible de manger la piece apres deplacement*/
    public boolean testMange(int deltaX,int deltaY,int[][] laCarte){
        int etatDautre=laCarte[this.x+deltaX][this.y+deltaY];
        return (etatDautre!=0)&&(!verifierMemeEquipe(etatDautre));
    }

    /**
     * faire promotion avec change etat de ce piece
     * */
    public void promotion(){
        promo=true;
        etat=etat+20;
    }

    /**
    * tester si ce piece a deja manger par d'autre piece
     * utilise pour version thread
    * */
    public boolean testEtreMange(CarteCommun laCarte){
        int[][] c=laCarte.getCarte();
        if(c[x][y]!=etat)
            return true;
        else
            return false;
    }

    /**
     * si ce piece est mange par d'autre piece
     * on motifier son lieu et son etat
     *  utilise pour version thread
    * */
    public void etreMange(){
        if(etat/10==1 || etat/10==3){
            x=0;
            y=0;
            etat=etat%10+60;
        }else if(etat/10 == 2 || etat/10==4){
            x=0;
            y=0;
            etat=etat%10+50;
        }
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void sasirPiece(int x,int y,int etat){
        this.x=x;
        this.y=y;
        this.etat=etat;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isPromo() {
        return promo;
    }

    public void setPromo(boolean promo) {
        this.promo = promo;
    }

    /**
     * si les deux piece sont dans meme equipe retourner true
     * sinon retourner false
     * */
    public boolean verifierMemeEquipe(int etatDautre){
        if((etat>=51 && etat<=58)||(etat>=33 && etat<=38 )||(etat>=11 && etat<=18)){
            if((etatDautre>=51 && etatDautre<=58)||(etatDautre>=33 && etatDautre<=38 )||(etatDautre>=11 && etatDautre<=18)){
                return true;
            }
        }
        else if((etat>=61 && etat<=68)||(etat>=43 && etat<=48 )||(etat>=21 && etat<=28)){
            if((etatDautre>=61 && etatDautre<=68)||(etatDautre>=43 && etatDautre<=48 )||(etatDautre>=21 && etatDautre<=28)){
                return true;
            }
        }
        return false;
    }

    /**
     * si les deux piece sont dans meme equipe retourner false
     * sinon retourner true
     * */
    public boolean verifierDautreEquipe(int etatDautre){
        if((etat>=51 && etat<=58)||(etat>=33 && etat<=38 )||(etat>=11 && etat<=18)){
            if((etatDautre>=61 && etatDautre<=68)||(etatDautre>=43 && etatDautre<=48 )||(etatDautre>=21 && etatDautre<=28)){
                return true;
            }
        }
        else if((etat>=61 && etat<=68)||(etat>=43 && etat<=48 )||(etat>=21 && etat<=28)){
            if((etatDautre>=51 && etatDautre<=58)||(etatDautre>=33 && etatDautre<=38 )||(etatDautre>=11 && etatDautre<=18)){
                return true;
            }
        }
        return false;
    }

    /**
     * apelle la fonction testPromoNormal pour realiser
     * */
    public abstract boolean testPromotion(int deltaY);

    /**
     * test de la promotion normal
     * mais on utilise pas pour Gold et King
     * */
    public boolean testPromoNormal(int deltaY){
        if(etat>=11 && etat<=18){
            if(y+deltaY>=7)
                return true;
        }else if(etat>=21 && etat<=28){
            if(y+deltaY<=3)
                return true;
        }
        return false;
    }

    /**
     * retourner les info pour deplacement possible
     * avec premier version qui peut bouger et manger
     * */
    public abstract ArrayList<InfoD> deplacePossible(int[][] laCarte);

    /**
     * retourner les info pour deplacement possible
     * avec premier version qui peut bouger et manger.
     * On aussi donne le note de priorite pour chqaue case.
     * 1 pour mange d'autre piece.0 pour bouger.
     * */
    public abstract ArrayList<InfoD> deplacePossibleAvecNote(int[][] laCarte);

    /**
     * retourner une piece hors de la carte d'une joueur
     */
    public abstract PieceNormal copyHorsCarte(String joueur);

    /**
     * deplacer automatiquement si il peut mange il mange,
     * si il peut deplacer il dep[lacer
     */
    public abstract CarteCommun auto1(CarteCommun laCarte);

    /**
     * deplacer automatiquement avec priorite
     * En utilisant le case au choix dans CarteCommun
     */
    public abstract CarteCommun  auto2(CarteCommun laCarte);

    public CarteCommun auto4(CarteCommun laCarte) {
        //si ce piece a deja mange
        if(testEtreMange(laCarte)) {
            etreMange();
            return laCarte;
        }

        int deltaX,deltaY;
        InfoD myInfo=this.deplacementEquipe(laCarte);
        int[][]carteMaintnance=laCarte.getCarte();
        //verifier que il y a cas de deplacer
        //verifier que le piece n'a pas bouge
        if(myInfo != null || carteMaintnance[x][y]!=0){
            deltaX = myInfo.getDeltaX();
            deltaY = myInfo.getDeltaY();
            if(testDeplacer(deltaX,deltaY,laCarte.getCarte())) {
                laCarte.deplacerUnPiece(x, y, deltaX, deltaY);
                x += deltaX;
                y += deltaY;
            }
        }

        return laCarte;
    }

    /**
     * choisir une case qui contient les info deplacement dans InfoD
     * */
    public InfoD choixAuHazard(ArrayList<InfoD> list){
        if(list.size()==0)
            return null;
        int index = (int) (Math.random()* list.size());
        return list.get(index);
    }

    /**
     * choisir une case qui contient les info deplacement dans InfoD.
     * Ce cas a un plus grand note dans ce list.
     *  Si il y a plusieur cas a le plus grand note, on les choix au hasard parmi les case
     * */
    public InfoD choixAuHazardAvecPriorite(ArrayList<InfoD> list){
        if(list.size()==0)
            return null;
        InfoD caseTemp=list.get(0);
        int noteMax=caseTemp.getPoint();
        for (InfoD infoD : list) {
            caseTemp = infoD;
            if (caseTemp.getPoint() > noteMax)
                noteMax = caseTemp.getPoint();
        }
        ArrayList<InfoD> listAvecPlusGrandNote=new ArrayList<InfoD>();
        for (InfoD infoD : list) {
            caseTemp = infoD;
            if (caseTemp.getPoint() == noteMax)
                listAvecPlusGrandNote.add(caseTemp);
        }
        int index = (int) (Math.random()* listAvecPlusGrandNote.size());
        return listAvecPlusGrandNote.get(index);
    }

    /**
     * retourner le equipe de piece d'apres l'etat
     * */
    public int eaquipeDePiece(){
        if((etat>=51 && etat<=58)||(etat>=33 && etat<=38 )||(etat>=11 && etat<=18)){
            return 1;
        }
        else
            return 2;
    }

    /**
     * Obtenir le info de deplacement de equipe de ce piece dans carteEnCommun
     * */
    public InfoD deplacementEquipe(CarteCommun c){
        if(this.eaquipeDePiece()==1)
            return c.getInfoDeplaceEquip1();
        else if(this.eaquipeDePiece()==2)
            return c.getInfoDeplaceEquip2();
        else
            return null;
    }
}
