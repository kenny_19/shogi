package Modele.Piece;

import Modele.InfoDeplace.InfoD;
import Modele.InfoDeplace.InfoDBishop;
import Modele.InfoDeplace.InfoDKing;
import Modele.InfoDeplace.InfoDSilver;
import Modele.Map.CarteCommun;

import java.util.ArrayList;

public class Silver extends PieceNormal{
    //lieu initial 1,2,3,4 haut gaunche haut droite base gauche base droite
    public Silver(int lieu){
        nom="silver";
        if(lieu==1){
            this.x=3;
            this.y=1;
            this.etat=13;
        }else if(lieu ==2){
            this.x=7;
            this.y=1;
            this.etat=13;
        }else if(lieu ==3){
            this.x=3;
            this.y=9;
            this.etat=23;
        }else{
            this.x=7;
            this.y=9;
            this.etat=23;
        }
    }

    /**
     * ini une piece hors de la carte d'une joueur
     * */
    public Silver(String joueur){
        nom="silver";
        if(joueur.equals("joueur1")){
            this.etat=53;
        }else{
            this.etat=63;
        }
    }

    public Silver(int x,int y,int etat){
        sasirPiece(x,y,etat);
    }


    @Override
    public boolean testDeplacer(int deltaX,int deltaY,int [][]carte) {
        int operation = 0;
        //saisir les cas
        if(deltaX==-1 && deltaY==1 && etat==13){
            operation=1;
        }else if(deltaX==0 && deltaY==1 && etat==13){
            operation=2;
        }else if(deltaX==1 && deltaY==1 && etat==13){
            operation=3;
        }else if(deltaX==-1 && deltaY==-1 && etat==13){
            operation=4;
        }else if(deltaX==1 && deltaY==-1 && etat==13){
            operation=5;
        }else if(deltaX==-1 && deltaY==-1 && etat==23){
            operation=6;
        }else if(deltaX==0 && deltaY==-1 && etat==23){
            operation=7;
        }else if(deltaX==1 && deltaY==-1 && etat==23){
            operation=8;
        }else if(deltaX==-1 && deltaY==1 && etat==23){
            operation=9;
        }else if(deltaX==1 && deltaY==1 && etat==23){
            operation=10;
        }else if(etat==33){
            //les cas avec promottion
            if(deltaX==-1 && deltaY==1){
                operation=11;
            }else if(deltaX==0 && deltaY==1){
                operation=12;
            }else if(deltaX==1 && deltaY==1){
                operation=13;
            }else if(deltaX==1 && deltaY==0){
                operation=14;
            }else if(deltaX==-1 && deltaY==0){
                operation=15;
            }else if(deltaX==0 && deltaY==-1){
                operation=16;
            }
        }else if(etat == 43){
            //les cas avec promottion
            if(deltaX==-1 && deltaY==-1){
                operation=21;
            }else if(deltaX==0 && deltaY==-1){
                operation=22;
            }else if(deltaX==1 && deltaY==-1){
                operation=23;
            }else if(deltaX==1 && deltaY==0){
                operation=24;
            }else if(deltaX==-1 && deltaY==0){
                operation=25;
            }else if(deltaX==0 && deltaY==1){
                operation=26;
            }
        }

        //tester
        if(x+deltaX < 1 || x+deltaX > 9 || y+deltaY<1 || y+deltaY>9)
            operation = 0;
        switch (operation){
            //deplacer a gauche base
            case 1:
            //deplacer a base
            case 2:
            //deplacer a droite base
            case 3:
            //deplacer a gauche haute
            case 4:
            //deplacer a droite haute
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
                //deplacer avec promotion
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
                return !verifierMemeEquipe(carte[x + deltaX][y + deltaY]);
            default:
                return false;
        }
    }

    @Override
    public boolean testPromotion(int deltaY) {
        return testPromoNormal(deltaY);
    }

    @Override
    public ArrayList<InfoD> deplacePossible(int[][] laCarte) {
        InfoDSilver info=new InfoDSilver(x,y,etat);
        return info.geneAutoDeplacement1(laCarte);
    }

    @Override
    public ArrayList<InfoD> deplacePossibleAvecNote(int[][] laCarte) {
        InfoDSilver info=new InfoDSilver(x,y,etat);
        return info.geneAutoDeplacement2(laCarte);
    }

    @Override
    public PieceNormal copyHorsCarte(String joueur) {
        return new Silver(joueur);
    }

    @Override
    public CarteCommun auto1(CarteCommun laCarte) {
        //si ce piece a deja mange
        if(testEtreMange(laCarte)) {
            etreMange();
            return laCarte;
        }


        int deltaX,deltaY;
        InfoD myInfo=choixAuHazard(deplacePossible(laCarte.getCarte()));
        //verifier que il y a cas de deplacer
        if(myInfo != null){
            InfoDSilver casDeplace= (InfoDSilver) myInfo;
            deltaX = casDeplace.getDeltaX();
            deltaY = casDeplace.getDeltaY();
            if(testDeplacer(deltaX,deltaY,laCarte.getCarte())) {
                laCarte.deplacerUnPiece(x, y, deltaX, deltaY);
                x += deltaX;
                y += deltaY;
            }
        }

        return laCarte;
    }

    @Override
    public CarteCommun auto2(CarteCommun laCarte) {
        //si ce piece a deja mange
        if(testEtreMange(laCarte)) {
            etreMange();
            return laCarte;
        }

        int deltaX,deltaY;
        InfoD myInfo=this.deplacementEquipe(laCarte);
        //verifier que il y a cas de deplacer
        if(myInfo != null && myInfo.getEtatP()==this.getEtat()
            && myInfo.getX()==this.getX() && myInfo.getY()==this.getY()){
            InfoDSilver casDeplace= (InfoDSilver) myInfo;
            deltaX = casDeplace.getDeltaX();
            deltaY = casDeplace.getDeltaY();
            if(testDeplacer(deltaX,deltaY,laCarte.getCarte())) {
                laCarte.deplacerUnPiece(x, y, deltaX, deltaY);
                x += deltaX;
                y += deltaY;
            }
        }

        return laCarte;
    }
}
