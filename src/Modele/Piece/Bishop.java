package Modele.Piece;

import Modele.InfoDeplace.InfoD;
import Modele.InfoDeplace.InfoDBishop;
import Modele.InfoDeplace.InfoDPawn;
import Modele.Map.CarteCommun;

import java.util.ArrayList;

public class Bishop extends PieceNormal{
    //lieu initial
    // 1 : en haut à droite
    // 2 : en bas à gauche
    public Bishop(int lieu){
        nom="Bishop";
        if(lieu==1){
            this.x=8;
            this.y=2;
            this.etat=16;
        }else{
            this.x=2;
            this.y=8;
            this.etat=26;
        }
    }

    public Bishop(int x,int y,int etat){
        sasirPiece(x,y,etat);
    }

    /**
     * ini une piece hors de la carte d'une joueur
     * */
    public Bishop(String joueur){
        nom="bishop";
        if(joueur.equals("joueur1")){
            this.etat=56;
        }else{
            this.etat=66;
        }
    }


    @Override
    public boolean testDeplacer(int deltaX,int deltaY,int [][]carte) {
        int operation = 0;
            //saisir les cas
            //les cas deplacer avec slash
        if(deltaX==deltaY){
            operation=1;
        }else if(deltaX==deltaY*-1){
            operation=2;
        }else if(etat==36 || etat==46){
            //les cas avec promottion
            if(deltaX==0 && (deltaY==1 || deltaY==-1)){
                operation=3;
            }else if(deltaY==0 && (deltaX==1 || deltaX==-1)){
                operation=3;
            }
        }

        //verifier
        // 1. verifier les destination est dans notre carte
        // 2. les grille dans le chemin de deplacer est vide
        // 3. pour la destination on permet il existe la piece dans
        // d'autre equipe pour mange

        //verifier les destination
        if(x+deltaX < 1 || x+deltaX > 9 || y+deltaY<1 || y+deltaY>9)
            operation = 0;
        //tester
        switch (operation){
            //deplacer a slash
            case 1:
                boolean positive = deltaX>0 ;
                if(positive){
                    for(int i=1;i<=deltaX;i++){
                        //si il existe piece pendans lieu de depart et destination
                        if(i<deltaX) {
                            if (carte[x + i][y + i] != 0)
                                return false;
                        }else{
                            //verifier dans la destination il est n'a pas piece de meme parti
                            if(verifierMemeEquipe(carte[x+i][y+i]))
                                return false;
                        }
                    }
                }else{
                    for(int i=1;i<=deltaX*(-1);i++){
                            //si il existe piece pendans lieu de depart et destination
                            if(i<deltaX*(-1)) {
                                if (carte[x + (-1 * i)][y + (-1 * i)] != 0)
                                    return false;
                            }else{
                                //verifier dans la destination il est n'a pas piece de meme parti
                                if(verifierMemeEquipe(carte[x+(-1*i)][y+(-1*i)]))
                                    return false;
                            }
                    }
                }
                return true;

            //deplacer a slash
            case 2:
                boolean positiveX = deltaX>0 ;
                if(positiveX){
                    for(int i=1;i<=deltaX;i++){
                        //si il existe piece pendans lieu de depart et destination
                        if(i<deltaX) {
                            if (carte[x + i][y + (i * -1)] != 0)
                                return false;
                        }else{
                            //verifier dans la destination il est n'a pas piece de meme parti
                            if(verifierMemeEquipe(carte[x+i][y+(i*-1)]))
                                return false;
                        }
                    }
                }else {
                    for (int i = 1; i <= deltaX*(-1); i++) {
                        //si il existe piece pendans lieu de depart et destination
                        if (i < deltaX*(-1)) {
                            if (carte[x + (i * -1)][y + i] != 0)
                                return false;
                        }else {
                            //verifier dans la destination il est n'a pas piece de meme parti
                            if (verifierMemeEquipe(carte[x + (i * -1)][y + i]))
                                return false;
                        }
                    }
                }
                return true;

            case 3:
                return !verifierMemeEquipe(carte[x + deltaX][y + deltaY]);
            default:
                return false;
        }
    }


    @Override
    public boolean testPromotion(int deltaY) {
        return testPromoNormal(deltaY);
    }

    @Override
    public ArrayList<InfoD> deplacePossible(int[][] laCarte) {
        InfoDBishop info=new InfoDBishop(x,y,etat);
        return info.geneAutoDeplacement1(laCarte);
    }

    @Override
    public ArrayList<InfoD> deplacePossibleAvecNote(int[][] laCarte) {
        InfoDBishop info=new InfoDBishop(x,y,etat);
        return  info.geneAutoDeplacement2(laCarte);
    }

    @Override
    public PieceNormal copyHorsCarte(String joueur) {
        return new Bishop(joueur);
    }

    @Override
    public CarteCommun auto1(CarteCommun laCarte) {
        //si ce piece a deja mange
        if(testEtreMange(laCarte)) {
            etreMange();
            return laCarte;
        }


        int deltaX,deltaY;
        InfoD myInfo=choixAuHazard(deplacePossible(laCarte.getCarte()));
        //verifier que il y a cas de deplacer
        if(myInfo != null){
            InfoDBishop casDeplace= (InfoDBishop) myInfo;
            deltaX = casDeplace.getDeltaX();
            deltaY = casDeplace.getDeltaY();
            if(testDeplacer(deltaX,deltaY,laCarte.getCarte())) {
                laCarte.deplacerUnPiece(x, y, deltaX, deltaY);
                x += deltaX;
                y += deltaY;
            }
        }

        return laCarte;
    }

    @Override
    public CarteCommun auto2(CarteCommun laCarte) {
        //si ce piece a deja mange
        if(testEtreMange(laCarte)) {
            etreMange();
            return laCarte;
        }

        int deltaX,deltaY;
        //verifier que il y a cas de deplacer
        InfoD myInfo=this.deplacementEquipe(laCarte);
        if(myInfo != null && myInfo.getEtatP()==this.getEtat()
                && myInfo.getX()==this.getX() && myInfo.getY()==this.getY()){
            InfoDBishop casDeplace= (InfoDBishop) myInfo;
            deltaX = casDeplace.getDeltaX();
            deltaY = casDeplace.getDeltaY();
            if(testDeplacer(deltaX,deltaY,laCarte.getCarte())) {
                laCarte.deplacerUnPiece(x, y, deltaX, deltaY);
                x += deltaX;
                y += deltaY;
            }
        }

        return laCarte;
    }


}