package Modele.Piece;

import Modele.InfoDeplace.InfoD;
import Modele.InfoDeplace.InfoDBishop;
import Modele.InfoDeplace.InfoDKNight;
import Modele.InfoDeplace.InfoDKing;
import Modele.Map.CarteCommun;

import java.util.ArrayList;

public class KNight extends PieceNormal{
    //lieu initial 1,2,3,4 haut gaunche haut droite base gauche base droite
    public KNight(int lieu){
        nom="knight";
        if(lieu == 1){
            this.x=2;
            this.y=1;
            this.etat=14;
        }else if(lieu == 2){
            this.x=8;
            this.y=1;
            this.etat=14;
        }else if(lieu == 3){
            this.x=2;
            this.y=9;
            this.etat=24;
        }else{
            this.x=8;
            this.y=9;
            this.etat=24;
        }
    }

    /**
     * ini une piece hors de la carte d'une joueur
     * */
    public KNight(String joueur){
        nom="knight";
        if(joueur.equals("joueur1")){
            this.etat=54;
        }else{
            this.etat=64;
        }
    }

    public KNight(int x,int y,int etat){
        sasirPiece(x,y,etat);
    }

    @Override
    public boolean testDeplacer(int deltaX, int deltaY,int [][]carte) {
        int operation = 0;
        //saisir les cas
        if(deltaX==1 && deltaY== 2 && etat==14){
            operation=1;
        }else if(deltaX==-1 && deltaY==2 && etat==14){
            operation=2;
        }else if(deltaX==1 && deltaY==-2 && etat==24){
            operation=3;
        }else if(deltaX==-1 && deltaY==-2 && etat==24){
            operation=4;
        }else if(etat==34){
            //les cas avec promottion
            if(deltaX==-1 && deltaY==1){
                operation=11;
            }else if(deltaX==0 && deltaY==1){
                operation=12;
            }else if(deltaX==1 && deltaY==1){
                operation=13;
            }else if(deltaX==1 && deltaY==0){
                operation=14;
            }else if(deltaX==-1 && deltaY==0){
                operation=15;
            }else if(deltaX==0 && deltaY==-1){
                operation=16;
            }
        }else if(etat==44){
            //les cas avec promottion
            if(deltaX==-1 && deltaY==-1){
                operation=21;
            }else if(deltaX==0 && deltaY==-1){
                operation=22;
            }else if(deltaX==1 && deltaY==-1){
                operation=23;
            }else if(deltaX==1 && deltaY==0){
                operation=24;
            }else if(deltaX==-1 && deltaY==0){
                operation=25;
            }else if(deltaX==0 && deltaY==1){
                operation=26;
            }
        }

        //verifier les destination
        if(x+deltaX < 1 || x+deltaX > 9 || y+deltaY<1 || y+deltaY>9)
            operation = 0;

        switch (operation) {
            //deplacer a gauche
            case 1:
            //deplacer a droite
            case 2:
                //deplacer a gauche
            case 3:
                //deplacer a droite
            case 4:
                //deplacer avec promotion
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
                return (!verifierMemeEquipe(carte[x+deltaX][y+deltaY]));
            default:
                return false;
        }
    }


    @Override
    public boolean testPromotion(int deltaY) {
        return testPromoNormal(deltaY);
    }

    @Override
    public ArrayList<InfoD> deplacePossible(int[][] laCarte) {
        InfoDKNight info=new InfoDKNight(x,y,etat);
        return info.geneAutoDeplacement1(laCarte);
    }

    @Override
    public ArrayList<InfoD> deplacePossibleAvecNote(int[][] laCarte) {
        InfoDKNight info=new InfoDKNight(x,y,etat);
        return info.geneAutoDeplacement2(laCarte);
    }

    @Override
    public PieceNormal copyHorsCarte(String joueur) {
        return new KNight(joueur);
    }

    @Override
    public CarteCommun auto1(CarteCommun laCarte) {
        //si ce piece a deja mange
        if(testEtreMange(laCarte)) {
            etreMange();
            return laCarte;
        }


        int deltaX,deltaY;
        InfoD myInfo=choixAuHazard(deplacePossible(laCarte.getCarte()));
        //verifier que il y a cas de deplacer
        if(myInfo != null){
            InfoDKNight casDeplace= (InfoDKNight) myInfo;
            deltaX = casDeplace.getDeltaX();
            deltaY = casDeplace.getDeltaY();
            if(testDeplacer(deltaX,deltaY,laCarte.getCarte())) {
                laCarte.deplacerUnPiece(x, y, deltaX, deltaY);
                x += deltaX;
                y += deltaY;
            }
        }

        return laCarte;
    }

    @Override
    public CarteCommun auto2(CarteCommun laCarte) {
        //si ce piece a deja mange
        if(testEtreMange(laCarte)) {
            etreMange();
            return laCarte;
        }

        int deltaX,deltaY;
        InfoD myInfo=this.deplacementEquipe(laCarte);
        //verifier que il y a cas de deplacer
        if(myInfo != null && myInfo.getEtatP()==this.getEtat()
                && myInfo.getX()==this.getX() && myInfo.getY()==this.getY()){
            InfoDKNight casDeplace= (InfoDKNight) myInfo;
            deltaX = casDeplace.getDeltaX();
            deltaY = casDeplace.getDeltaY();
            if(testDeplacer(deltaX,deltaY,laCarte.getCarte())) {
                laCarte.deplacerUnPiece(x, y, deltaX, deltaY);
                x += deltaX;
                y += deltaY;
            }
        }

        return laCarte;
    }
}
