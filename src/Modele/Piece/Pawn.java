package Modele.Piece;

import Modele.InfoDeplace.InfoD;
import Modele.InfoDeplace.InfoDKing;
import Modele.InfoDeplace.InfoDPawn;
import Modele.Map.CarteCommun;

import java.util.ArrayList;

public class Pawn extends PieceNormal{
    //lieu initial
    // 1 à 9 : en haut
    // 10 à 18 : en bas
    public Pawn(int lieu){
        nom="pawn";
        if(lieu==1){
            this.x=1;
            this.y=3;
            this.etat=18;
        }else if(lieu==2){
            this.x=2;
            this.y=3;
            this.etat=18;
        }else if(lieu==3){
            this.x=3;
            this.y=3;
            this.etat=18;
        }else if(lieu==4){
            this.x=4;
            this.y=3;
            this.etat=18;
        }else if(lieu==5){
            this.x=5;
            this.y=3;
            this.etat=18;
        }else if(lieu==6){
            this.x=6;
            this.y=3;
            this.etat=18;
        }else if(lieu==7){
            this.x=7;
            this.y=3;
            this.etat=18;
        }else if(lieu==8){
            this.x=8;
            this.y=3;
            this.etat=18;
        }else if(lieu==9){
            this.x=9;
            this.y=3;
            this.etat=18;
        }else if(lieu==10){
            this.x=1;
            this.y=7;
            this.etat=28;
        }else if(lieu==11){
            this.x=2;
            this.y=7;
            this.etat=28;
        }else if(lieu==12){
            this.x=3;
            this.y=7;
            this.etat=28;
        }else if(lieu==13){
            this.x=4;
            this.y=7;
            this.etat=28;
        }else if(lieu==14){
            this.x=5;
            this.y=7;
            this.etat=28;
        }else if(lieu==15){
            this.x=6;
            this.y=7;
            this.etat=28;
        }else if(lieu==16){
            this.x=7;
            this.y=7;
            this.etat=28;
        }else if(lieu==17){
            this.x=8;
            this.y=7;
            this.etat=28;
        }else{
            this.x=9;
            this.y=7;
            this.etat=28;
        }
    }

    /**
     * ini une piece hors de la carte d'une joueur
     * */
    public Pawn(String joueur){
        nom="pawn";
        if(joueur.equals("joueur1")){
            this.etat=58;
        }else{
            this.etat=68;
        }
    }

    public Pawn(int x,int y,int etat){
        sasirPiece(x,y,etat);
    }

    @Override
    public boolean testDeplacer(int deltaX,int deltaY,int [][]carte) {
        int operation = 0;
        //saisir les cas
        //comme Lancer
        if(deltaX==0 && deltaY ==1 && etat==18){
            operation=1;
        }else if(deltaX==0 && deltaY ==-1 && etat==28){
            operation=2;
        }else if(etat==38){
            //les cas avec promottion
            if(deltaX==-1 && deltaY==1){
                operation=11;
            }else if(deltaX==0 && deltaY==1){
                operation=12;
            }else if(deltaX==1 && deltaY==1){
                operation=13;
            }else if(deltaX==1 && deltaY==0){
                operation=14;
            }else if(deltaX==-1 && deltaY==0){
                operation=15;
            }else if(deltaX==0 && deltaY==-1){
                operation=16;
            }
        }else if(etat == 48){
            //les cas avec promottion
            if(deltaX==-1 && deltaY==-1){
                operation=21;
            }else if(deltaX==0 && deltaY==-1){
                operation=22;
            }else if(deltaX==1 && deltaY==-1){
                operation=23;
            }else if(deltaX==1 && deltaY==0){
                operation=24;
            }else if(deltaX==-1 && deltaY==0){
                operation=25;
            }else if(deltaX==0 && deltaY==1){
                operation=26;
            }
        }

        if(x+deltaX < 1 || x+deltaX > 9 || y+deltaY<1 || y+deltaY>9)
            operation = 0;

        switch (operation){
            case 1:
                //deplacer vers base
            case 2:
                //deplacer vers haut
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
                return !verifierMemeEquipe(carte[x + deltaX][y + deltaY]);
            default:
                return false;
        }
    }


    @Override
    public boolean testPromotion(int deltaY) {
        return testPromoNormal(deltaY);
    }

    @Override
    public ArrayList<InfoD> deplacePossible(int[][] laCarte) {
        InfoDPawn info=new InfoDPawn(x,y,etat);
        return info.geneAutoDeplacement1(laCarte);
    }

    @Override
    public ArrayList<InfoD> deplacePossibleAvecNote(int[][] laCarte) {
        InfoDPawn info=new InfoDPawn(x,y,etat);
        return info.geneAutoDeplacement2(laCarte);
    }

    @Override
    public PieceNormal copyHorsCarte(String joueur) {
        return new Pawn(joueur);
    }

    @Override
    public CarteCommun auto1(CarteCommun laCarte) {

        //si ce piece a deja mange
        if(testEtreMange(laCarte)) {
            etreMange();
            return laCarte;
        }


        int deltaX,deltaY;
        InfoD myInfo=choixAuHazard(deplacePossible(laCarte.getCarte()));
        //verifier que il y a cas de deplacer
        if(myInfo != null){
            InfoDPawn casDeplace= (InfoDPawn) myInfo;
            deltaX = casDeplace.getDeltaX();
            deltaY = casDeplace.getDeltaY();
            if(testDeplacer(deltaX,deltaY,laCarte.getCarte())) {
                laCarte.deplacerUnPiece(x, y, deltaX, deltaY);
                x += deltaX;
                y += deltaY;
            }
        }

        return laCarte;
    }

    @Override
    public CarteCommun auto2(CarteCommun laCarte) {
        //si ce piece a deja mange
        if(testEtreMange(laCarte)) {
            etreMange();
            return laCarte;
        }

        int deltaX,deltaY;
        InfoD myInfo=this.deplacementEquipe(laCarte);
        //verifier que il y a cas de deplacer
        if(myInfo != null && myInfo.getEtatP()==this.getEtat()
                && myInfo.getX()==this.getX() && myInfo.getY()==this.getY()){
            InfoDPawn casDeplace= (InfoDPawn) myInfo;
            deltaX = casDeplace.getDeltaX();
            deltaY = casDeplace.getDeltaY();
            if(testDeplacer(deltaX,deltaY,laCarte.getCarte())) {
                laCarte.deplacerUnPiece(x, y, deltaX, deltaY);
                x += deltaX;
                y += deltaY;
            }
        }

        return laCarte;
    }
}