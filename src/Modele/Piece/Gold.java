package Modele.Piece;

import Modele.InfoDeplace.InfoD;
import Modele.InfoDeplace.InfoDBishop;
import Modele.InfoDeplace.InfoDGold;
import Modele.Map.CarteCommun;

import java.util.ArrayList;

public class Gold extends PieceNormal{
    //lieu initial 1,2,3,4 haut gauche haut droite base gauche base droite
    public Gold(int lieu){
        nom="gold";
        if(lieu==1){
            this.x=4;
            this.y=1;
            this.etat=12;
        }else if(lieu ==2){
            this.x=6;
            this.y=1;
            this.etat=12;
        }else if(lieu ==3){
            this.x=4;
            this.y=9;
            this.etat=22;
        }else{
            this.x=6;
            this.y=9;
            this.etat=22;
        }
    }

    public Gold(int x,int y,int etat){
        sasirPiece(x,y,etat);
    }

    /**
     * ini une piece hors de la carte d'une joueur
     * */
    public Gold(String joueur){
        nom="gold";
        if(joueur.equals("joueur1")){
            this.etat=52;
        }else{
            this.etat=62;
        }
    }



    @Override
    public boolean testDeplacer(int deltaX,int deltaY,int [][]carte) {
        int operation = 0;
        //saisir les cas
        if(etat==12){
            if(deltaX==-1 && deltaY==1){
                operation=1;
            }else if(deltaX==0 && deltaY==1){
                operation=2;
            }else if(deltaX==1 && deltaY==1){
                operation=3;
            }else if(deltaX==1 && deltaY==0){
                operation=4;
            }else if(deltaX==-1 && deltaY==0){
                operation=5;
            }else if(deltaX==0 && deltaY==-1){
                operation=6;
            }
        }else if(etat==22){
            if(deltaX==-1 && deltaY==-1){
                operation=1;
            }else if(deltaX==0 && deltaY==-1){
                operation=2;
            }else if(deltaX==1 && deltaY==-1){
                operation=3;
            }else if(deltaX==1 && deltaY==0){
                operation=4;
            }else if(deltaX==-1 && deltaY==0){
                operation=5;
            }else if(deltaX==0 && deltaY==1){
                operation=6;
            }
        }

        //tester
        if(x+deltaX < 1 || x+deltaX > 9 || y+deltaY<1 || y+deltaY>9)
            operation = 0;

        switch (operation){
            //deplacer a gauche haute
            case 1:
            //deplacer a haute
            case 2:
            //deplacer a droite haute
            case 3:
            //deplacer a gauche
            case 4:
            //deplacer a droite
            case 5:
            //deplacer a base
            case 6:
                return !verifierMemeEquipe(carte[x + deltaX][y + deltaY]);
            default:
                return false;
        }
    }


    @Override
    public boolean testPromotion(int deltaY) {
        return false;
    }

    @Override
    public ArrayList<InfoD> deplacePossible(int[][] laCarte) {
        InfoDGold info=new InfoDGold(x,y,etat);
        return info.geneAutoDeplacement1(laCarte);
    }

    @Override
    public ArrayList<InfoD> deplacePossibleAvecNote(int[][] laCarte) {
        InfoDGold info=new InfoDGold(x,y,etat);
        return info.geneAutoDeplacement2(laCarte);
    }

    @Override
    public PieceNormal copyHorsCarte(String joueur) {
        return new Gold(joueur);
    }

    @Override
    public CarteCommun auto1(CarteCommun laCarte) {
        //si ce piece a deja mange
        if(testEtreMange(laCarte)) {
            etreMange();
            return laCarte;
        }


        int deltaX,deltaY;
        InfoD myInfo=choixAuHazard(deplacePossible(laCarte.getCarte()));
        //verifier que il y a cas de deplacer
        if(myInfo != null){
            InfoDGold casDeplace= (InfoDGold) myInfo;
            deltaX = casDeplace.getDeltaX();
            deltaY = casDeplace.getDeltaY();
            if(testDeplacer(deltaX,deltaY,laCarte.getCarte())) {
                laCarte.deplacerUnPiece(x, y, deltaX, deltaY);
                x += deltaX;
                y += deltaY;
            }
        }

        return laCarte;
    }

    @Override
    public CarteCommun auto2(CarteCommun laCarte) {
        //si ce piece a deja mange
        if(testEtreMange(laCarte)) {
            etreMange();
            return laCarte;
        }

        int deltaX,deltaY;
        InfoD myInfo=this.deplacementEquipe(laCarte);
        //verifier que il y a cas de deplacer
        if(myInfo != null && myInfo.getEtatP()==this.getEtat()
                && myInfo.getX()==this.getX() && myInfo.getY()==this.getY()){
            InfoDGold casDeplace= (InfoDGold) myInfo;
            deltaX = casDeplace.getDeltaX();
            deltaY = casDeplace.getDeltaY();
            if(testDeplacer(deltaX,deltaY,laCarte.getCarte())) {
                laCarte.deplacerUnPiece(x, y, deltaX, deltaY);
                x += deltaX;
                y += deltaY;
            }
        }

        return laCarte;
    }
}
