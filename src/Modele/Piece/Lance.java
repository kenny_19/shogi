package Modele.Piece;

import Modele.InfoDeplace.InfoD;
import Modele.InfoDeplace.InfoDBishop;
import Modele.InfoDeplace.InfoDKing;
import Modele.InfoDeplace.InfoDLance;
import Modele.Map.CarteCommun;

import java.util.ArrayList;

public class Lance extends PieceNormal{
    //lieu initial
    // 1 : en haut à droite
    // 2 : en haut à gauche
    // 3 : en bas à droite
    // 4 : en bas à gauche
    public Lance(int lieu){
        nom="lance";
        if(lieu==1){
            this.x=1;
            this.y=1;
            this.etat=17;
        }else if(lieu ==2){
            this.x=9;
            this.y=1;
            this.etat=17;
        }else if(lieu ==3){
            this.x=1;
            this.y=9;
            this.etat=27;
        }else{
            this.x=9;
            this.y=9;
            this.etat=27;
        }
    }

    /**
     * ini une piece hors de la carte d'une joueur
     * */
    public Lance(String joueur){
        nom="lance";
        if(joueur.equals("joueur1")){
            this.etat=57;
        }else{
            this.etat=67;
        }
    }

    public Lance(int x,int y,int etat){
        sasirPiece(x,y,etat);
    }

    @Override
    public boolean testDeplacer(int deltaX,int deltaY,int [][]carte) {
        //saisir les cas
        //tjr aller direct
        int operation = 0;
        if(deltaX==0 && deltaY>0 && (etat==17||etat==37)){
            operation=1;
        }else if(deltaX==0 && deltaY<0 && (etat==27||etat==47)){
            operation=2;
        }else if(etat == 37 ){
            //les cas avec promottion
            if(deltaX==-1 && deltaY==1){
                operation=11;
            }else if(deltaX==0 && deltaY==1){
                operation=12;
            }else if(deltaX==1 && deltaY==1){
                operation=13;
            }else if(deltaX==1 && deltaY==0){
                operation=14;
            }else if(deltaX==-1 && deltaY==0){
                operation=15;
            }else if(deltaX==0 && deltaY==-1){
                operation=16;
            }
        }else if(etat == 47){
            //les cas avec promottion
            if(deltaX==-1 && deltaY==-1){
                operation=21;
            }else if(deltaX==0 && deltaY==-1){
                operation=22;
            }else if(deltaX==1 && deltaY==-1){
                operation=23;
            }else if(deltaX==1 && deltaY==0){
                operation=24;
            }else if(deltaX==-1 && deltaY==0){
                operation=25;
            }else if(deltaX==0 && deltaY==1){
                operation=26;
            }
        }

        if(x+deltaX < 1 || x+deltaX > 9 || y+deltaY<1 || y+deltaY>9)
            operation = 0;

        switch (operation){
            case 1:
                //deplacer vers base
            case 2:
                //deplacer vers haut
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
                return !verifierMemeEquipe(carte[x + deltaX][y + deltaY]);
            default:
                return false;
        }
    }

    @Override
    public boolean testPromotion(int deltaY) {
        return testPromoNormal(deltaY);
    }

    @Override
    public ArrayList<InfoD> deplacePossible(int[][] laCarte) {
        InfoDLance info=new InfoDLance(x,y,etat);
        return info.geneAutoDeplacement1(laCarte);
    }

    @Override
    public ArrayList<InfoD> deplacePossibleAvecNote(int[][] laCarte) {
        InfoDLance info=new InfoDLance(x,y,etat);
        return info.geneAutoDeplacement2(laCarte);
    }

    @Override
    public PieceNormal copyHorsCarte(String joueur) {
        return new Lance(joueur);
    }

    @Override
    public CarteCommun auto1(CarteCommun laCarte) {
        //si ce piece a deja mange
        if(testEtreMange(laCarte)) {
            etreMange();
            return laCarte;
        }


        int deltaX,deltaY;
        InfoD myInfo=choixAuHazard(deplacePossible(laCarte.getCarte()));
        //verifier que il y a cas de deplacer
        if(myInfo != null){
            InfoDLance casDeplace= (InfoDLance) myInfo;
            deltaX = casDeplace.getDeltaX();
            deltaY = casDeplace.getDeltaY();
            if(testDeplacer(deltaX,deltaY,laCarte.getCarte())) {
                laCarte.deplacerUnPiece(x, y, deltaX, deltaY);
                x += deltaX;
                y += deltaY;
            }
        }

        return laCarte;
    }

    @Override
    public CarteCommun auto2(CarteCommun laCarte) {
        //si ce piece a deja mange
        if(testEtreMange(laCarte)) {
            etreMange();
            return laCarte;
        }

        int deltaX,deltaY;
        InfoD myInfo=this.deplacementEquipe(laCarte);
        //verifier que il y a cas de deplacer
        if(myInfo != null && myInfo.getEtatP()==this.getEtat()
                && myInfo.getX()==this.getX() && myInfo.getY()==this.getY()){
            InfoDLance casDeplace= (InfoDLance) myInfo;
            deltaX = casDeplace.getDeltaX();
            deltaY = casDeplace.getDeltaY();
            if(testDeplacer(deltaX,deltaY,laCarte.getCarte())) {
                laCarte.deplacerUnPiece(x, y, deltaX, deltaY);
                x += deltaX;
                y += deltaY;
            }
        }

        return laCarte;
    }
}