package Modele.Piece;
/**
 * pour fabriquer les pieces
 * */
public class PieceFactory {
    public PieceNormal getPieceEnLieuExact(String typePiece,int coteJoueur,int x,int y){
        if(typePiece.equals("Bishop")){
            if(coteJoueur==1){
                return new Bishop(x,y,16);
            }else{
                return new Bishop(x,y,26);
            }
        }else if(typePiece.equals("Gold")){
            if(coteJoueur==1){
                return new Gold(x,y,12);
            }else{
                return new Gold(x,y,22);
            }
        }else if(typePiece.equals("King")){
            if(coteJoueur==1){
                return new King(x,y,11);
            }else{
                return new King(x,y,21);
            }
        }else if(typePiece.equals("KNight")){
            if(coteJoueur==1){
                return new KNight(x,y,14);
            }else{
                return new KNight(x,y,24);
            }
        }else if(typePiece.equals("Lance")){
            if(coteJoueur==1){
                return new KNight(x,y,17);
            }else{
                return new KNight(x,y,27);
            }
        }else if(typePiece.equals("Pawn")){
            if(coteJoueur==1){
                return new Pawn(x,y,18);
            }else{
                return new Pawn(x,y,28);
            }
        }else if(typePiece.equals("Rook")){
            if(coteJoueur==1){
                return new Rook(x,y,15);
            }else{
                return new Rook(x,y,25);
            }
        }else if(typePiece.equals("Silver")){
            if(coteJoueur==1){
                return new Silver(x,y,13);
            }else{
                return new Silver(x,y,23);
            }
        }else{
            return null;
        }
    }

    public PieceNormal getPieceEnLieu(String typePiece,int lieu){
        switch (typePiece){
            case "Bishop":
                return new Bishop(lieu);
            case "Silver":
                return new Silver(lieu);
            case "Pawn":
                return  new Pawn(lieu);
            case "Lance":
                return  new Lance(lieu);
            case "KNight":
                return new KNight(lieu);
            case "King":
                return new King(lieu);
            case "Gold":
                return new Gold(lieu);
            case "Rook":
                return new Rook(lieu);
            default:
                return null;
        }
    }
}
