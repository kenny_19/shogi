import Modele.Joueur.PieceAuto;
import Modele.Map.CarteCommun;
import Modele.Piece.PieceFactory;
import Vue.Dessinateur;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Final {
    public static void main(String[] args) {

        choisirStrategie();
        //jouer(3,3);
    }
    /**
     * lancer interface pour choisir strategie
     * */
    public static void choisirStrategie(){
        JFrame window = new JFrame();
        JComboBox jcb;	//下拉框 boite au choisir
        JComboBox jcb1;
        JLabel jlb1, jlb2,faceDeCarte;


        //ce table pour stocker de strategie
        String tabTypeStrqtegie[] = new String [2];
        //jp1 = new JPanel();
        //jp2 = new JPanel();

        JLabel label1 = new JLabel("配置AI的策略 :", JLabel.CENTER);
        window.add(label1);
        label1.setBounds(700, 200, 220, 40);
        // label1.setForeground(Color.RED);
        label1.setFont(new Font("Serif", Font.BOLD, 16));

        jlb1 = new JLabel("玩家 1");
        String str1[] = {"优先吃掉棋子再移动", "minmax算法", "minmax算法 Alpha-Beta 剪枝", "随机移动"};
        jcb = new JComboBox(str1);

        jlb2 = new JLabel("玩家 2");
        String str2[] = {"优先吃掉棋子再移动", "minmax算法", "minmax算法 Alpha-Beta 剪枝", "随机移动"};
        jcb1 = new JComboBox(str2);

        window.add(jlb1);
        window.add(jcb);

        window.add(jlb2);
        window.add(jcb1);

        jlb1.setBounds(700, 260, 200, 20);
        // label2.setForeground(Color.RED);
        jlb1.setFont(new Font("Serif", Font.BOLD, 14));

        jcb.setBounds(700, 325, 200, 20);
        // label2.setForeground(Color.RED);


        jlb2.setBounds(700, 400, 200, 20);
        // label3.setForeground(Color.RED);
        jlb2.setFont(new Font("Serif", Font.BOLD, 14));

        jcb1.setBounds(700, 465, 200, 20);
        // label2.setForeground(Color.RED);

        //ajouter bouton de lancer
        JButton bouton = new JButton("开始游戏 !");
        bouton.setFont(new Font("Serif", Font.BOLD, 18));
        bouton.setBackground(Color.green);
        window.add(bouton);
        bouton.setBounds(700, 100, 150, 60);

        //ajouter interface pour beaute
        faceDeCarte=new JLabel();
        faceDeCarte.setIcon(new ImageIcon("src/img/interface.PNG"));
        window.add(faceDeCarte);
        faceDeCarte.setBounds(0,0,720,720);



        window.setSize(950,720);
        window.setTitle("page menu");
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setVisible(true);

        bouton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //obtient les strategie au choix pour equipe
                String strategie1 = (String)jcb.getSelectedItem();
                String strategie2 = (String)jcb1.getSelectedItem();
                int strqtegieNum1 = chercheNumberDeStrategie(strategie1);
                int strqtegieNum2 = chercheNumberDeStrategie(strategie2);

                //quitter la page menu/setting
                window.setVisible(false);

                //lancer jeu
                jouer(strqtegieNum1,strqtegieNum2);
            }
        });
    }
    /**
     * renvoyer nom de Strategie
     * 2 communication entre eux pas de priori d'action
     * 3 communication entre eux avec priorite de action basic mange et bouge
     * 4 communication entre eux avec priorite de action avec les note vient de algo minmax
     * 5 communication entre eux avec priorite de action avec les note vient de algo alphe-beta minmax
     * */
    public static String chercheNomDeStrategie(int typJeu){
        if(typJeu == 3)
            return "Priori Manger Et Bouger";
        else if(typJeu == 4)
            return "Algo MinMax";
        else if(typJeu == 5)
            return "Algo Alpha-Beta MinMax";
        else
            return "Deplacer Aleatoire";
    }

    /**
     * renvoyer numero de Strategie d'apres String
     * 2 communication entre eux pas de priori d'action
     * 3 communication entre eux avec priorite de action basic mange et bouge
     * 4 communication entre eux avec priorite de action avec les note vient de algo minmax
     * 5 communication entre eux avec priorite de action avec les note vient de algo alphe-beta minmax
     * */
    public static int chercheNumberDeStrategie(String nomStratage){
        if(nomStratage.equals("Priori Manger Et Bouger"))
            return 3;
        else if(nomStratage.equals("Algo MinMax"))
            return 4;
        else if(nomStratage.equals("Algo Alpha-Beta MinMax"))
            return 5;
        else
            return 2;
    }

    /**
     * Pour jouer ce jeu
     * @param typeJeuEquipe1 type de strategie pour equipe 1
     * @param typeJeuEquipe2 type de strategie pour equipe 2
     * valeur pour les type: 2 communication entre eux pas de priori d'action
     *                       3 communication entre eux avec priorite de action basic mange et bouge
     *                       4 communication entre eux avec priorite de action avec les note vient de algo minmax
     *                       5 communication entre eux avec priorite de action avec les note vient de algo alphe-beta minmax
     */
    public static void jouer(int typeJeuEquipe1, int typeJeuEquipe2){
        if(typeJeuEquipe1<2 || typeJeuEquipe1>5)
            typeJeuEquipe1=1;
        if(typeJeuEquipe2<2 || typeJeuEquipe2>5)
            typeJeuEquipe2=1;

        CarteCommun myCarteM=new CarteCommun();
        Dessinateur dessine=new Dessinateur(myCarteM,3);
        JFrame frame=new JFrame();
        frame.setTitle(Dessinateur.TITLE);

        //ajouter label
        String titleSrategieEquip1 = chercheNomDeStrategie(typeJeuEquipe1);
        String titleSrategieEquip2 = chercheNomDeStrategie(typeJeuEquipe2);

        JLabel label2 = new JLabel("Joueur 1", JLabel.CENTER);
        frame.add(label2);
        label2.setBounds(730, 260, 200, 20);
        // label2.setForeground(Color.RED);
        label2.setFont(new Font("Serif", Font.BOLD, 14));

        JLabel label21 = new JLabel(titleSrategieEquip1, JLabel.CENTER);
        frame.add(label21);
        label21.setBounds(730, 325, 200, 20);
        // label2.setForeground(Color.RED);
        label21.setFont(new Font("Serif", Font.BOLD, 14));

        JLabel label3 = new JLabel("Joueur 2", JLabel.CENTER);
        frame.add(label3);
        label3.setBounds(730, 400, 200, 20);
        // label3.setForeground(Color.RED);
        label3.setFont(new Font("Serif", Font.BOLD, 14));

        JLabel label31 = new JLabel(titleSrategieEquip2, JLabel.CENTER);
        frame.add(label31);
        label31.setBounds(730, 465, 200, 20);
        // label2.setForeground(Color.RED);
        label31.setFont(new Font("Serif", Font.BOLD, 14));


        //partie carte
        frame.add(dessine);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);

        frame.setVisible(true);


        CyclicBarrier myB = new CyclicBarrier(40,dessine);

        Thread t1;
        Thread t2;
        Thread t3;
        Thread t4;
        Thread t5;
        Thread t6;
        Thread t7;
        Thread t8;
        Thread t9;
        Thread t10;
        Thread t11;
        Thread t12;
        Thread t13;
        Thread t14;
        Thread t15;
        Thread t16;
        Thread t17;
        Thread t18;
        Thread t19;
        Thread t20;
        Thread t21;
        Thread t22;
        Thread t23;
        Thread t24;
        Thread t25;
        Thread t26;
        Thread t27;
        Thread t28;
        Thread t29;
        Thread t30;
        Thread t31;
        Thread t32;
        Thread t33;
        Thread t34;
        Thread t35;
        Thread t36;
        Thread t37;
        Thread t38;
        Thread t39;
        Thread t40;

        PieceFactory pf=new PieceFactory();
        //ini les pawn

        t1 = new PieceAuto(pf.getPieceEnLieu("Pawn",1),myCarteM,myB,"t1",typeJeuEquipe1);
        t2 = new PieceAuto(pf.getPieceEnLieu("Pawn",2),myCarteM,myB,"t2",typeJeuEquipe1);
        t3 = new PieceAuto(pf.getPieceEnLieu("Pawn",3),myCarteM,myB,"t3",typeJeuEquipe1);
        t4 = new PieceAuto(pf.getPieceEnLieu("Pawn",4),myCarteM,myB,"t4",typeJeuEquipe1);
        t5 = new PieceAuto(pf.getPieceEnLieu("Pawn",5),myCarteM,myB,"t5",typeJeuEquipe1);
        t6 = new PieceAuto(pf.getPieceEnLieu("Pawn",6),myCarteM,myB,"t6",typeJeuEquipe1);
        t7 = new PieceAuto(pf.getPieceEnLieu("Pawn",7),myCarteM,myB,"t7",typeJeuEquipe1);
        t8 = new PieceAuto(pf.getPieceEnLieu("Pawn",8),myCarteM,myB,"t8",typeJeuEquipe1);

        t9 = new PieceAuto(pf.getPieceEnLieu("Pawn",9),myCarteM,myB,"t9",typeJeuEquipe2);
        t10 = new PieceAuto(pf.getPieceEnLieu("Pawn",10),myCarteM,myB,"t10",typeJeuEquipe2);
        t11 = new PieceAuto(pf.getPieceEnLieu("Pawn",11),myCarteM,myB,"t11",typeJeuEquipe2);
        t12 = new PieceAuto(pf.getPieceEnLieu("Pawn",12),myCarteM,myB,"t12",typeJeuEquipe2);
        t13 = new PieceAuto(pf.getPieceEnLieu("Pawn",13),myCarteM,myB,"t13",typeJeuEquipe2);
        t14 = new PieceAuto(pf.getPieceEnLieu("Pawn",14),myCarteM,myB,"t14",typeJeuEquipe2);
        t15 = new PieceAuto(pf.getPieceEnLieu("Pawn",15),myCarteM,myB,"t15",typeJeuEquipe2);
        t16 = new PieceAuto(pf.getPieceEnLieu("Pawn",16),myCarteM,myB,"t16",typeJeuEquipe2);
        t17 = new PieceAuto(pf.getPieceEnLieu("Pawn",17),myCarteM,myB,"t17",typeJeuEquipe2);
        t18 = new PieceAuto(pf.getPieceEnLieu("Pawn",18),myCarteM,myB,"t18",typeJeuEquipe2);

        //ini les bishop
        t19 = new PieceAuto(pf.getPieceEnLieu("Bishop",1),myCarteM,myB,"t19",typeJeuEquipe1);
        t20 = new PieceAuto(pf.getPieceEnLieu("Bishop",2),myCarteM,myB,"t20",typeJeuEquipe2);

        //ini les gold
        t21 = new PieceAuto(pf.getPieceEnLieu("Gold",1),myCarteM,myB,"t21",typeJeuEquipe1);
        t22 = new PieceAuto(pf.getPieceEnLieu("Gold",2),myCarteM,myB,"t22",typeJeuEquipe1);
        t23 = new PieceAuto(pf.getPieceEnLieu("Gold",3),myCarteM,myB,"t23",typeJeuEquipe2);
        t24 = new PieceAuto(pf.getPieceEnLieu("Gold",4),myCarteM,myB,"t24",typeJeuEquipe2);

        //ini les king
        t25 = new PieceAuto(pf.getPieceEnLieu("King",1),myCarteM,myB,"t25",typeJeuEquipe1);
        t26 = new PieceAuto(pf.getPieceEnLieu("King",2),myCarteM,myB,"t26",typeJeuEquipe2);

        //ini les knights
        t27 = new PieceAuto(pf.getPieceEnLieu("KNight",1),myCarteM,myB,"t27",typeJeuEquipe1);
        t28 = new PieceAuto(pf.getPieceEnLieu("KNight",2),myCarteM,myB,"t28",typeJeuEquipe1);
        t29 = new PieceAuto(pf.getPieceEnLieu("KNight",3),myCarteM,myB,"t29",typeJeuEquipe2);
        t30 = new PieceAuto(pf.getPieceEnLieu("KNight",4),myCarteM,myB,"t30",typeJeuEquipe2);

        //ini les lancer
        t31 = new PieceAuto(pf.getPieceEnLieu("Lance",1),myCarteM,myB,"t31",typeJeuEquipe1);
        t32 = new PieceAuto(pf.getPieceEnLieu("Lance",2),myCarteM,myB,"t32",typeJeuEquipe1);
        t33 = new PieceAuto(pf.getPieceEnLieu("Lance",3),myCarteM,myB,"t33",typeJeuEquipe2);
        t34 = new PieceAuto(pf.getPieceEnLieu("Lance",4),myCarteM,myB,"t34",typeJeuEquipe2);

        //ini les rook
        t35 = new PieceAuto(pf.getPieceEnLieu("Rook",1),myCarteM,myB,"t35",typeJeuEquipe1);
        t36 = new PieceAuto(pf.getPieceEnLieu("Rook",2),myCarteM,myB,"t36",typeJeuEquipe2);

        //ini les silver
        t37 = new PieceAuto(pf.getPieceEnLieu("Silver",1),myCarteM,myB,"t37",typeJeuEquipe1);
        t38 = new PieceAuto(pf.getPieceEnLieu("Silver",2),myCarteM,myB,"t38",typeJeuEquipe1);
        t39 = new PieceAuto(pf.getPieceEnLieu("Silver",3),myCarteM,myB,"t39",typeJeuEquipe2);
        t40 = new PieceAuto(pf.getPieceEnLieu("Silver",4),myCarteM,myB,"t40",typeJeuEquipe2);

        Executor executor = Executors.newFixedThreadPool(40);

        executor.execute(t1);
        executor.execute(t2);
        executor.execute(t3);
        executor.execute(t4);
        executor.execute(t5);
        executor.execute(t6);
        executor.execute(t7);
        executor.execute(t8);
        executor.execute(t9);
        executor.execute(t10);

        executor.execute(t11);
        executor.execute(t12);
        executor.execute(t13);
        executor.execute(t14);
        executor.execute(t15);
        executor.execute(t16);
        executor.execute(t17);
        executor.execute(t18);
        executor.execute(t19);
        executor.execute(t20);

        executor.execute(t21);
        executor.execute(t22);
        executor.execute(t23);
        executor.execute(t24);
        executor.execute(t25);
        executor.execute(t26);
        executor.execute(t27);
        executor.execute(t28);
        executor.execute(t29);
        executor.execute(t30);

        executor.execute(t31);
        executor.execute(t32);
        executor.execute(t33);
        executor.execute(t34);
        executor.execute(t35);
        executor.execute(t36);
        executor.execute(t37);
        executor.execute(t38);
        executor.execute(t39);
        executor.execute(t40);
    }
}
