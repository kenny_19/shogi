import Modele.Joueur.PieceAuto;
import Modele.Map.CarteCommun;
import Modele.Piece.PieceFactory;
import Vue.MapEnVue;
import Vue.PieceEnVue;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.util.concurrent.CyclicBarrier;

/**
 * Ce class est pour auto deplcement de piece.
 * Avec communication avec donnes en commun.
 * En train de faire.
 * */
public class GameAuto2 extends Canvas implements Runnable {
    public static final int WIDTH=720,HEIGHT=720;
    private boolean isRunning=false;
    private Thread thread;
    public static final String TITLE="Shogi";
    public MapEnVue level;
    public PieceEnVue piece;
    public CarteCommun myCarteM;
    public CyclicBarrier myB=new CyclicBarrier(40);
    public Thread t1;
    public Thread t2;
    public Thread t3;
    public Thread t4;
    public Thread t5;
    public Thread t6;
    public Thread t7;
    public Thread t8;
    public Thread t9;
    public Thread t10;
    public Thread t11;
    public Thread t12;
    public Thread t13;
    public Thread t14;
    public Thread t15;
    public Thread t16;
    public Thread t17;
    public Thread t18;
    public Thread t19;
    public Thread t20;
    public Thread t21;
    public Thread t22;
    public Thread t23;
    public Thread t24;
    public Thread t25;
    public Thread t26;
    public Thread t27;
    public Thread t28;
    public Thread t29;
    public Thread t30;
    public Thread t31;
    public Thread t32;
    public Thread t33;
    public Thread t34;
    public Thread t35;
    public Thread t36;
    public Thread t37;
    public Thread t38;
    public Thread t39;
    public Thread t40;

    public GameAuto2() {
        Dimension dimension=new Dimension(GameAuto1.WIDTH, GameAuto1.HEIGHT);
        setPreferredSize(dimension);
        setMinimumSize(dimension);
        setMaximumSize(dimension);

        //ini ce jeu
        myCarteM =new CarteCommun();

        level = new MapEnVue();//ini la carte
        piece= new PieceEnVue();//ini les piece
    }

    public GameAuto2(CarteCommun myCarteM) {
        Dimension dimension=new Dimension(GameAuto1.WIDTH, GameAuto1.HEIGHT);
        setPreferredSize(dimension);
        setMinimumSize(dimension);
        setMaximumSize(dimension);

        //ini ce jeu
        this.myCarteM =myCarteM;

        level = new MapEnVue();//ini la carte
        piece= new PieceEnVue();//ini les piece
    }

    private void render() {
        BufferStrategy bs=getBufferStrategy();
        if(bs==null) {
            createBufferStrategy(3);
            return;
        }

        Graphics g=bs.getDrawGraphics();
        g.setColor(Color.black);
        g.fillRect(0, 0, Game.WIDTH, Game.HEIGHT);


        //print les chose
        level.render(g);//appelle render dans MapEnVue
        piece.render(g,myCarteM.getCarte());
        //piecec.render(g,myCarteM.getStartage())
        g.dispose();
        bs.show();
    }

    @Override
    public void run() {
        requestFocus();
        int fps=0;
        double timer=System.currentTimeMillis();
        long lastTime=System.nanoTime();
        double targetTick=60.0;
        double delta=0;
        double ns=1000000000/targetTick;



        //pour attend dessine
        int i = 0;
        while(isRunning) {
            long now=System.nanoTime();
            delta+=(now - lastTime)/ns;
            lastTime=now;
            while(delta>=1) {
                i++;
                if(i%10 == 0 && i != 0){
                    tick();
                    i=0;
                }
                render();
                fps++;
                delta--;
            }

            if(System.currentTimeMillis() - timer>=1000) {
                //System.out.println(fps);
                fps=0;
                timer+=1000;
            }
        }
        stop();
    }

    public synchronized void stop()  {
        if(!isRunning)return;
        isRunning=false;

        try {
            thread.join();
            t1.join();
            t2.join();
            t3.join();
            t4.join();
            t5.join();
            t6.join();
            t7.join();
            t8.join();
            t9.join();
            t10.join();
            t11.join();
            t12.join();
            t13.join();
            t14.join();
            t15.join();
            t16.join();
            t17.join();
            t18.join();
            t19.join();
            t20.join();
            t21.join();
            t22.join();
            t23.join();
            t24.join();
            t25.join();
            t26.join();
            t27.join();
            t28.join();
            t29.join();
            t30.join();
            t31.join();
            t32.join();
            t33.join();
            t34.join();
            t35.join();
            t36.join();
            t37.join();
            t38.join();
            t39.join();
            t40.join();

        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

    private void tick() {
        //System.out.println("Working");
    }

    public synchronized void start() {
        if(isRunning)return;
        isRunning=true;

        thread=new Thread(this);

        PieceFactory pf=new PieceFactory();
        //ini les pawn
        t1 = new PieceAuto(pf.getPieceEnLieu("Pawn",1),myCarteM,myB,"t1");
        t2 = new PieceAuto(pf.getPieceEnLieu("Pawn",2),myCarteM,myB,"t2");
        t3 = new PieceAuto(pf.getPieceEnLieu("Pawn",3),myCarteM,myB,"t3");
        t4 = new PieceAuto(pf.getPieceEnLieu("Pawn",4),myCarteM,myB,"t4");
        t5 = new PieceAuto(pf.getPieceEnLieu("Pawn",5),myCarteM,myB,"t5");
        t6 = new PieceAuto(pf.getPieceEnLieu("Pawn",6),myCarteM,myB,"t6");
        t7 = new PieceAuto(pf.getPieceEnLieu("Pawn",7),myCarteM,myB,"t7");
        t8 = new PieceAuto(pf.getPieceEnLieu("Pawn",8),myCarteM,myB,"t8");

        t9 = new PieceAuto(pf.getPieceEnLieu("Pawn",9),myCarteM,myB,"t9");
        t10 = new PieceAuto(pf.getPieceEnLieu("Pawn",10),myCarteM,myB,"t10");
        t11 = new PieceAuto(pf.getPieceEnLieu("Pawn",11),myCarteM,myB,"t11");
        t12 = new PieceAuto(pf.getPieceEnLieu("Pawn",12),myCarteM,myB,"t12");
        t13 = new PieceAuto(pf.getPieceEnLieu("Pawn",13),myCarteM,myB,"t13");
        t14 = new PieceAuto(pf.getPieceEnLieu("Pawn",14),myCarteM,myB,"t14");
        t15 = new PieceAuto(pf.getPieceEnLieu("Pawn",15),myCarteM,myB,"t15");
        t16 = new PieceAuto(pf.getPieceEnLieu("Pawn",16),myCarteM,myB,"t16");
        t17 = new PieceAuto(pf.getPieceEnLieu("Pawn",17),myCarteM,myB,"t17");
        t18 = new PieceAuto(pf.getPieceEnLieu("Pawn",18),myCarteM,myB,"t18");

        //ini les bishop
        t19 = new PieceAuto(pf.getPieceEnLieu("Bishop",1),myCarteM,myB,"t19");
        t20 = new PieceAuto(pf.getPieceEnLieu("Bishop",2),myCarteM,myB,"t20");

        //ini les gold
        t21 = new PieceAuto(pf.getPieceEnLieu("Gold",1),myCarteM,myB,"t21");
        t22 = new PieceAuto(pf.getPieceEnLieu("Gold",2),myCarteM,myB,"t22");
        t23 = new PieceAuto(pf.getPieceEnLieu("Gold",3),myCarteM,myB,"t23");
        t24 = new PieceAuto(pf.getPieceEnLieu("Gold",4),myCarteM,myB,"t24");

        //ini les king
        t25 = new PieceAuto(pf.getPieceEnLieu("King",1),myCarteM,myB,"t25");
        t26 = new PieceAuto(pf.getPieceEnLieu("King",2),myCarteM,myB,"t26");

        //ini les knights
        t27 = new PieceAuto(pf.getPieceEnLieu("KNight",1),myCarteM,myB,"t27");
        t28 = new PieceAuto(pf.getPieceEnLieu("KNight",2),myCarteM,myB,"t28");
        t29 = new PieceAuto(pf.getPieceEnLieu("KNight",3),myCarteM,myB,"t29");
        t30 = new PieceAuto(pf.getPieceEnLieu("KNight",4),myCarteM,myB,"t30");

        //ini les lancer
        t31 = new PieceAuto(pf.getPieceEnLieu("Lance",1),myCarteM,myB,"t31");
        t32 = new PieceAuto(pf.getPieceEnLieu("Lance",2),myCarteM,myB,"t32");
        t33 = new PieceAuto(pf.getPieceEnLieu("Lance",3),myCarteM,myB,"t33");
        t34 = new PieceAuto(pf.getPieceEnLieu("Lance",4),myCarteM,myB,"t34");

        //ini les rook
        t35 = new PieceAuto(pf.getPieceEnLieu("Rook",1),myCarteM,myB,"t35");
        t36 = new PieceAuto(pf.getPieceEnLieu("Rook",2),myCarteM,myB,"t36");

        //ini les silver
        t37 = new PieceAuto(pf.getPieceEnLieu("Silver",1),myCarteM,myB,"t37");
        t38 = new PieceAuto(pf.getPieceEnLieu("Silver",2),myCarteM,myB,"t38");
        t39 = new PieceAuto(pf.getPieceEnLieu("Silver",3),myCarteM,myB,"t39");
        t40 = new PieceAuto(pf.getPieceEnLieu("Silver",4),myCarteM,myB,"t40");

        thread.start();
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();
        t7.start();
        t8.start();
        t9.start();
        t10.start();
        t11.start();
        t12.start();
        t13.start();
        t14.start();
        t15.start();
        t16.start();
        t17.start();
        t18.start();
        t19.start();
        t20.start();
        t21.start();
        t22.start();
        t23.start();
        t24.start();
        t25.start();
        t26.start();
        t27.start();
        t28.start();
        t29.start();
        t30.start();
        t31.start();
        t32.start();
        t33.start();
        t34.start();
        t35.start();
        t36.start();
        t37.start();
        t38.start();
        t39.start();
        t40.start();
    }

    public static void main(String[] args) {

        CarteCommun c=new CarteCommun();
        GameAuto2 game=new GameAuto2(c);
        JFrame frame=new JFrame();
        frame.setTitle(Game.TITLE);
        frame.add(game);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);

        frame.setVisible(true);
        

        game.start();

    }
}
